<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use App\Bus;

/**
 * Description of LoadHireTable
 *
 * @author anura
 */
class LoadHireTable {
    public function index() {
        $data = executePDOQuery("SELECT`bus`.`id`, `bus`.`bus_number`, `bus`.`seat_count`, GROUP_CONCAT(`feature`.`feature_name`) as feature_name FROM `bus_booking`.`bus_has_feature`RIGHT JOIN `bus_booking`.`bus` ON (`bus_has_feature`.`bus_id` = `bus`.`id`)LEFT JOIN `bus_booking`.`feature` ON (`bus_has_feature`.`feature_id` = `feature`.`id`)WHERE (`bus`.`is_for_hire` =1)GROUP BY `bus`.`id`", array());
        return json_encode(array("data"=> (json_decode($data))));
    }
}
