<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $layout_name
 * @property Bus[] $buses
 */
class SeatingLayout extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'seating_layout';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['layout_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function buses()
    {
        return $this->hasMany('App\Bus');
    }
}
