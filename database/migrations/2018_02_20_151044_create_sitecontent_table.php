<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSitecontentTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sitecontent', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('contentname', 100)->nullable();
			$table->text('content', 65535)->nullable();
			$table->string('pagename', 30)->nullable();
			$table->boolean('isimage')->default(0);
			$table->boolean('iscolor')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sitecontent');
	}

}
