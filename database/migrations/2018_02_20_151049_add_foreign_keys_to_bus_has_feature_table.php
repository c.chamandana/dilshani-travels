<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBusHasFeatureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bus_has_feature', function(Blueprint $table)
		{
			$table->foreign('bus_id', 'fk_bus_has_feature_bus1')->references('id')->on('bus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('feature_id', 'fk_bus_has_feature_feature1')->references('id')->on('feature')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bus_has_feature', function(Blueprint $table)
		{
			$table->dropForeign('fk_bus_has_feature_bus1');
			$table->dropForeign('fk_bus_has_feature_feature1');
		});
	}

}
