@extends('layouts.admin')
<?php 
session(['selectedDate'=>$_GET['date']]);
?>
@section('mainContent')
<table id="timetbl" class="table table-striped table-bordered" width='100%'>
    <thead>
        <tr>
            <th></th>
            <th>From</th>
            <th>To</th>
            <th>Bus No.</th>
            <th>Day</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Action</th>
        </tr>
    </thead>
</table>
@stop

@section('scripts')
<script>
    $(document).ready(function () {
        var table = $("#timetbl").DataTable({
            "searching": true,
            paging: true,
            "ajax": {
                "url": "/LoadTimeTableWithSelectedData/"+$.urlParam('start')+"/"+$.urlParam('end')+"/"+$.urlParam('from')+"/"+$.urlParam('to')+"/"+$.urlParam('date')
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<form action="/seating" method="get">\n\
<input type="hidden" name="ttID"/>\n\
<input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
<div class="btn-group">\n\
<button type="submit" class="btn btn-primary" value="Book">\n\
Book\n\
</button>\n\
</div>\n\
<form>',
                    "className": "text-center"
                },
                {
                    "targets": 0,
                    "visible": false
                }],
            "columns": [
                {"data": "ID", 'title': ''},
                {"data": "FromBusStand", 'title': 'From'},
                {"data": "ToBusStand", 'title': 'To'},
                {"data": "BusNo", 'title': 'Bus No.'},
                {"data": "Day", 'title': 'Day'},
                {"data": "StartTime", 'title': 'Start Time'},
                {"data": "EndTime", 'title': 'End Time'},
                {"data": "action", "title": "ACTIONS"}
            ],
            'rowCallback': function (row, data, index) {
                $(row).find('input[name=ttID]').val(data['ID']);
            },
            "order": [[2, "desc"]]
        });
    });
</script>
@stop