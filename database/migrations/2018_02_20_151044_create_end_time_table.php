<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEndTimeTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('end_time', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('time_name', 7)->nullable()->unique('time_name_UNIQUE');
			$table->time('time_value')->nullable()->unique('time_value_UNIQUE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('end_time');
	}

}
