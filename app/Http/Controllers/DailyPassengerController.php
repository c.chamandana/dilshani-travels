<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DailyPassengerController extends Controller
{
    public function save(Request $request)
    {
    	$user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();

        $customer = null;

        if (\App\Customer::where("users_id", $user->id)->exists() == false)
        {
            // Make the user a new customer
            $customer = new \App\Customer;
            $customer->email = $user->email;
            $customer->customer_name = $user->name;
            $customer->address_line1 = "";
            $customer->address_line2 = "";
            $customer->city_id = "1";
            $customer->mobile_no = "";
            $customer->recovery_code = null;
            $customer->users_id = $user->id;

            $customer->save();
        } 
        else 
        {
            // Get the customer
            $customer = \App\Customer::where("users_id", $user->id)->first();
        }

        $adults = $request->input("seats");

        $format = "Y-m-d";

        $from = strtotime($request->from_date);
        $to = strtotime($request->to_date);

        $adults = $request->input("seats");

        var_dump($request->input("to_date"));
        $booking = \App\Booking::create(
        [
            "time_table_id" => $request->input("bookingID"),
            "customer_id" => $customer->id,
            "booking_status_id" => 4,
            "payment_status_id" => 2,
            "booked_date" => date("Y-m-d"),
            "booking_date" => $request->input("from_date"),
            "end_date" => $request->input("to_date")
        ]);

        $booking->ref = "Ref".str_pad($booking->id, 8, "0", STR_PAD_LEFT);
        $booking->save();
        $seats = $request->input("seats");
        $seatsArray = explode(",", $seats);

        // Reserve the seats
        foreach ($seatsArray as $seat) {
            \App\BookingHasSeats::create([
                "booking_id" => $booking->id,
                "seats_id" => $seat,
                "ticket_printed" => false
            ]);
        }
        
        return redirect()->route("viewSummary",['bookingID'=>$booking->id]);
    }
}
