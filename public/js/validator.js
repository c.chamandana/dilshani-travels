(() =>
{
	var _emailValidator = (input) =>
	{
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	    return re.test(String(input).toLowerCase());
	}

	var _telephoneValidator = (input) =>
	{
		if (input.length == 10 || input.length == 9) // 000 000 0000 || 00 000 0000
		{
			return !(isNaN(input));
		}
		else if (input.length == 12 && input.includes("+")) // +94 00 000 0000
		{
			return !(isNaN(input.replace("+", "")));
		}

		return false;
	}

	$.validate = function(element)
	{	
		var $element = $(element);

		var type = $element.attr("type");
		var value = $element.val();

		switch (type)
		{
			case 'text' : 
				if (value.replace(" ", "").length == 0)
					return false;
				else
					return true;
				break;
			case 'email' : 
				return _emailValidator(value);
				break;
			case 'tel' :
				return _telephoneValidator(value);
				break;

			default : 
				if (value === null)
					return false;
				else
					if (value.replace(" ", "").length == 0)
						return false;
					else
						return true;
				break;
		}
	};
}
)();