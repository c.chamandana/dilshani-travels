<script id="result-template" type="text/x-handlebars-template">
    <form action="/seating" method="GET">
        <input type="hidden" name="ttID" value="{{ID}}">
        <input type="hidden" name="_token" value="{{ csrf_token }}">
        <div class="result-card">
            <table>
                <tr>
                    <td class="left">
                        <span class="bus-from-to">{{ busFrom }} - {{ busTo }}</span><br>
                        <span class="bus-start-end">{{ busDay }}, {{ busStart }} - {{ busEnd }}</span>
                    </td>
                    <td class="right">
                        <button type="submit" class="book-btn">BOOK NOW</button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</script>