<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Session;

class AdminInsist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {
            $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();

            if ($user->admin != 1)
            {
                Session::flash("message", "Only administrators are allowed to access that page");
                Session::flash("message-type", "danger");

                return redirect("/login")->with([
                    "message" => "Only administrators are allowed to access that page",
                    "message-type" => "danger"
                ]);
            }
        } else {
            Session::flash("message", "You need to login first");
            Session::flash("message-type", "danger");

            return redirect("/login")->with([
                "message" => "You need to login first",
                "message-type" => "danger"
            ]);
        }

        return $next($request);
    }
}
