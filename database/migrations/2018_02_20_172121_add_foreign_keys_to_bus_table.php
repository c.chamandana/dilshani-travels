<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bus', function(Blueprint $table)
		{
			$table->foreign('seating_layout_id', 'fk_bus_seating_layout1')->references('id')->on('seating_layout')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bus', function(Blueprint $table)
		{
			$table->dropForeign('fk_bus_seating_layout1');
		});
	}

}
