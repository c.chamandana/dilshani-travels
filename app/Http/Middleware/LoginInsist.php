<?php

namespace App\Http\Middleware;
use Auth;
use Session;
use Closure;

class LoginInsist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check())
        {

        } else {
            Session::flash("message", "You need to login first");
            Session::flash("message-type", "danger");

            return redirect("/login")->with([
                "message" => "You need to login first",
                "message-type" => "danger"
            ]);
        }

        return $next($request);
    }
}
