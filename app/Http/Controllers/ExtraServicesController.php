<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ExtraServiceSubmission;
use Session;

class ExtraServicesController extends Controller
{
	// function validates input (if the input is empty or whitespace)
	public function valid($ar)
	{
		foreach ($ar as $val)
		{
			if (empty($val))
			{
				return false;
			}
		}

		return true;
	}

	public function submit(Request $request)
	{
		$user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();


		// Check if the user is logged in
		if ($user)
		{
			$name = $request->name;
			$phone = $request->phone;
			$email = $request->email;
			$service = $request->service;
			$bus_no = $request->bus_no;
			$journey_start = $request->journey_start;
			$journey_destination = $request->journey_destination;
			$description = $request->description;

			// Check if all required fields are not empty
			if (ExtraServicesController::valid([ $name, $phone, $email, $service, $description ]))
			{
				// Create a new submission
				$submission = new ExtraServiceSubmission;
				$submission->user_id = $user->id;
				$submission->name = $name;
				$submission->phone = $phone;
				$submission->email = $email;
				$submission->type = $service;
				$submission->bus = $bus_no;
				$submission->start_location = $journey_start;
				$submission->end_location = $journey_destination;
				$submission->description = $description;

				// Save a new record
				$submission->save();

				return redirect("home")->with([
					"modal" => "extra-service-modal"
				]);
			} else {

				// User hasn't inputted every field correctly, return the user back to the extras page
				return redirect("extras")->with([
					"message" => "You haven't filled all the fields of the form",
					"message-type" => "danger"
				]);
			}
		}
	}
}
