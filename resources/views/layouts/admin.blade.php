<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
Krlove\EloquentModelGenerator\Provider\GeneratorServiceProvider::class,
-->
@auth
<html>
    <head>
        <meta charset="UTF-8">
        <title>{{ config('app.description') }} | {{ config('app.name') }}</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <!-- Bootstrap 3.3.2 -->
        <link href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- Font Awesome Icons -->
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!-- Ionicons -->
        <link href="http://code.ionicframework.com/ionicons/2.0.0/css/ionicons.min.css" rel="stylesheet" type="text/css" />
        <!-- Theme style -->
        <link href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css')}}" rel="stylesheet" type="text/css" />
        <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
              page. However, you can choose any other skin. Make sure you
              apply the skin class to the body tag so the changes take effect.
        -->
        <link href="{{ asset('/bower_components/admin-lte/dist/css/skins/skin-blue.min.css')}}" rel="stylesheet" type="text/css" />

        <link href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/css/seatinglayout.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/assets/JqueryUI/jquery-ui.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css')}}" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="{{ asset("/bower_components/moment/min/moment.min.js") }}"></script>

        <link href="{{ asset('/css/custom.css')}}" rel="stylesheet" type="text/css" />
        <link rel="icon" href="{{ asset("/assets/images/icon.png") }}">

        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset("/css/admin.css") }}">

        <link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/animate/animate.css") }}">

        @yield('styles')

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="skin-blue">
        <div class="wrapper">

            @include('header')
            <!-- Left side column. contains the logo and sidebar -->
            @include('sidebar')
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">

                <!-- Main content -->
                <section class="content">

                    @yield('mainContent')

                </section><!-- /.content -->
            </div><!-- /.content-wrapper -->

            <!-- Main Footer -->
            @include('footer')

        </div><!-- ./wrapper -->

        <!-- REQUIRED JS SCRIPTS -->

        <!-- jQuery 2.1.3 -->
        <script src="{{ asset ('/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <script src="{{ asset ('/assets/JqueryUI/jquery-ui.js') }}"></script>
        <!-- Bootstrap 3.3.2 JS -->
        <script src="{{ asset ('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <!-- AdminLTE App -->
        <script src="{{ asset ('/bower_components/admin-lte/dist/js/adminlte.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/bower_components/admin-lte/dist/js/demo.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/js/utility.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/js/sweetalert.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset ('/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js') }}" type="text/javascript"></script>
        <script type="text/javascript" src="{{ asset("/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("/js/validator.js") }}"></script>

        <!--<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>-->
        @yield('scripts')
        <!-- Optionally, you can add Slimscroll and FastClick plugins.
              Both of these plugins are recommended to enhance the
              user experience -->
    </body>
    
    @if (Session::has("message") && Session::has("message-type"))
        <script type="text/javascript">
            $.notify({
                // options
                message: "{{ Session::get("message") }}"
            },{
                // settings
                type: '{{ Session::get("message-type") }}',
                placement:
                {
                    from: "bottom",
                    align: "right"
                }
            });   
        </script>
    @endif
    @yield('nonShown')
</html>
@else
    <script type="text/javascript">
        window.location = "{{ url('/login') }}";//here double curly bracket
    </script>


@endauth