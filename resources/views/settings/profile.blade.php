@extends('layouts.admin')

@section('mainContent')
<?php $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first(); ?>


<div class="modal fade" id="delete-confirm-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body align-center padded-container">
       	<h4>Confirmation</h4>
       	<p>Are you sure you want to delete your account permanently?</p>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-primary" href="/settings/delete/account">YES</a>
        <a type="button" class="btn btn-danger" data-dismiss="modal">NO</a>
      </div>
    </div>
  </div>
</div>

    <div class="container" style="margin-top:50px;">

    	<div class="row">
    		<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
    			
    			<div class="box">
		    		<div class="box-body" style="padding-bottom:20px;">
		    			<h4 class="margin-bottom-2x">Profile Settings</h4>

		    			<form action="/settings/save/profile" method="POST" enctype="multipart/form-data">
		    				{{ csrf_field() }}

		    				<label for="name">Your name</label>
		    				<input placeholder="John Doe" type="text" id="name" class="form-control rounded margin-bottom" name="name" value="{{ $user->name }}">
{{-- 
		    				<label for="name">Change profile picture</label>
		    				<input placeholder="johndoe@example.com" type="file" id="email" class="form-control rounded margin-bottom" name="email" value="{{ $user->email }}"> --}}

		    				<button type="submit" class="btn btn-primary">Save Settings</button>
		    			</form>
		    		</div>
		    	</div>

    			<div class="box">
		    		<div class="box-body" style="padding-bottom:20px;">
		    			<h4 class="margin-bottom">Administrative Decisions</h4>
		    			
		    			<table class="settings-table">
		    				<tr>
		    					<td class="align-left">Delete your Dilshani Travels account permenantly</td>
		    					<td class="align-right">
		    						<button class="btn btn-danger" data-toggle="modal" data-target="#delete-confirm-modal">Delete Account</button>
		    					</td>
		    				</tr>
		    			</table>
		    		</div>
		    	</div>
    		</div>

    		<div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
    			
    			<div class="box">
		    		<div class="box-body" style="padding-bottom:20px;">
		    			<h4 class="margin-bottom-2x">Change Password</h4>

		    			<form id="password_changer" action="/settings/save/password" method="POST" enctype="multipart/form-data">
		    				{{ csrf_field() }}
		    				
		    				<label for="current_password">Current Password</label>
		    				<input placeholder="Enter your current password here" type="password" id="current_password" class="form-control rounded margin-bottom" name="current_password" value="">

		    				<label for="new_password">New Password</label>
		    				<input placeholder="Enter a new password" type="password" id="new_password" class="form-control rounded margin-bottom" name="new_password" value="">

		    				<label for="confirm_password">Confirm Password</label>
		    				<input placeholder="Enter your password again to confirm it" type="password" id="confirm_password" class="form-control rounded margin-bottom" name="confirm_password" value="">

		    				<button type="submit" class="btn btn-primary">Change Password</button>
		    			</form>
		    		</div>
		    	</div>

    		</div>
    	</div>    

    </div>
@stop
@section('scripts')
	@if (Session::has("message") && Session::has("message-type"))
	    <script type="text/javascript">
	        $.notify({
	            // options
	            message: "{{ Session::get("message") }}"
	        },{
	            // settings
	            type: '{{ Session::get("message-type") }}',
	            placement:
	            {
	                from: "bottom",
	                align: "right"
	            }
	        });   
	    </script>
	@endif

	<script type="text/javascript">
		$("#password_changer").submit(function(e)
		{
			var current = $("#current_password").val();
			var newPass = $("#new_password").val();
			var confirm = $("#confirm_password").val();

			if (current.replace(" ", "").length != 0 && newPass.replace(" ", "").length != 0 && confirm.replace(" ", "").length != 0)
			{
				if (newPass != confirm)
				{
				 	$.notify({
			            // options
			            message: "Your password confirmation does not match the new password"
			        },{
			            // settings
			            type: 'danger',
			            placement:
			            {
			                from: "bottom",
			                align: "right"
			            }
			        });

			        e.preventDefault();
				}
			} else {

				if (current.replace(" ", "").length == 0)
					$("#current_password").addClass("invalid");
				if (newPass.replace(" ", "").length == 0)
					$("#new_password").addClass("invalid");
				if (confirm.replace(" ", "").length == 0)
					$("#confirm_password").addClass("invalid");

			 	$.notify({
		            // options
		            message: "Do not leave the fields empty"
		        },{
		            // settings
		            type: 'danger',
		            placement:
		            {
		                from: "bottom",
		                align: "right"
		            }
		        });

		        e.preventDefault();
			}
		});
	</script>
@stop