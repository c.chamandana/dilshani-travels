<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToSeatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('seats', function(Blueprint $table)
		{
			$table->foreign('bus_id', 'fk_seats_bus1')->references('id')->on('bus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('seats', function(Blueprint $table)
		{
			$table->dropForeign('fk_seats_bus1');
		});
	}

}
