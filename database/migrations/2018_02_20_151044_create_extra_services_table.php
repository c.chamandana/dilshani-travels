<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateExtraServicesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('extra_services', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('user_id')->unsigned()->index('UserConstraint');
			$table->string('name');
			$table->string('phone');
			$table->string('email');
			$table->string('type');
			$table->string('bus')->nullable();
			$table->string('start_location')->nullable();
			$table->string('end_location')->nullable();
			$table->string('description', 1000);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('extra_services');
	}

}
