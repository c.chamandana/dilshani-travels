<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
<title>{{ config("app.description") }} | {{ config("app.name") }}</title>

<link href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css" />
<link href="{{ asset('/assets/JqueryUI/jquery-ui.css')}}" rel="stylesheet" type="text/css" />

<script src="{{ asset ('/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset ('/assets/JqueryUI/jquery-ui.js') }}"></script>
<script src="{{ asset ('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/bower_components/datatables.net/js/jquery.dataTables.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset ('/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>

<script src="{{ asset ('/assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset ('/assets/js/contact.js') }}"></script>
<script src="{{ asset ('/assets/js/jquery.flexslider.js') }}"></script>

<script type="text/javascript" src="{{ asset("/bower_components/moment/min/moment.min.js") }}"></script>
<script type="text/javascript" src="{{ asset("/bower_components/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js") }}"></script>
<script type="text/javascript" src="{{ asset("/bower_components/bootstrap-typeahead/bootstrap-typeahead.js") }}"></script>

<script src="{{ asset ('/bower_components/select2/dist/js/select2.min.js') }}" type="text/javascript"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lobster+Two">

<link rel="stylesheet" href="{{ asset('/assets/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/owl.carousel.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/owl.theme.default.css') }}">
<link rel="stylesheet" href="{{ asset('/assets/css/flexslider.css') }}" type="text/css">

<!--<link rel="stylesheet" href="{{ asset('/assets/css/main.css') }}">-->
<!--<link rel="stylesheet" type="text/css" href="/maincss">-->
@include('Front.css.main')

<link href="https://fonts.googleapis.com/css?family=Rubik" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

@include('Front.css.section')
<link href="{{ asset('/bower_components/select2/dist/css/select2.min.css') }}" rel="stylesheet" type="text/css"/>
<link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css") }}">
<link rel="stylesheet" type="text/css" href="{{ asset("/bower_components/animate/animate.css") }}">
<script type="text/javascript" src="{{ asset("/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js") }}"></script>

<link rel="icon" href="{{ asset("/assets/images/icon.png") }}">