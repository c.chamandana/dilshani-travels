@extends('layouts.admin')

@section('mainContent')
    <div class="container edit-content-container">
        <table id="contenttbl" class="table table-striped table-bordered" width='100%'>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Content</th>
                    <th>Page</th>
                    <th>IsImage</th>
                    <th>IsColor</th>
                </tr>
            </thead>
        </table>
    </div>
@stop
@section('nonShown')
<div id="dialog-form" title="Edit This Content">
    <form>
        <fieldset>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="id" id='id'/>
            <label for="name">Name</label>
            <input name="name" id="name"  class="text ui-widget-content ui-corner-all" disabled="">
            <label for="content">Content</label>
            <textarea name="content" id="content"  class="text ui-widget-content ui-corner-all"></textarea>
            <label for="page">Page</label>
            <input name="page" id="page"  class="text ui-widget-content ui-corner-all" disabled="">

            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>

<div id="color-form" title="Edit This Content">
    <form id="colorform">
        <fieldset>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="colorid" id='colorid'/>
            <label for="colorname">Name</label>
            <input name="colorname" id="colorname"  class="text ui-widget-content ui-corner-all" disabled="">
            <label for="colorcontent">Content</label>
            <input type="text" name="colorcontent" id="colorcontent" class="text ui-widget-content ui-corner-all">
            <label for="colorpage">Page</label>
            <input name="colorpage" id="colorpage"  class="text ui-widget-content ui-corner-all" disabled="">
            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>

<div id="img-dialog-form" title="Edit This Content">
    <form enctype="multipart/form-data" action="imageEdit" method="post" id="imgform">
        <fieldset>
            <?php echo csrf_field(); ?>
            <input type="hidden" name="imgid" id='imgid'/>
            <label for="imgname">Name : <span name="imgname" id="imgname"></span></label>
            <br>
            <label for="imgcontent">Content : <span name="imgcontent" id="imgcontent"></span></label>
            <br>
            <label for="imgpage" >Page : <span name="imgpage" id="imgpage"></span></label>
            <br>
            <!--<input name="imgpage" id="imgpage"  class="text ui-widget-content ui-corner-all" disabled="">-->
            <label for="imgpage">Image</label>
            <input name="imgimage" id="imgimage"  class="text ui-widget-content ui-corner-all" type="file">
            <img src="#" id="newimg"/>

            <!-- Allow form submission with keyboard without duplicating the dialog button -->
            <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
        </fieldset>
    </form>
</div>
@stop
@section('scripts')
<script>
    $(document).ready(function () {
        var dialog, form, imagedialog, imgform, colordialog, colorform;
        dialog = $("#dialog-form").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Edit": function () {
                    edit();
                    dialog.dialog("close");
                },
                Cancel: function () {
                    dialog.dialog("close");
                }
            },
            close: function () {
                form[ 0 ].reset();
                dialog.dialog("close");
            }
        });
        $("#imgimage").change(function (e) {
            readURL(this, e);
        });
        imagedialog = $("#img-dialog-form").dialog({
            autoOpen: false,
            height: 600,
            width: 950,
            modal: true,
            buttons: {
                "Edit": function () {
                    editImage();
                    imagedialog.dialog("close");
                },
                Cancel: function () {
                    $("#imgform")[ 0 ].reset();
                    imagedialog.dialog("close");
                }
            },
            close: function () {
                $("#imgform")[ 0 ].reset();
                imagedialog.dialog("close");
            }
        });
        colordialog = $("#color-form").dialog({
            autoOpen: false,
            height: 400,
            width: 350,
            modal: true,
            buttons: {
                "Edit": function () {
                    editColor();
                    colordialog.dialog("close");
                },
                Cancel: function () {
                    $("#colorform")[ 0 ].reset();
                    colordialog.dialog("close");
                }
            },
            close: function () {
                $("#colorform")[ 0 ].reset();
                colordialog.dialog("close");
            }
        });
        form = dialog.find("form").on("submit", function (event) {
            event.preventDefault();
            dialog.dialog("close");
        });
        var table = $("#contenttbl").DataTable({
            "searching": true,
            paging: true,
            pageLength: 25,
            "ajax": {
                "url": "/content"
            },
//            "columnDefs": [],
            "columns": [
                {"data": "id", 'title': '#'},
                {"data": "contentname", 'title': 'Name'},
                {"data": "content", 'title': 'Content'},
                {"data": "pagename", 'title': 'Page'},
                {"data": "isimage", 'title': 'Is Image'},
                {"data": "iscolor", 'title': 'Is Color'}
            ],
            'rowCallback': function (row, data, index) {

            },
            "order": [[0, "asc"]]
        });
        $('#contenttbl tbody').on('click', 'tr', function () {
            var data = table.row(this).data();
            if (data['isimage'] === 1) {
                $("#imgid").val(data['id']);
                $("#imgname").text(data['contentname']);
                $("#imgcontent").text(data['content']);
                $("#imgpage").text(data['pagename']);
                $("#newimg").attr("src", data['content']);
                imagedialog.dialog("open");
            } else if (data['iscolor'] === 1) {
                $("#colorid").val(data['id']);
                $("#colorname").val(data['contentname']);
//                $("#colorcontent").text(data['content']);
                $("#colorcontent").colorpicker({
                    "color": data['content']
                });
                $("#colorpage").val(data['pagename']);
                colordialog.dialog("open");
            } else {
                $("#id").val(data['id']);
                $("#name").val(data['contentname']);
                $("#content").val(data['content']);
                $("#page").val(data['pagename']);
                dialog.dialog("open");
            }
        });
    }
    );
    function edit() {
        $.post("contentEdit",
                {
                    _token: $('input[name=_token]').val(),
                    id: $("#id").val(),
                    name: $("#name").val(),
                    content: $("#content").val(),
                    page: $("#page").val()
                },
                function (data, status) {
                    location.reload();
                });
    }
    function editColor() {
        $.post("contentEdit",
                {
                    _token: $('input[name=_token]').val(),
                    id: $("#colorid").val(),
                    name: $("#colorname").val(),
                    content: $("#colorcontent").val(),
                    page: $("#colorpage").val()
                },
                function (data, status) {
                    location.reload();
                });
    }
    function editImage() {
        $("#imgform").submit();
//        $.post("imageEdit",
//                {
//                    _token: $('input[name=_token]').val(),
//                    id: $("#imgid").val(),
//                    name: $("#imgname").val(),
//                    content: $("#imgcontent").val(),
//                    page: $("#imgpage").val()
//                },
//                function (data, status) {
//                    location.reload();
//                });
    }
    function readURL(input, event) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                var fileName = event.target.files[0].name;
                $("#imgcontent").text("/assets/images/" + fileName);
                $('#newimg').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
@stop
@section('styles')
<style>
    input.text { margin-bottom:12px; width:95%; padding: .4em; }
    textarea { margin-bottom:12px; width:95%; padding: .4em; }
    fieldset { padding:0; border:0; margin-top:25px; }
    .ui-dialog .ui-state-error { padding: .3em; }
    .validateTips { border: 1px solid transparent; padding: 0.3em; }
</style>
@stop