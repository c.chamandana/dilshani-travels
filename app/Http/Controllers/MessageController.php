<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Message;

class MessageController extends Controller
{
    public function save(Request $request)
    {
    	// Create a new message
    	$message = new Message;
    	
    	// Set the infromation from the input
    	$message->name = $request->input("name");
    	$message->email = $request->input("email");
    	$message->phone = $request->input("phone");
    	$message->message = $request->input("message");

    	// Save the message in the database
    	$message->save();

    	// Redirect the user to the homepage
    	return redirect('/')->with([
    		"message" => "Your message has been submitted",
    		"message-type" => "info"
    	]);
    }
}
