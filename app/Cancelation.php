<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $booking_id
 * @property int $cancelation_fees_id
 * @property int $customer_id
 * @property string $canceled_at
 * @property Booking $booking
 * @property CancelationFee $cancelationFee
 * @property Customer $customer
 */
class Cancelation extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'cancelation';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['booking_id', 'cancelation_fees_id', 'customer_id', 'canceled_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cancelationFee()
    {
        return $this->belongsTo('App\CancelationFee', 'cancelation_fees_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }
}
