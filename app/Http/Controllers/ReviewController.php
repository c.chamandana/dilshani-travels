<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use App\Review;

class ReviewController extends Controller
{
    public function getDetails(Request $request, $id)
    {
    	$review = Review::where("id", $id)->first(); // Get the review

    	echo json_encode([
    		"name" => $review->name,
    		"position" => $review->position,
    		"rating" => $review->stars,
    		"sentence" => $review->sentence,
    		"image" => $review->image
    	]);
    }

    public function create(Request $request)
    {
    	$name = ($request->name);
    	$position = ($request->position);
    	$sentence = ($request->sentence);
    	$rating = ($request->rating);
        $image = $request->image;

        $review = new Review;
        $review->name = $name;
        $review->position = $position;
        $review->stars = $rating;
        $review->image = $image;
        $review->sentence = $sentence;

        $review->save();

        // Return to the reviews section
        Session::flash("message", "Review added successfully");
        Session::flash("message-type", "info");
        
        return redirect("/showReviews");
    }

    public function save(Request $request, $id)
    {
        $review = Review::where("id", $id)->first();

        if ($review)
        {
            $name = ($request->name);
            $position = ($request->position);
            $sentence = ($request->sentence);
            $rating = ($request->rating);
            $image = $request->image;

            $review->name = $name;
            $review->position = $position;
            $review->stars = $rating;
            $review->image = $image;
            $review->sentence = $sentence;

            $review->save();

            // Return to the reviews section
            Session::flash("message", "Review saved successfully");
            Session::flash("message-type", "info");

            return redirect("/showReviews");
        }else {
            // Return to the reviews section
            return redirect("/showReviews");
        }
    }

    public function delete(Request $request, $id)
    {
        // Delete the reviews
        $review = Review::where("id", $id)->first();

        if ($review)
            $review->delete();

        // Return to the reviews section
        Session::flash("message", "Review deleted successfully");
        Session::flash("message-type", "info");

        return redirect("/showReviews");
    }
}
