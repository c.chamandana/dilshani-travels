@extends('layouts.admin')

@section('mainContent')
    <div class="container messages-container">
    	<div class="row">
    		<div class="col col-lg-8 col-md-6 col-sm-12 col-xs-12">
    			<h4 class="sector-title" style="margin-bottom:25px">Problems related to Buses</h4>

    			@if (count(\App\Report::where("problem_type", "bus_problem")->get()) > 0)
	    			<div id="accordion">

						@foreach (\App\Report::where("problem_type", "bus_problem")->get() as $problem)
							<div class="card">
								<div class="card-header" id="headingOne">
									<h5 class="mb-0">
										<button class="btn-link" data-toggle="collapse" data-target="#collapse{{$problem->id}}" aria-expanded="true" aria-controls="collapseOne">
											{{ substr($problem->description, 0, 25) . "..."}}
										</button>

										<span class="time pull-right">{{ Carbon\Carbon::parse($problem->created_at)->format("d-m-Y H:i") }}</span>
									</h5>

								</div>

								<div id="collapse{{$problem->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<span class="message-meta">From : {{ $problem->name }} </span>
										<span class="message-meta">Phone : {{ $problem->phone }} </span>

										<span class="message">{{ $problem->description }}</span>
									</div>
								</div>
							</div>
						@endforeach

					</div>
				@else

					<div class="no-messages-container no-padd">
						No problems found about buses						
					</div>

				@endif

				<h4 class="sector-title" style="margin-top:30px;margin-bottom:25px">Problems related to the website</h4>

    			@if (count(\App\Report::where("problem_type", "website_problem")->get()) > 0)
	    			<div id="accordion">

						@foreach (\App\Report::where("problem_type", "website_problem")->get() as $problem)
							<div class="card">
								<div class="card-header" id="headingOne">
									<h5 class="mb-0">
										<button class="btn-link" data-toggle="collapse" data-target="#collapse{{$problem->id}}" aria-expanded="true" aria-controls="collapseOne">
											{{ substr($problem->description, 0, 25) . "..."}}
										</button>

										<span class="time pull-right">{{ Carbon\Carbon::parse($problem->created_at)->format("d-m-Y H:i") }}</span>
									</h5>

								</div>

								<div id="collapse{{$problem->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<span class="message-meta">From : {{ $problem->name }} </span>
										<span class="message-meta">Phone : {{ $problem->phone }} </span>

										<span class="message">{{ $problem->description }}</span>
									</div>
								</div>
							</div>
						@endforeach

					</div>
				@else

					<div class="no-messages-container no-padd">
						No problems found about website		
					</div>

				@endif

				<h4 class="sector-title" style="margin-top:30px;margin-bottom:25px">Problems related to the website</h4>

    			@if (count(\App\Report::where("problem_type", "customer_service_problem")->get()) > 0)
	    			<div id="accordion">

						@foreach (\App\Report::where("problem_type", "customer_service_problem")->get() as $problem)
							<div class="card">
								<div class="card-header" id="headingOne">
									<h5 class="mb-0">
										<button class="btn-link" data-toggle="collapse" data-target="#collapse{{$problem->id}}" aria-expanded="true" aria-controls="collapseOne">
											{{ substr($problem->description, 0, 25) . "..."}}
										</button>

										<span class="time pull-right">{{ Carbon\Carbon::parse($problem->created_at)->format("d-m-Y H:i") }}</span>
									</h5>

								</div>

								<div id="collapse{{$problem->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<span class="message-meta">From : {{ $problem->name }} </span>
										<span class="message-meta">Phone : {{ $problem->phone }} </span>

										<span class="message">{{ $problem->description }}</span>
									</div>
								</div>
							</div>
						@endforeach

					</div>
				@else

					<div class="no-messages-container no-padd">
						No problems found about Customer Service 						
					</div>

				@endif
    		</div>

    		<div class="col col-lg-4 col-md-6 col-sm-12 col-xs-12">
    			<div class="box">
				    <div class="box-body" style="padding-bottom:20px;">
				        <h4 class="sector-title">Flags / Reports</h4>

				        <p>There are 3 types of problems reported by users,</p>

				        <ol>
				        	<li>Problems related to Buses</li>
				        	<li>Problems related to the Website</li>
				        	<li>Problems related to Customer Service</li>
				        </ol>
				    </div>
				</div>

				<div class="box red">
				    <div class="box-body red" style="padding-bottom:20px;">
				        <h4 class="sector-title">HELP</h4>

				       	<p>Click on a Reported problem to expand it's content and view Message sender, sender's email and then take action</p>
				    </div>
				</div>
    		</div>
    	</div>
		

    </div>
@stop

@section('nonShown')

@stop
@section('scripts')
	<script type="text/javascript">
		$('.collapse').collapse('hide')
	</script>
@stop
@section('styles')
    
@stop