<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHirerateTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hirerate', function(Blueprint $table)
		{
			$table->increments('id');
			$table->decimal('Rate', 10)->nullable();
			$table->text('description', 65535)->nullable();
			$table->decimal('defaultmilage', 7)->nullable();
			$table->decimal('defaultrate', 10)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hirerate');
	}

}
