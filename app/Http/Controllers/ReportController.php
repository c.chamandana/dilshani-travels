<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Report;
use Session;

class ReportController extends Controller
{
    public function submit(Request $request)
    {
    	$name = $request->name;
    	$description = $request->description;
    	$problem_type = $request->problem_type;
    	$phone = $request->phone;

    	$report = new Report;

    	$report->name = $name;
    	$report->description = $description;
    	$report->problem_type = $problem_type;
    	$report->phone = $phone;

    	$report->save();

    	Session::flash("message", "Your report has been sent");
    	Session::flash("message-type", "info");
    	return redirect("/");
    }
}
