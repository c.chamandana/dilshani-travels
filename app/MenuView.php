<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $menuname
 * @property string $url
 * @property int $level
 * @property int $parentmenu
 * @property string $icon
 * @property boolean $admin
 */
class MenuView extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'menuview';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['menuname', 'url', 'level', 'parentmenu', 'icon', 'admin'];

}
