@extends('layouts.admin')
@section('mainContent')
<table id="tblusersummary" class="table table-striped table-bordered" width='100%'>
    <thead>
        <tr>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <!--<th></th>-->
        </tr>
    </thead>
    <tbody></tbody>
</table>
@stop
@section('scripts')
<script>
    $(document).ready(function () {
        var table = $("#tblusersummary").DataTable({
            "searching": false,
            paging: true,
            "ajax": {
                "url": "/getUserHistorySummary/" + <?php echo $userID; ?>},
            "columns": [
                {"data": "id", 'title': 'ID'},
                {"data": "ref", 'title': 'Ref. No.'},
                {"data": "booked_date", "title": "Booked Date"},
                {"data": "booking_date", 'title': 'Booking Date'},
                {"data": "from", 'title': 'From'},
                {"data": "to", 'title': 'To'},
                {"data": "bkstat", 'title': 'Booking Status'},
                {"data": "bus_name", 'title': 'Bus Name'},
                {"data": "bus_number", 'title': 'Bus No.'}
//                {"data": "action", 'title': 'Action'},
            ],
            "columnDefs": [
//                {
//                    "targets": -1,
//                    "data": null,
//                    "defaultContent": '<form action="/seating" method="get">\n\
//<input type="hidden" name="ttID"/>\n\
//<input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
//<div class="btn-group">\n\
//<button type="submit" class="btn btn-primary" value="Book">\n\
//Book\n\
//</button>\n\
//</div>\n\
//<form>',
//                    "className": "text-center"
//                }
                 {
                    "targets": 0,
                    "visible": false
                }
            ]
        });
    });
</script>
@stop