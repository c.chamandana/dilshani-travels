<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $booking_status_id
 * @property int $customer_id
 * @property int $payment_status_id
 * @property int $time_table_id
 * @property string $ref
 * @property int $adults
 * @property int $children
 * @property string $booked_date
 * @property string $booking_date
 * @property BookingStatus $bookingStatus
 * @property Customer $customer
 * @property PaymentStatus $paymentStatus
 * @property TimeTable $timeTable
 * @property BookingHasSeat[] $bookingHasSeats
 * @property Cancelation[] $cancelations
 */
class Booking extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'booking';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['booking_status_id', 'customer_id', 'payment_status_id', 'time_table_id', 'ref', 'adults', 'children', 'booked_date', 'booking_date', "end_date"];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookingStatus()
    {
        return $this->belongsTo('App\BookingStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function paymentStatus()
    {
        return $this->belongsTo('App\PaymentStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function timeTable()
    {
        return $this->belongsTo('App\TimeTable');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingHasSeats()
    {
        return $this->hasMany('App\BookingHasSeats');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cancelations()
    {
        return $this->hasMany('App\Cancelation');
    }
}
