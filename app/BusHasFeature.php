<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $bus_id
 * @property int $feature_id
 * @property Bus $bus
 * @property Feature $feature
 */
class BusHasFeature extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bus_has_feature';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = [];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function feature()
    {
        return $this->belongsTo('App\Feature');
    }
}
