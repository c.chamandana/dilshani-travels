<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('ref', 45)->nullable()->unique('ref_UNIQUE');
			$table->integer('time_table_id')->index('fk_booking_time_table1_idx');
			$table->integer('customer_id')->index('fk_booking_customer1_idx');
			$table->integer('adults')->nullable()->default(0);
			$table->integer('children')->nullable()->default(0);
			$table->integer('booking_status_id')->index('fk_booking_booking_status1_idx');
			$table->integer('payment_status_id')->index('fk_booking_payment_status1_idx');
			$table->dateTime('booked_date');
			$table->dateTime('booking_date');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking');
	}

}
