<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingHasSeatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking_has_seats', function(Blueprint $table)
		{
			$table->foreign('booking_id', 'fk_booking_has_seats_booking1')->references('id')->on('booking')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('seats_id', 'fk_booking_has_seats_seats1')->references('id')->on('seats')->onUpdate('NO ACTION')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking_has_seats', function(Blueprint $table)
		{
			$table->dropForeign('fk_booking_has_seats_booking1');
			$table->dropForeign('fk_booking_has_seats_seats1');
		});
	}

}
