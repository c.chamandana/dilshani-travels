<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
    <!--<![endif]-->
    <head>
        @include('Front.head')
        @include('Front.css.about')
    </head>
    <body>

        <!-- Home -->
        <section class="header">
            @include('Front.navbar')
        </section> <!-- /#header -->

        <!-- Section Background -->
        <section class="section-background">
            <div class="container">
                <h2 class="page-header">
                    {{ \App\SiteContent::find(46)->content }}
                </h2>
                <ol class="breadcrumb">
                    <li><a href="/">&nbsp;</a></li>
                    <li class="active">&nbsp;</li>
                </ol>
            </div> <!-- /.container -->
        </section> <!-- /.section-background -->


        <!-- Who we are -->
        <section class="wwa section-wrapper">
            <div class="container">
                <h2 class="section-title">
                    {{ \App\SiteContent::find(47)->content }}
                </h2>
                <p class="section-subtitle">
                    {{ \App\SiteContent::find(48)->content }}
                </p>
                <div class="row">
                    <div class="col-sm-3 col-xs-6">
                        <div class="who">
                            <img src="{{ asset(\App\SiteContent::find(49)->content) }}" alt="" class="img-responsive who-img">
                            <h3>
                                {{ \App\SiteContent::find(50)->content }}
                            </h3>
                            <p class="who-detail">
                                {{ \App\SiteContent::find(51)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-sm-3 -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="who">
                            <img src="{{ asset(\App\SiteContent::find(52)->content) }}" alt="" class="img-responsive who-img">
                            <h3>
                                {{ \App\SiteContent::find(53)->content }}
                            </h3>
                            <p class="who-detail">
                                {{ \App\SiteContent::find(54)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-sm-3 -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="who">
                            <img src="{{ asset(\App\SiteContent::find(55)->content) }}" alt="" class="img-responsive who-img">
                            <h3>
                                {{ \App\SiteContent::find(56)->content }}
                            </h3>
                            <p class="who-detail">
                                {{ \App\SiteContent::find(57)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-sm-3 -->
                    <div class="col-sm-3 col-xs-6">
                        <div class="who">
                            <img src="{{ asset(\App\SiteContent::find(58)->content) }}" alt="" class="img-responsive who-img">
                            <h3>
                                {{ \App\SiteContent::find(59)->content }}
                            </h3>
                            <p class="who-detail">
                                {{ \App\SiteContent::find(60)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-sm-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.wwa -->

        <!-- Story and Client -->
        <section class="story-and-client section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="story">
                            <h2 class="section-title">
                                {{ \App\SiteContent::find(61)->content }}
                            </h2>
                            <img src="{{ asset(\App\SiteContent::find(62)->content) }}" alt="story" class="story-img">
                            <p class="story-detail">
                                {{ \App\SiteContent::find(63)->content }}
                            </p>
                            <p class="story-detail">
                                {{ \App\SiteContent::find(64)->content }}
                            </p>
                        </div> <!-- /.story -->
                    </div> <!-- /.col-sm-6 -->

                    <div class="col-sm-6">
                        <div class="story">
                            <h2 class="section-title">
                                {{ \App\SiteContent::find(65)->content }}
                            </h2>
                            <table class="table _table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            5 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(66)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(66)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(66)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(67)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            4 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(68)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(68)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(68)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(69)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            3 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(70)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(70)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(70)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(71)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            2 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(72)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(72)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(72)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(73)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            1 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(74)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(74)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(74)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(75)->content }}
                                        </td>
                                    </tr>

                                </tbody>
                            </table> <!-- /.table -->
                        </div> <!-- /.story -->
                    </div> <!-- /.col-sm-6 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.story-and-client -->

        @include('Front.subscribe')

        <script src="{{ asset('/assets/js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset('/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset('/assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset('/assets/js/contact.js') }}"></script>
        <script src="{{ asset('/assets/js/script.js') }}"></script>






    </body>
</html>