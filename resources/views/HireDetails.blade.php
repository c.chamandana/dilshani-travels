@extends('layouts.admin')

@section('mainContent')
<div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Please Enter Your Details</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" method="post" action="{{ route('saveHire') }}">
              <div class="box-body">
                <div class="form-group">
                    <input type="hidden" name="busID" id="busID"/>
                    {{ csrf_field() }}
                    <input type="hidden" name="cusID" id="cusID" value="<?php echo \App\Customer::where("users_id","=" ,\Illuminate\Support\Facades\Auth::user()->id)->get()[0]->id;?>"/>
                  <label for="FromDate">From Date </label>
                  <input type="date" class="form-control" id="FromDate" name="FromDate">
                </div>
                <div class="form-group">
                  <label for="ToDate">To Date </label>
                  <input type="date" class="form-control" id="ToDate" name="ToDate">
                </div>
                <div class="form-group">
                  <label for="start">Start</label>
                  <input type="text" id="start" name="start" class="form-control">
                </div>
                  <div class="form-group">
                  <label for="destination">Destination</label>
                  <input type="text" id="destination" name="destination" class="form-control"/>
                </div>
                  <div class="form-group">
                  <label for="km">Estimated Distance(km)</label>
                  <input type="text" id="km" name="km" class="form-control"/>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-body">
                <button type="submit" class="btn btn-primary">Send Request</button>
            </div>
            </form>
          </div>
          <!-- /.box -->

        </div>
@stop
@section('scripts')
<script>
$(document).ready(function() {
   $("#busID").val($.urlParam("busID")); 
});
</script>
@stop