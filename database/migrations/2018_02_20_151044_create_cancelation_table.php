<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCancelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('cancelation', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->dateTime('canceled_at')->nullable();
			$table->integer('booking_id')->index('fk_cancelation_booking1_idx');
			$table->integer('cancelation_fees_id')->index('fk_cancelation_cancelation_fees1_idx');
			$table->integer('customer_id')->index('fk_cancelation_customer1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('cancelation');
	}

}
