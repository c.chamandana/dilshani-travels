<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use \Illuminate\Database\Eloquent\Model;
/**
 * Description of Role
 *
 * @author User
 */
class Role extends Model{
    /**
       * Set timestamps off
       */
      public $timestamps = false;
  
     /**
      * Get users with a certain role
      */
     public function users()
     {
         return $this->belongsToMany('User', 'users_roles');
     }
}
