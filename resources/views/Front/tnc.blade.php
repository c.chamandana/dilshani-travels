<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
    <!--<![endif]-->
    <head>
        @include('Front.head')
        @include('Front.css.about')
        @include('Front.css.contact')
        @include('Front.css.services')

        {{-- Google Maps API --}}
        <script
            src="http://maps.googleapis.com/maps/api/js">
        </script>
        <style>
            tfoot input {
                width: 100%;
                padding: 3px;
                box-sizing: border-box;
            }
        </style>

        <!-- AOS Scrolling animations -->
        <link rel="stylesheet" href="{{ asset('/bower_components/aos/dist/aos.css') }}">
    </head>
    <body>

         @include('Front.navbar')

         {{-- CONTAINS REPORTING FORM --}}
        <div class="tnc-container container">
            <h4>Terms and Conditions</h4>
            <span class="dilshani-travels-text">Dilshani Travels</span>

            <p>Our Service is regulated with following terms and conditions to provide a better passenger experience and to develop a well regulated and disciplined service with passenger transport facility providers and transport facility staff.</p>

            <h5>Ground Rules</h5>

            <p>By using BusBooking.lk services you agree to be bound by the terms and conditions shown below. If you do not agree our terms and conditions we are not in a position to provide you our services and we do apologize for that. Our ground rules are subject to change at any time without a prior notice. To make sure you are aware of any changes, please review this section periodically.</p>

            <p>
                The passenger should present in the bus station, 15 minutes before the scheduled departure time of the reserved bus service.
                BusBooking.lk is not liable for any mis-communications, delays of the passenger or any misleading and/or fake informations provided by the passenger.
            </p>
            <p>
                BusBooking.lk is reserve rights to change the schedule and/or cancel, terminate, postpone, divert, reschedule or delay any bus/coach, anytime after booking without a prior notice. This may be due to safety or reasonably considered and justified circumstances beyond our control.
            </p>

            <p>
                BusBooking.lk will not refund cash under general circumstances after a booking cancellation. But the passenger will be able to get the full refund as an eCash coupon, if he/she cancelled the booking 24 hours prior to the journey. Passenger should provide the eTicket number/the booking confirmation SMS to the bus service staff to verify the identity of the prepaid customer. Additionally, passenger should ready to provide their national identity card for relevant authorities to check the identity at security incidents.
            </p>
            <p>
                BusBooking.lk is not liable for any delays before and/or during the jouney & accidents and/or during the jouney that prepaid customer might have to face.
            </p>

            <h5>Nature of Business</h5>

            <p>BusBooking.lk is an online value added services provider. It does not operate bus services of its own. In order to provide a comprehensive choice of bus operators, departure times and prices to customers, it has tied up with many bus operators. We advice to customers is to choose bus operators they are aware of and whose service they are comfortable with.</p>

            <h5>Our responsibilities include:</h5>

            <p><span class="number">7.1</span> Issuing a valid ticket (a ticket that will be accepted by the bus operator) for our network of bus operators</p>
            <p><span class="number">7.2</span> Providing booking modification in the event of cancellation (prior to 24 hours of the journey)</p>
            <p><span class="number">7.3</span> Providing customer support and information in case of any delays/inconvenience</p>
    
            <h5>Our responsibilities do NOT include:</h5>  
            
            <p><span class="number">7.4</span> The bus operator’s bus not departing/reaching on time</p>

            <p><span class="number">7.4</span> The bus operator’s employees being rude and/or their discipline</p>

            <p><span class="number">7.4</span> The bus operator’s bus seats etc not being up to the customer’s expectation</p>

            <p><span class="number">7.7</span> The bus operator canceling the trip due to unavoidable reasons</p>

            <p><span class="number">7.8</span> The baggage of the customer getting lost/stolen/damaged</p>

            <p><span class="number">7.9</span> The bus operator changing a customer’s seat at the last minute to accommodate a separate passenger</p>

            <p><span class="number">7.10</span> The customer waiting at the wrong boarding point (please call the bus operator to find out the exact boarding point if you are not a regular traveler on that particular bus)</p>

            <p><span class="number">7.11</span> The bus operator changing the boarding point and/or using a pick-up vehicle at the boarding point to take customers to the bus departure point</p>


            <p><span class="number">8.</span> The arrival and departure times mentioned on the ticket are only tentative timings . However the bus will not leave the source before the time that is mentioned on the ticket.</p>


            <p><span class="number">9.</span> Passengers are required to furnish a copy of the ticket (mTicket or eTicket) and an Identity proof (Driving license, National Identity Card) at the time of boarding the bus. Failing to do so, they may not be allowed to board the bus.</p>

            <h5>Cancellation Policy</h5>

            <p><span class="number">10.</span> The tickets booked through BusBooking.lk, are cancelable. Kindly note that the money is not transferred back to the passenger's credit/debit card. Instead the money will be refunded in the form of a eCash Coupon which can be used during your next journey. This coupon is valid up to 6 months from the date of cancellation.</p>

            <p><span class="number">11.</span> In case you need the refund to be credited back to your bank account, please write your ecash coupon details to refund@busbooking.lk (The transaction charges or the home delivery charges, will not be refunded in the event of cash refund.)</p>


            <p><span class="number">12.</span> In case a booking confirmation e-mail and sms gets delayed or fails because of technical reasons or as a result of incorrect e-mail ID/phone number provided by the user etc, a ticket will be considered 'booked' as long as the ticket shows up on the confirmation page of www.busbooking.lk</p>


            <p><span class="number">13.</span> BusBooking.lk will not be liable to you or to any other person for any direct, indirect, incidental, punitive or consequential loss, damage, cost or expense of any kind whatsoever and howsoever caused from out of the information derived by you through your usage of this Site. In no event shall BusBooking.lk be liable for any direct, indirect, punitive, incidental, special, or consequential damages arising out of information provided on the website in so far as such information is derived from other service providers.</p>


            <h5>Communication Policy</h5>

            <p><span class="number">14.</span> By accepting the terms and conditions the customer accepts that BusBooking.lk may send the alerts to the mobile phone number provided by the customer while registering for the service or to any such number replaced and informed by the customer. The customer acknowledges that the alerts will be received only if the mobile phone is in ‘On’ mode to receive the SMS. If the mobile phone is in ‘Off’’ mode then the customer may not get/get after delay any alerts sent during such period.


            <p><span class="number">15.</span> BusBooking.lk will make best efforts to provide the service and it shall be deemed that the customer shall have received the information sent from BusBooking.lk as an alert on the mobile phone number provided during the course of ticket booking and BusBooking.lk shall not be under any obligation to confirm the authenticity of the person(s) receiving the alert. The customer cannot hold BusBooking.lk liable for non-availability of the service in any manner whatsoever.


            <p><span class="number">16.</span> The customer acknowledges that the SMS service provided by BusBooking.lk is an additional facility provided for the customer’s convenience and that it may be susceptible to error, omission and/ or inaccuracy. In the event the customer observes any error in the information provided in the alert, BusBooking.lk shall be immediately informed about the same by the customer and BusBooking.lk will make best possible efforts to rectify the error as early as possible. The customer shall not hold BusBooking.lk liable for any loss, damages, claim, expense including legal cost that may be incurred/ suffered by the customer on account of the SMS facility.


            <p><span class="number">17.</span> The customer acknowledges that the clarity, readability, accuracy, and promptness of providing the service depend on many factors including the infrastructure, connectivity of the service provider. BusBooking.lk shall not be responsible for any non-delivery, delayed delivery or distortion of the alert in any way whatsoever.


            <p><span class="number">18.</span> The customer agrees to indemnify and hold harmless BusBooking.lk and the SMS service provider including its officials from any damages, claims, demands, proceedings, loss, cost, charges and expenses whatsoever including legal charges and attorney fees which BusBooking.lk and the SMS service provider may at any time incur, sustain, suffer or be put to as a consequence of or arising out

            <p><span class="number">18.1</span>  misuse, improper or fraudulent information provided by the customer

            <p><span class="number">18.2</span>  the customer providing incorrect number or providing a number that belongs to that of an unrelated third party, and/or

            <p><span class="number">18.3</span>  the customer receiving any message relating to the reservation number, travel itinerary information, booking confirmation, modification to a ticket, cancellation of ticket, change in bus schedule, delay, and/or rescheduling from BusBooking.lk and/or the SMS service provider.


            <p><span class="number">19.</span> By accepting the terms and conditions the customer acknowledges and agrees that BusBooking.lk may call the mobile phone number provided by the customer while registering for the service or to any such number replaced and informed by the customer, for the purpose of collecting feedback from the customer regarding their travel, the bus facilities and/or services of the bus operator.


            <p><span class="number">20.</span> Grievances and claims related to the bus journey should be reported to BusBooking.lk support team within 3 days of your travel date.

            <h5>General Terms and Conditions</h5>

            <p style="margin-top:0">General Information and Conditions for passengers.</p>

            <p>Our BusTicke.lk is noticed of the bus operating services Island wide so that the passengers could book their tickets without any problems of staying at the terminals.</p>

            <p>The On-Line system is very easy and convenient for all travelers who could take this opportunity of the unique mode of booking tickets.</p>

            <p>The passengers who reserved the tickets through the online systems will be given notify by "eToursLanka" or "0777299522" SMS system where the particular bus conductor will be able to identify the passenger and issue a valid ticket.</p>

            <p>12 hours before the departure Time cancellation can be made by sell the ticket. Cash will be refund by reducing the followings charges. Service Charges, Credit / Debit Card, Easy Cash charges.</p>

            <p>After the expiry of 12 Hours before the departure time No refunds will be made.</p>

            <p>The passengers should take responsibility of all the baggage's and other personnel effects while on travel.</p>

            <p>The passengers who have booked their seats should be at the terminal at least 30 minimum of the departure of the bus.</p>
            </p>
        </div>

         @include('Front.footer')
         <script src="{{ asset ('/assets/js/script.js') }}"></script>
        <script src="{{ asset ('/assets/js/contact.js') }}"></script>
        <!-- AOS js -->
        <script src="{{ asset('/bower_components/aos/dist/aos.js') }}"></script>
        <script type="text/javascript" src="{{ asset("/bower_components/handlebars/handlebars.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("/js/validator.js") }}"></script>

        @if (Session::has("message") && Session::has("message-type"))
            <script type="text/javascript">
                $.notify({
                    // options
                    message: "{{ Session::get("message") }}"
                },{
                    // settings
                    type: '{{ Session::get("message-type") }}',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });   
            </script>
        @endif
     </body>

</html>