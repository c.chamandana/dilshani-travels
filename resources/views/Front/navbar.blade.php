<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <!--<i class="ion-android-bus"></i>-->
            <a class="navbar-brand" href="/" title="HOME"> <img height="40px" src="{{ asset(('/assets/images/logo1.png'))}}"></a>
        </div> <!-- /.navbar-header -->

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">
                <li {{ (Request::is('/') ? 'class=active' : '') }}><a href="/#about">About</a></li>
                <li {{ (Request::is('about') ? 'class=active' : '') }}><a href="/#destinations">Destinations</a></li>
                <li {{ (Request::is('services') ? 'class=active' : '') }}><a href="/#services">Extra services</a></li>
                <li {{ (Request::is('contact') ? 'class=active' : '') }}><a href="/#contact">contact</a></li>
                
                @auth
                <button class="log-btn btn btn-primary"><a href="/home" style="color: #FFF;">DASHBOARD</a></button>
                <button class="log-btn btn btn-primary" style="margin-left:5px !important"><a href="logout" style="color: #FFF">LOGOUT</a></button>
                @else
                <button class="log-btn btn btn-primary"><a href="login" style="color: #FFF">LOGIN</a></button>
                @endauth
                <!--<li class="active" style="background-color: #2565BA"><a href="login">login</a></li>-->
            </ul> <!-- /.nav -->
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container -->
</nav>