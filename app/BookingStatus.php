<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $status_name
 * @property Booking[] $bookings
 */
class BookingStatus extends Model {

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'booking_status';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['status_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings() {
        return $this->hasMany('App\Booking');
    }
    
    public function hires() {
        return $this->hasMany("App\hire");
    }

}
