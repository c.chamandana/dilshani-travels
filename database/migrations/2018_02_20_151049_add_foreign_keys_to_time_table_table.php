<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToTimeTableTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('time_table', function(Blueprint $table)
		{
			$table->foreign('bus_id', 'fk_time_table_bus1')->references('id')->on('bus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('day_of_week_id', 'fk_time_table_day_of_week1')->references('id')->on('day_of_week')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('end_time_id', 'fk_time_table_end_time1')->references('id')->on('end_time')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('from_bus_stand_id', 'fk_time_table_from_bus_stand')->references('id')->on('from_bus_stand')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('start_time_id', 'fk_time_table_start_time1')->references('id')->on('start_time')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('to_bus_stand_id', 'fk_time_table_to_bus_stand1')->references('id')->on('to_bus_stand')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('time_table', function(Blueprint $table)
		{
			$table->dropForeign('fk_time_table_bus1');
			$table->dropForeign('fk_time_table_day_of_week1');
			$table->dropForeign('fk_time_table_end_time1');
			$table->dropForeign('fk_time_table_from_bus_stand');
			$table->dropForeign('fk_time_table_start_time1');
			$table->dropForeign('fk_time_table_to_bus_stand1');
		});
	}

}
