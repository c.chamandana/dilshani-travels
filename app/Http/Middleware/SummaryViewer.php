<?php

namespace App\Http\Middleware;

use \App\BookingHasSeats;
use \App\Booking;
use \App\User;
use \App\Customer;
use \App\Seats;
use Session;
use Closure;

class SummaryViewer
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Get the booking
        $booking = Booking::where("id", $request->bookingID)->first();

        if ($booking)
        {
            // If the booking is valid
            $customer = Customer::where("id", $booking->customer_id)->first();

            if ($customer)
            {
                if ($customer->users_id === \Illuminate\Support\Facades\Auth::user()->id)
                {
                    // User owns the ticket
                    $seats = BookingHasSeats::where("booking_id", $booking->id)->get(); // Get the booking has seats information
                    
                    Session::flash("seats", $seats);
                    Session::flash("booking", $booking);

                } else {
                    // User does not own the ticket
                    redirect()->route("/home", [
                        "message" => "You do not own that ticket",
                        "message-type" => "danger"
                    ]);
                }
            }
        }

        return $next($request);
    }
}
