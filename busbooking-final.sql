-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 18, 2018 at 06:35 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `busbooking`
--

-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `id` int(11) NOT NULL,
  `ref` varchar(45) DEFAULT NULL,
  `time_table_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `adults` int(11) DEFAULT '0',
  `children` int(11) DEFAULT '0',
  `booking_status_id` int(11) NOT NULL,
  `payment_status_id` int(11) NOT NULL,
  `booked_date` datetime NOT NULL,
  `booking_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`id`, `ref`, `time_table_id`, `customer_id`, `adults`, `children`, `booking_status_id`, `payment_status_id`, `booked_date`, `booking_date`) VALUES
(3, 'Ref00000003', 4, 1, 0, 0, 4, 2, '2018-02-08 00:00:00', '2018-02-09 00:00:00'),
(4, 'Ref00000004', 3, 1, 0, 0, 4, 2, '2018-02-10 00:00:00', '2018-02-10 00:00:00'),
(5, 'Ref00000005', 3, 1, 0, 0, 4, 2, '2018-02-11 00:00:00', '2018-03-01 00:00:00'),
(8, 'Ref00000008', 3, 2, 0, 0, 5, 2, '2018-02-16 00:00:00', '2018-02-18 00:00:00'),
(9, 'Ref00000009', 3, 2, 0, 0, 4, 2, '2018-02-16 00:00:00', '2018-02-18 00:00:00'),
(10, 'Ref00000010', 3, 2, 0, 0, 4, 2, '2018-02-16 00:00:00', '2018-02-23 00:00:00'),
(11, 'Ref00000011', 3, 2, 0, 0, 4, 2, '2018-02-16 00:00:00', '2018-02-23 00:00:00'),
(12, 'Ref00000012', 5, 2, 0, 0, 4, 2, '2018-02-16 00:00:00', '2018-02-23 00:00:00'),
(13, 'Ref00000013', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-21 00:00:00'),
(14, 'Ref00000014', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-20 00:00:00'),
(15, 'Ref00000015', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-19 00:00:00'),
(16, 'Ref00000016', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-28 00:00:00'),
(17, 'Ref00000017', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-28 00:00:00'),
(18, 'Ref00000018', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-28 00:00:00'),
(19, 'Ref00000019', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-27 00:00:00'),
(20, 'Ref00000020', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-21 00:00:00'),
(21, 'Ref00000021', 3, 2, 0, 0, 4, 2, '2018-02-17 00:00:00', '2018-02-27 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `booking_has_seats`
--

CREATE TABLE `booking_has_seats` (
  `booking_id` int(11) NOT NULL,
  `seats_id` int(11) NOT NULL,
  `ticket_printed` tinyint(4) DEFAULT NULL,
  `id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_has_seats`
--

INSERT INTO `booking_has_seats` (`booking_id`, `seats_id`, `ticket_printed`, `id`) VALUES
(3, 2, 0, 5),
(4, 2, 0, 6),
(5, 1, 0, 7),
(8, 1, 0, 8),
(9, 1, 0, 9),
(9, 2, 0, 10),
(9, 3, 0, 11),
(9, 4, 0, 12),
(10, 1, 0, 13),
(10, 2, 0, 14),
(10, 3, 0, 15),
(10, 4, 0, 16),
(10, 13, 0, 17),
(10, 15, 0, 18),
(11, 1, 0, 19),
(11, 2, 0, 20),
(12, 1, 0, 21),
(12, 2, 0, 22),
(13, 10, 0, 23),
(14, 1, 0, 24),
(15, 1, 0, 25),
(16, 1, 0, 26),
(16, 2, 0, 27),
(16, 3, 0, 28),
(17, 4, 0, 29),
(17, 16, 0, 30),
(18, 1, 0, 31),
(18, 13, 0, 32),
(19, 1, 0, 33),
(19, 2, 0, 34),
(19, 13, 0, 35),
(19, 14, 0, 36),
(20, 1, 0, 37),
(20, 13, 0, 38),
(21, 1, 0, 39),
(21, 2, 0, 40),
(21, 3, 0, 41),
(21, 4, 0, 42);

-- --------------------------------------------------------

--
-- Table structure for table `booking_status`
--

CREATE TABLE `booking_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(15) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `booking_status`
--

INSERT INTO `booking_status` (`id`, `status_name`) VALUES
(2, 'Available'),
(1, 'Booked'),
(5, 'Canceled'),
(3, 'Confirmed'),
(6, 'Hire Available'),
(7, 'Hire Booked'),
(8, 'Hire Canceled'),
(9, 'Hire Pending'),
(4, 'Pending');

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE `bus` (
  `id` int(11) NOT NULL,
  `bus_name` varchar(255) DEFAULT NULL,
  `bus_number` varchar(15) DEFAULT NULL,
  `seat_count` int(11) DEFAULT NULL,
  `seating_layout_id` int(11) NOT NULL,
  `is_for_hire` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`id`, `bus_name`, `bus_number`, `seat_count`, `seating_layout_id`, `is_for_hire`) VALUES
(1, 'NC-0861', 'NC-0861', 52, 1, 0),
(2, 'NB-0645', 'NB-0645', 52, 1, 0),
(3, 'Hire Bus 1', 'SD-2356', 52, 1, 1),
(4, 'NB-0647', 'NB-0647', 52, 1, 0),
(5, 'ND-2900', 'ND-2900', 52, 1, 0),
(6, 'ND-5211', 'ND-5211', 52, 1, 0),
(7, 'NC-2090', 'NC-2090', 52, 1, 0),
(8, 'NC-2112', 'NC-2112', 52, 1, 0),
(9, 'NB-7974', 'NB-7974', 52, 1, 0),
(10, 'NC-3677', 'NC-3677', 52, 1, 0),
(11, 'NC-9698', 'NC-9698', 52, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `bus_has_feature`
--

CREATE TABLE `bus_has_feature` (
  `bus_id` int(11) NOT NULL,
  `feature_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bus_has_feature`
--

INSERT INTO `bus_has_feature` (`bus_id`, `feature_id`) VALUES
(3, 1),
(3, 2),
(3, 3),
(3, 4);

-- --------------------------------------------------------

--
-- Table structure for table `bus_info`
--

CREATE TABLE `bus_info` (
  `bus_id` int(11) NOT NULL,
  `driver_name` varchar(255) NOT NULL,
  `conductor_name` varchar(255) NOT NULL,
  `driver_phone` varchar(255) NOT NULL,
  `conductor_phone` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bus_info`
--

INSERT INTO `bus_info` (`bus_id`, `driver_name`, `conductor_name`, `driver_phone`, `conductor_phone`) VALUES
(1, 'Chaminda Karunarathna', 'Asanka Amarasena', '0777462598', '077 4105952'),
(2, 'Lalith Perera', 'Kalum Rathnasiri', '076 6532335', '071 3003452'),
(3, 'Priyadasa wikramarachchi', 'Sahan Kariyawasam', '071 7295954', '071 9205876'),
(4, 'Dilum De Silva', 'Wikum Ginige', '077 3423562', '077 1127711'),
(5, 'Anil siriwardhana', 'Sarath Jayakodi', '077 8925234', '071 5211425'),
(6, 'Ajith amarathunga', 'Pradeep Gamage', '077 9556332', '071 1814662'),
(7, 'Priyantha dayas', 'Kumara Rathnaweera', '071 7852443', '077 6532529'),
(8, 'Premasiri kodithuwakku', 'Nuwan Sirisena', '071 0356269', '077 8025499'),
(9, 'Sumanasiri gunaweera', 'Kulasiri De silva', '071 2305576', '077 9023561'),
(10, 'Saman aluthwala', 'Nishantha Karunasena', '071 6201182', '071 8335516'),
(11, 'Chaminda karunarathna', 'Asanka Amarasena', '077 7462598', '077 4105952');

-- --------------------------------------------------------

--
-- Table structure for table `cancelation`
--

CREATE TABLE `cancelation` (
  `id` int(11) NOT NULL,
  `canceled_at` datetime DEFAULT NULL,
  `booking_id` int(11) NOT NULL,
  `cancelation_fees_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cancelation_fees`
--

CREATE TABLE `cancelation_fees` (
  `id` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `fee` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE `city` (
  `id` int(11) NOT NULL,
  `city_name` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `city_name`) VALUES
(19, 'Colombo'),
(13, 'Delkanda'),
(3, 'Galle'),
(9, 'Gammana Rd'),
(10, 'Guru Vidyalaya'),
(15, 'Kirulapona'),
(2, 'Kottawa'),
(4, 'Mahalwaraya'),
(11, 'Maharagama'),
(1, 'Makumbura'),
(5, 'Morakatiya'),
(14, 'Nugegoda'),
(8, 'Pannipitiya'),
(18, 'Pettah'),
(16, 'Thimbirigasyaya'),
(17, 'Town Hall'),
(12, 'Wijayarama');

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id` int(11) NOT NULL,
  `customer_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `address_line1` varchar(255) DEFAULT NULL,
  `address_line2` varchar(255) DEFAULT NULL,
  `city_id` int(11) NOT NULL,
  `mobile_no` varchar(15) NOT NULL,
  `recovery_code` varchar(15) DEFAULT NULL,
  `users_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id`, `customer_name`, `email`, `address_line1`, `address_line2`, `city_id`, `mobile_no`, `recovery_code`, `users_id`) VALUES
(1, 'Dilshani Nishadari', 'dilshaninishadari@gmail.com', 'A1', 'A2', 1, '0715602338', NULL, 3),
(2, 'Thushara Nishanthi', 'mmtnwijeratne@gmail.com', '', '', 1, '', NULL, 5);

-- --------------------------------------------------------

--
-- Table structure for table `day_of_week`
--

CREATE TABLE `day_of_week` (
  `id` int(11) NOT NULL,
  `day_name` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `day_of_week`
--

INSERT INTO `day_of_week` (`id`, `day_name`) VALUES
(5, 'Friday'),
(1, 'Monday'),
(6, 'Saturday'),
(7, 'Sunday'),
(4, 'Thursday'),
(2, 'Tuesday'),
(3, 'Wednesday');

-- --------------------------------------------------------

--
-- Table structure for table `end_time`
--

CREATE TABLE `end_time` (
  `id` int(11) NOT NULL,
  `time_name` varchar(7) DEFAULT NULL,
  `time_value` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `end_time`
--

INSERT INTO `end_time` (`id`, `time_name`, `time_value`) VALUES
(1, '08:30', '08:30:00'),
(2, '11:00', '11:00:00'),
(3, '12:30', '12:30:00'),
(4, '06:30', '06:30:00'),
(5, '15:00', '15:00:00'),
(6, '05:30', '05:30:00'),
(7, '10:30', '10:30:00'),
(8, '11:30', '11:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `extra_services`
--

CREATE TABLE `extra_services` (
  `id` int(255) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `bus` varchar(255) DEFAULT NULL,
  `start_location` varchar(255) DEFAULT NULL,
  `end_location` varchar(255) DEFAULT NULL,
  `description` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `extra_services`
--

INSERT INTO `extra_services` (`id`, `user_id`, `name`, `phone`, `email`, `type`, `bus`, `start_location`, `end_location`, `description`, `created_at`, `updated_at`) VALUES
(1, 5, 'Thushara Nishanthi', '0718325626', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'I need some tourism things', '2018-02-17 08:25:12', '2018-02-17 08:25:12'),
(2, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:29:19', '2018-02-17 08:29:19'),
(3, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:30:10', '2018-02-17 08:30:10'),
(4, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:30:58', '2018-02-17 08:30:58'),
(5, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:31:46', '2018-02-17 08:31:46'),
(6, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:32:13', '2018-02-17 08:32:13'),
(7, 5, 'Thushara Nishanthi', '0756281355', 'mmtnwijeratne@gmail.com', 'Tourism', NULL, NULL, NULL, 'Something is happening', '2018-02-17 08:33:08', '2018-02-17 08:33:08');

-- --------------------------------------------------------

--
-- Table structure for table `feature`
--

CREATE TABLE `feature` (
  `id` int(11) NOT NULL,
  `feature_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `feature`
--

INSERT INTO `feature` (`id`, `feature_name`) VALUES
(1, 'A/C'),
(4, 'Fridge'),
(3, 'Power Outlets'),
(2, 'Wi-Fi');

-- --------------------------------------------------------

--
-- Table structure for table `from_bus_stand`
--

CREATE TABLE `from_bus_stand` (
  `id` int(11) NOT NULL,
  `stand_name` varchar(255) DEFAULT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `from_bus_stand`
--

INSERT INTO `from_bus_stand` (`id`, `stand_name`, `city_id`) VALUES
(4, 'Colombo', 19),
(5, 'Delkanda', 13),
(6, 'Galle', 3),
(7, 'Gammana Rd', 9),
(8, 'Guru Vidyalaya', 10),
(9, 'Kirulapona', 15),
(10, 'Kottawa', 2),
(11, 'Mahalwaraya', 4),
(12, 'Maharagama', 11),
(13, 'Makumbura', 1),
(14, 'Morakatiya', 5),
(15, 'Nugegoda', 14),
(16, 'Pannipitiya', 8),
(17, 'Pettah', 18),
(18, 'Thimbirigasyaya', 16),
(19, 'Town Hall', 17),
(20, 'Wijayarama', 12);

-- --------------------------------------------------------

--
-- Table structure for table `hire`
--

CREATE TABLE `hire` (
  `id` int(10) UNSIGNED NOT NULL,
  `fromdate` date DEFAULT NULL,
  `todate` date DEFAULT NULL,
  `fromplace` varchar(255) DEFAULT NULL,
  `toplace` varchar(255) DEFAULT NULL,
  `km` decimal(7,2) DEFAULT NULL,
  `bus_id` int(11) NOT NULL,
  `customer_id` int(11) NOT NULL,
  `hireRate_id` int(10) UNSIGNED NOT NULL,
  `booking_status_id` int(11) NOT NULL,
  `actual_end_date` date DEFAULT NULL,
  `actual_milage` decimal(10,2) DEFAULT NULL,
  `total` decimal(12,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hire`
--

INSERT INTO `hire` (`id`, `fromdate`, `todate`, `fromplace`, `toplace`, `km`, `bus_id`, `customer_id`, `hireRate_id`, `booking_status_id`, `actual_end_date`, `actual_milage`, `total`) VALUES
(1, '2018-02-10', '2018-02-12', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-12', '300.00', '0.00'),
(2, '2018-02-10', '2018-02-12', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-12', '300.00', '0.00'),
(3, '2018-02-10', '2018-02-12', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-12', '300.00', '0.00'),
(4, '2018-02-10', '2018-02-12', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-12', '300.00', '0.00'),
(5, '2018-02-10', '2018-02-12', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-12', '300.00', '0.00'),
(6, '2018-02-10', '2018-02-15', 'Chilaw', 'Galle', '300.00', 3, 1, 1, 9, '2018-02-15', '300.00', '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `hirerate`
--

CREATE TABLE `hirerate` (
  `id` int(10) UNSIGNED NOT NULL,
  `Rate` decimal(10,2) DEFAULT NULL,
  `description` text,
  `defaultmilage` decimal(7,2) DEFAULT NULL,
  `defaultrate` decimal(10,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hirerate`
--

INSERT INTO `hirerate` (`id`, `Rate`, `description`, `defaultmilage`, `defaultrate`) VALUES
(1, '1000.00', NULL, '150.00', '20000.00');

-- --------------------------------------------------------

--
-- Table structure for table `menuview`
--

CREATE TABLE `menuview` (
  `id` int(11) NOT NULL,
  `menuname` varchar(100) COLLATE utf8_bin NOT NULL,
  `url` varchar(255) COLLATE utf8_bin NOT NULL,
  `level` int(11) NOT NULL,
  `parentmenu` int(11) NOT NULL,
  `icon` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `menuview`
--

INSERT INTO `menuview` (`id`, `menuname`, `url`, `level`, `parentmenu`, `icon`, `admin`) VALUES
(1, 'Mainmenu', 'home', 0, 0, '', 0),
(2, 'Revenue', 'home', 1, 1, 'fa-arrow-up', 0),
(3, 'Direct Cost', 'home', 1, 1, 'fa-arrow-down', 0),
(4, 'Profit', 'home', 1, 1, 'fa-bar-chart', 0),
(5, 'Customers', 'home', 1, 1, 'fa-user', 0),
(6, 'Sales', 'home', 1, 1, 'fa-exchange', 0),
(7, 'Debtors', 'home', 1, 1, 'fa-book', 0),
(8, 'Creditors', 'home', 1, 1, 'fa-briefcase', 0),
(9, 'Group', 'home', 1, 1, NULL, 0),
(10, 'Gensoft Network', 'home', 1, 1, 'fa-building', 0),
(11, 'Growth', 'home', 2, 2, 'fa-line-chart', 0),
(12, 'Based On Product', 'home', 2, 2, 'fa-archive', 0),
(13, 'Top 10 Customers', 'home', 2, 2, 'fa-users', 0),
(14, 'Per Employee', 'home', 2, 2, 'fa-user', 0),
(15, 'Growth', 'home', 2, 3, 'fa-line-chart', 0),
(16, 'Based On Product', 'home', 2, 3, 'fa-archive', 0),
(17, 'Gross Profit Growth', 'home', 2, 4, 'fa-line-chart', 0),
(18, 'Net Profit Growth', 'home', 2, 4, 'fa-area-chart', 0),
(19, 'Gross Profit Based On Product', 'home', 2, 4, 'fa-archive', 0),
(20, 'Top 10 Customers', 'home', 2, 5, 'fa-users', 0),
(21, 'Based On GP', 'home', 3, 20, 'fa-bar-chart', 0),
(22, 'Top Performing Person', 'home', 2, 6, 'fa-user', 0),
(23, 'Based On Revenue', 'home', 3, 22, 'fa-money', 0),
(24, 'Based On GP', 'home', 3, 22, 'fa-bar-chart', 0),
(25, 'Based On Revenue', 'home', 3, 20, 'fa-money', 0),
(26, 'Top 10 Debtors', 'home', 2, 7, 'fa-users', 0),
(27, 'Based On Value', 'home', 3, 26, 'fa-cubes', 0),
(28, 'Average Settlement Days', 'home', 2, 7, 'fa-calendar', 0),
(29, 'Top 10 Creditors', 'home', 2, 8, 'fa-users', 0),
(30, 'Based On Value', 'home', 3, 29, 'fa-cubes', 0),
(31, 'Average Settlement Days', 'home', 2, 8, 'fa-calendar', 0),
(32, 'Ranking Based On', 'home', 2, 10, 'fa-star', 0),
(33, 'Total Revenue', 'home', 3, 32, 'fa-money', 0),
(34, 'GP', 'home', 3, 32, 'fa-bar-chart', 0),
(35, 'GP(%)', 'home', 3, 32, 'fa-percent', 0),
(36, 'Revenue Per Employee', 'home', 3, 32, 'fa-user', 0),
(37, 'Average', 'home', 2, 10, 'fa-arrows-v', 0),
(38, 'Revenue Per Sales Person', 'home', 3, 37, 'fa-user', 0),
(39, 'GP Per Sales Person', 'home', 3, 37, 'fa-bar-chart', 0),
(40, 'Debtor Settlement Days', 'home', 3, 37, 'fa-calendar', 0),
(41, 'Feedback', 'home', 1, 1, 'fa-comments', 0),
(42, 'Users', '/#', 1, 1, 'fa-user', 1),
(43, 'Create Users', 'register', 2, 42, 'fa-user-plus', 1),
(44, 'Groups', '/#', 1, 1, 'fa-users', 1),
(45, 'New Group', 'newGroup', 2, 44, 'fa-building', 1),
(46, 'View Groups', 'viewGroups', 2, 44, 'fa-binoculars', 1),
(47, 'View Users', 'viewUsers', 2, 42, 'fa-binoculars', 1),
(48, 'Add Companies', 'addCompanies', 2, 44, 'fa-building', 1);

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `updated_at`) VALUES
(2, 'Chamuth Chamandana', 'c.chamandana@gmail.com', '756281355', 'Testing this out', '2018-02-15 10:32:39', '2018-02-15 10:32:39');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `token` varchar(255) COLLATE utf8_bin NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Table structure for table `payment_status`
--

CREATE TABLE `payment_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_status`
--

INSERT INTO `payment_status` (`id`, `status_name`) VALUES
(2, 'Not Paid'),
(1, 'Paid');

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE `reports` (
  `id` int(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `problem_type` varchar(255) NOT NULL,
  `description` varchar(1000) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reports`
--

INSERT INTO `reports` (`id`, `name`, `phone`, `problem_type`, `description`, `created_at`, `updated_at`) VALUES
(2, 'Chamuth Chamandana', '0718325626', 'bus_problem', 'asdasdasdas', '2018-02-17 23:42:01', '2018-02-17 23:42:01');

-- --------------------------------------------------------

--
-- Table structure for table `seating_layout`
--

CREATE TABLE `seating_layout` (
  `id` int(11) NOT NULL,
  `layout_name` varchar(70) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seating_layout`
--

INSERT INTO `seating_layout` (`id`, `layout_name`) VALUES
(1, 'Default Layout');

-- --------------------------------------------------------

--
-- Table structure for table `seats`
--

CREATE TABLE `seats` (
  `id` int(11) NOT NULL,
  `seat_number` varchar(7) DEFAULT NULL,
  `bus_id` int(11) NOT NULL,
  `title` int(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `seats`
--

INSERT INTO `seats` (`id`, `seat_number`, `bus_id`, `title`) VALUES
(1, 'EX1_1', 1, 1),
(2, 'EX1_2', 1, 2),
(3, 'EX1_3', 1, 3),
(4, 'EX1_4', 1, 4),
(5, 'EX1_5', 1, 5),
(6, 'EX1_6', 1, 6),
(7, 'EX1_7', 1, 7),
(8, 'EX1_8', 1, 8),
(9, 'EX1_9', 1, 9),
(10, 'EX1_10', 1, 10),
(11, 'EX1_11', 1, 11),
(12, 'EX1_12', 1, 12),
(13, 'EX1_13', 1, 13),
(14, 'EX1_14', 1, 14),
(15, 'EX1_15', 1, 15),
(16, 'EX1_16', 1, 16),
(17, 'EX1_17', 1, 17),
(18, 'EX1_18', 1, 18),
(19, 'EX1_19', 1, 19),
(20, 'EX1_20', 1, 20),
(21, 'EX1_21', 1, 21),
(22, 'EX1_22', 1, 22),
(23, 'EX1_23', 1, 23),
(24, 'EX1_24', 1, 24),
(25, 'EX1_25', 1, 25),
(26, 'EX1_26', 1, 26),
(27, 'EX1_27', 1, 27),
(28, 'EX1_28', 1, 28),
(29, 'EX1_29', 1, 29),
(30, 'EX1_30', 1, 30),
(31, 'EX1_31', 1, 31),
(32, 'EX1_32', 1, 32),
(33, 'EX1_33', 1, 33),
(34, 'EX1_34', 1, 34),
(35, 'EX1_35', 1, 35),
(36, 'EX1_36', 1, 36),
(37, 'EX1_37', 1, 37),
(38, 'EX1_38', 1, 38),
(39, 'EX1_39', 1, 39),
(40, 'EX1_40', 1, 40),
(41, 'EX1_41', 1, 41),
(42, 'EX1_42', 1, 42),
(43, 'EX1_43', 1, 43),
(44, 'EX1_44', 1, 44),
(45, 'EX1_45', 1, 45),
(46, 'EX1_46', 1, 46),
(47, 'EX1_47', 1, 47),
(48, 'EX1_48', 1, 48),
(49, 'EX1_49', 1, 49),
(50, 'EX1_50', 1, 50),
(53, 'EX2_1', 2, 1),
(54, 'EX2_2', 2, 2),
(55, 'EX2_3', 2, 3),
(56, 'EX2_4', 2, 4),
(57, 'EX2_5', 2, 5),
(58, 'EX2_6', 2, 6),
(59, 'EX2_7', 2, 7),
(60, 'EX2_8', 2, 8),
(61, 'EX2_9', 2, 9),
(62, 'EX2_10', 2, 10),
(63, 'EX2_11', 2, 11),
(64, 'EX2_12', 2, 12),
(65, 'EX2_13', 2, 13),
(66, 'EX2_14', 2, 14),
(67, 'EX2_15', 2, 15),
(68, 'EX2_16', 2, 16),
(69, 'EX2_17', 2, 17),
(70, 'EX2_18', 2, 18),
(71, 'EX2_19', 2, 19),
(72, 'EX2_20', 2, 20),
(73, 'EX2_21', 2, 21),
(74, 'EX2_22', 2, 22),
(75, 'EX2_23', 2, 23),
(76, 'EX2_24', 2, 24),
(77, 'EX2_25', 2, 25),
(78, 'EX2_26', 2, 26),
(79, 'EX2_27', 2, 27),
(80, 'EX2_28', 2, 28),
(81, 'EX2_29', 2, 29),
(82, 'EX2_30', 2, 30),
(83, 'EX2_31', 2, 31),
(84, 'EX2_32', 2, 32),
(85, 'EX2_33', 2, 33),
(86, 'EX2_34', 2, 34),
(87, 'EX2_35', 2, 35),
(88, 'EX2_36', 2, 36),
(89, 'EX2_37', 2, 37),
(90, 'EX2_38', 2, 38),
(91, 'EX2_39', 2, 39),
(92, 'EX2_40', 2, 40),
(93, 'EX2_41', 2, 41),
(94, 'EX2_42', 2, 42),
(95, 'EX2_43', 2, 43),
(96, 'EX2_44', 2, 44),
(97, 'EX2_45', 2, 45),
(98, 'EX2_46', 2, 46),
(99, 'EX2_47', 2, 47),
(100, 'EX2_48', 2, 48),
(101, 'EX2_49', 2, 49),
(102, 'EX2_50', 2, 50);

-- --------------------------------------------------------

--
-- Table structure for table `sitecontent`
--

CREATE TABLE `sitecontent` (
  `id` int(10) UNSIGNED NOT NULL,
  `contentname` varchar(100) DEFAULT NULL,
  `content` text,
  `pagename` varchar(30) DEFAULT NULL,
  `isimage` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `iscolor` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sitecontent`
--

INSERT INTO `sitecontent` (`id`, `contentname`, `content`, `pagename`, `isimage`, `iscolor`) VALUES
(1, 'header title 1', 'Welcome', 'index', 0, 0),
(2, 'header subtitle 1', 'galle to colombo highway bus online booking platform', 'index', 0, 0),
(3, 'book now button', 'Book Now', 'index', 0, 0),
(4, 'header title 2', 'Welcome', 'index', 0, 0),
(5, 'header subtitle 2', 'galle to colombo highway bus online booking platform', 'index', 0, 0),
(6, 'date selection header', 'Find a tour', 'index', 0, 0),
(7, 'date selection header subtitle', 'where would you like to go?', 'index', 0, 0),
(8, 'offers section title', 'WHY BOOK WITH US?', 'index', 0, 0),
(9, 'offers section subtitle', 'Fully Fledged Booking & Reservation Service', 'index', 0, 0),
(10, 'offer 1', 'From Anywhere - At Anytime', 'index', 0, 0),
(11, 'offer 1 description', 'Reserve your seats from anywhere at anytime by simply log into internet with your favorite device.', 'index', 0, 0),
(12, 'offer 2', 'Reserve Early - Save More', 'index', 0, 0),
(13, 'offer 2 description', 'Save your valuable time and money by making advanced booking via our website.', 'index', 0, 0),
(14, 'offer 3', 'Passenger Assistance - Support', 'index', 0, 0),
(15, 'offer 3 description', 'Experience next generation passenger service with our well experienced passenger services executives.', 'index', 0, 0),
(16, 'offer 4', '100% Secure & hassle-free', 'index', 0, 0),
(17, 'offer 4 description', 'Pay online securely via the internet payment gateways provided by leading financial institutions.', 'index', 0, 0),
(18, 'places to visit', 'Dilshani Travels', 'index', 0, 0),
(19, 'top places subtitle', 'We Protect Your Trust', 'index', 0, 0),
(20, 'visit image 1', '/assets/images/bus/1.PNG', 'index', 1, 0),
(21, 'visit image 2', '/assets/images/bus/2.PNG', 'index', 1, 0),
(22, 'visit image 3', '/assets/images/bus/418.jpg', 'index', 1, 0),
(23, 'visit image 4', '/assets/images/bus/763b40babf71b7706e8bb914c100c5f8.jpg', 'index', 1, 0),
(24, 'visit image 5', '/assets/images/bus/77.PNG', 'index', 1, 0),
(25, 'visit image 6', '/assets/images/bus/bus-hire.jpg', 'index', 1, 0),
(26, 'additional services title', 'Top place to visit in Galle', 'index', 0, 0),
(27, 'additional services subtitle', 'Galle is a city on the southwest coast of Sri Lanka. It’s known for Galle Fort, the fortified old city founded by Portuguese colonists in the 16th century.', 'index', 0, 0),
(28, 'services 1 image', '/assets/images/15184449408869.png', 'index', 1, 0),
(29, 'services 1 title', 'Galle Fort', 'index', 0, 0),
(30, 'services 1 description', 'Galle Fort, in the Bay of Galle on the southwest coast of Sri Lanka, was built first in 1588 by the Portuguese.', 'index', 0, 0),
(31, 'services 2 title', 'Unawatuna Beach', 'index', 0, 0),
(32, 'services 2 description', 'Unawatuna is one of the biggest tourist destinations in Sri Lanka and is the most “ famous” beach in the country.', 'index', 0, 0),
(33, 'services 3 image', '/assets/images/15184449668575.png', 'index', 1, 0),
(34, 'services 3 title', 'National Museum of Galle', 'index', 0, 0),
(35, 'services 3 description', 'The National Museum of Galle is one of the national museums of Sri Lanka.', 'index', 0, 0),
(36, 'services 4 image', '/assets/images/15184449940391.png', 'index', 1, 0),
(37, 'services 4 title', 'Japanese Peace Pagoda', 'index', 0, 0),
(38, 'services 4 description', 'Unique Buddhist temple with a circular deck for sweeping views of the ocean & surrounding landscape.', 'index', 0, 0),
(39, 'services 5 image', '/assets/images/15184450154517.png', 'index', 1, 0),
(40, 'services 5 title', 'Galle Lighthouse', 'index', 0, 0),
(41, 'services 5 description', 'Galle Lighthouse is an onshore Lighthouse in Galle.', 'index', 0, 0),
(42, 'services 6 image', '/assets/images/15184450641209.png', 'index', 1, 0),
(43, 'services 6 title', 'Yatagala Raja Maha Viharaya', 'index', 0, 0),
(44, 'services 6 description', 'Old Buddhist temple complex built into rock formations featuring many statues & a serene atmosphere.', 'index', 0, 0),
(45, 'services 3 image', '/assets/images/15184450377405.png', 'index', 1, 0),
(46, 'our story', '&nbsp;', 'about', 0, 0),
(47, 'Who we are?', 'Who we are?', 'about', 0, 0),
(48, 'who we are subtitle', 'Dilshani Travels is a Sri Lankan owned and operated company that has been helping visitors to discover and enjoy Sri Lanka for over 25 years.', 'about', 0, 0),
(49, 'who we are image 1', '/assets/images/11.png', 'about', 1, 0),
(50, 'who we are tile 1', 'From Anywhere - At Anytime', 'about', 0, 0),
(51, 'who we are tile 1 detais', 'Reserve your seats from anywhere at anytime by simply log into internet with your favorite device.', 'about', 0, 0),
(52, 'who we are image 2', '/assets/images/22.png', 'about', 1, 0),
(53, 'who we are tile 2', 'Reserve Early - Save More', 'about', 0, 0),
(54, 'who we are tile 2 detais', 'Save your valuable time and money by making advanced booking via our website.', 'about', 0, 0),
(55, 'who we are image 3', '/assets/images/33.png', 'about', 1, 0),
(56, 'who we are tile 3', 'Passenger Assistance - Support', 'about', 0, 0),
(57, 'who we are tile 3 detais', 'Experience next generation passenger service with our well experienced passenger services executives.', 'about', 0, 0),
(58, 'who we are image 4', '/assets/images/44.png', 'about', 1, 0),
(59, 'who we are tile 4', '100% Secure & hassle-free', 'about', 0, 0),
(60, 'who we are tile 4 detais', 'Pay online securely via the internet payment gateways provided by leading financial institutions.', 'about', 0, 0),
(61, 'story title 1', 'Our Story', 'about', 0, 0),
(62, 'story image 1', '/assets/images/client.png', 'about', 1, 0),
(63, 'story detail 1', 'Dilshani Travels Highway Transport Service” (DTHTS) which is a well-established highway bus transport Service (HBTS) company located at No,357/G, Hirimbura Road, Dangedara, Galle, Sri Lanka. Mr. Asoka Gunasekara is the founder of the company and it was started in 1994 as a normal bus service provider facilitating Galle to Colombo bus service in the country. Not only that, we have twenty-two years’ excellent experience and thirty more brilliant staff.', 'about', 0, 0),
(64, 'story detail 1 2', 'Currently, we have reached a huge transformation due to the highway bus service and accordingly, at present, we have ten highway buses in “Galle to Colombo” (GC) route and as a transport company they do not think twice to fulfill their duties in terms of best customer service provision. As well as, we give you satisfaction servicers like Tourism Services, Special Journey and Company Hires as your wishes. Therefore, you can enjoy your vacations, holidays etc.., without hassles. We always attempt to give best and trusted service to their clients. Our main goal is, through the trust, to get more customers and become the best transport service company in Sri Lanka.', 'about', 0, 0),
(65, 'Client feedback', 'Client feedback', 'about', 0, 0),
(66, '5 star area value', '70', 'about', 0, 0),
(67, '5 star amount', '50%', 'about', 0, 0),
(68, '4 star area value', '90', 'about', 0, 0),
(69, '4 star amount', '90%', 'about', 0, 0),
(70, '3 star area value', '70', 'about', 0, 0),
(71, '3 star amount', '70%', 'about', 0, 0),
(72, '2 star area value', '70', 'about', 0, 0),
(73, '2 star amount', '70%', 'about', 0, 0),
(74, '1 star area value', '82', 'about', 0, 0),
(75, '1 star amount', '82%', 'about', 0, 0),
(76, 'about curosal 1', '/assets/images/sp-1.png', 'about', 1, 0),
(77, 'get in touch with us', 'get in touch with us', 'contact', 0, 0),
(78, 'Address Line 1', 'Dilshani Travels Passenger Transport Service,', 'contact', 0, 0),
(79, 'Address Line 2', '357/G, Hrimbura Road, Dangedara, Galle Sri Lanka.', 'contact', 0, 0),
(80, 'phone local', '091 2233272', 'contact', 0, 0),
(81, 'phone mobile', '076729399', 'contact', 0, 0),
(82, 'email', 'dilshanitravels@gmail.com', 'contact', 0, 0),
(83, 'who h3', '#313131', 'about color', 0, 1),
(84, 'who border', '#ddd', 'about color', 0, 1),
(85, 'who-detail', '#969595', 'about color', 0, 1),
(86, 'about tbody', '#969595', 'about color', 0, 1),
(87, 'about progress', '#fff', 'about color', 0, 1),
(88, 'about progress bar', '#2565BA', 'about color', 0, 1),
(89, 'contact contacts', '#f9f9f9', 'contact color', 0, 1),
(90, 'contact hover', '#60C9EB', 'contact color', 0, 1),
(91, 'contact icon', '#60C9EB', 'contact color', 0, 1),
(92, 'contact detail', '#969595', 'contact color', 0, 1),
(93, 'main body', '#fff', 'main color', 0, 1),
(94, 'sction title', '#313131', 'main color', 0, 1),
(95, 'section subtitle', '#583b54', 'main color', 0, 1),
(96, 'paragraphs', '#969595', 'main color', 0, 1),
(97, 'border radius', '#2565BA', 'main color', 0, 1),
(98, 'form control focus', '#66afe9', 'main color', 0, 1),
(99, 'custom btn', '#583b54', 'main color', 0, 1),
(100, 'custom btn hover', '#583b54', 'main color', 0, 1),
(101, 'custom button', '#2565BA', 'main color', 0, 1),
(102, 'custom button hover', '#53B1DE', 'main color', 0, 1),
(103, 'offer', 'black', 'main color', 0, 1),
(104, 'offer background', '#f9f9f9', 'main color', 0, 1),
(105, 'icon', '#2565BA', 'main color', 0, 1),
(106, 'visit', '#2565BA', 'main color', 0, 1),
(107, 'add service details background', '#f9f9f9', 'main color', 0, 1),
(108, 'add service details', '#969595', 'main color', 0, 1),
(109, 'subscribe now', '#313131', 'main color', 0, 1),
(110, 'top', '#2F6B5A', 'main color', 0, 1),
(111, 'ul features list', '#969595', 'services color', 0, 1),
(112, 'item name', '#366BB0', 'services color', 0, 1),
(113, 'services header', '&nbsp;', 'services', 0, 0),
(114, 'Features', 'Features', 'services', 0, 0),
(115, 'section subtitle', NULL, 'services', 0, 0),
(116, 'feature details', 'The Home of great value Sri Lanka travel packages, Sri Lanka tours, vacations and holidays in Sri Lanka. Through this website, we invite you to take a glimpse of Sri Lanka, which is not only the lustrous Pearl of the Indian Ocean, but one of the most exciting locations for your next tour, vacation. You will be enticed by the rich diversity of sights and sensations that this little island has to offer. Step into the island and discover. So we like to provide extra services like,', 'services', 0, 0),
(117, 'feature list 1', 'Tourism Service', 'services', 0, 0),
(118, 'feature list 2', 'Special Journey', 'services', 0, 0),
(119, 'feature list 3', 'Company Hires', 'services', 0, 0),
(120, 'feature list 4', 'Daily Booking', 'services', 0, 0),
(121, 'features background', '/assets/images/15184465314100.png', 'services', 1, 0),
(122, 'additional services', 'Additional services', 'services', 0, 0),
(123, 'additional services subtitle', 'Lorem Ipsum is simply dummy text of the industry.', 'services', 0, 0),
(124, 'additional service 1 image', '/assets/images/add-srvc-1.png', 'services', 1, 0),
(125, 'additional service 1 title', 'Photography', 'services', 0, 0),
(126, 'additional service 1 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0),
(127, 'additional service 2 image', '/assets/images/add-srvc-2.png', 'services', 1, 0),
(128, 'additional service 2 title', 'Cycling', 'services', 0, 0),
(129, 'additional service 2 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0),
(130, 'additional service 3 image', '/assets/images/add-srvc-3.png', 'services', 1, 0),
(131, 'additional service 3 title', 'Waking', 'services', 0, 0),
(132, 'additional service 3 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0),
(133, 'additional service 4 image', '/assets/images/add-srvc-4.png', 'services', 1, 0),
(134, 'additional service 4 title', 'Waking', 'services', 0, 0),
(135, 'additional service 4 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0),
(136, 'additional service 5 image', '/assets/images/add-srvc-5.png', 'services', 1, 0),
(137, 'additional service 5 title', 'Waking', 'services', 0, 0),
(138, 'additional service 5 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0),
(139, 'additional service 6 image', '/assets/images/add-srvc-6.png', 'services', 1, 0),
(140, 'additional service 6 title', 'Waking', 'services', 0, 0),
(141, 'additional service 6 text', 'Etiam pharetra ut ante eget ehy efficitur. Nullam quis felis at nunc vehicula sollicitudin euliquam quis felis atblan dit.', 'services', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `start_time`
--

CREATE TABLE `start_time` (
  `id` int(11) NOT NULL,
  `time_name` varchar(7) DEFAULT NULL,
  `time_value` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `start_time`
--

INSERT INTO `start_time` (`id`, `time_name`, `time_value`) VALUES
(1, '05:00', '05:00:00'),
(2, '07:00', '07:00:00'),
(3, '09:30', '09:30:00'),
(4, '11:00', '11:00:00'),
(5, '13:30', '13:30:00'),
(6, '04:00', '04:00:00'),
(7, '09:00', '09:00:00'),
(8, '10:00', '10:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `time_table`
--

CREATE TABLE `time_table` (
  `id` int(11) NOT NULL,
  `from_bus_stand_id` int(11) NOT NULL,
  `to_bus_stand_id` int(11) NOT NULL,
  `day_of_week_id` int(11) NOT NULL,
  `start_time_id` int(11) NOT NULL,
  `end_time_id` int(11) NOT NULL,
  `bus_id` int(11) NOT NULL,
  `ticket_price_adult` decimal(6,2) DEFAULT NULL,
  `ticket_price_child` decimal(6,2) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `time_table`
--

INSERT INTO `time_table` (`id`, `from_bus_stand_id`, `to_bus_stand_id`, `day_of_week_id`, `start_time_id`, `end_time_id`, `bus_id`, `ticket_price_adult`, `ticket_price_child`) VALUES
(3, 6, 4, 1, 1, 4, 1, '650.00', NULL),
(4, 6, 4, 1, 2, 1, 2, '650.00', NULL),
(5, 6, 4, 1, 3, 2, 4, '650.00', NULL),
(6, 6, 4, 1, 4, 3, 5, '650.00', NULL),
(7, 6, 4, 1, 5, 5, 6, '650.00', NULL),
(8, 6, 4, 2, 1, 4, 1, '650.00', NULL),
(9, 6, 4, 2, 2, 1, 2, '650.00', NULL),
(10, 6, 4, 2, 3, 2, 4, '650.00', NULL),
(11, 6, 4, 2, 4, 3, 5, '650.00', NULL),
(12, 6, 4, 2, 5, 5, 6, '650.00', NULL),
(13, 6, 4, 3, 1, 4, 1, '650.00', NULL),
(14, 6, 4, 3, 2, 1, 2, '650.00', NULL),
(15, 6, 4, 3, 3, 2, 4, '650.00', NULL),
(16, 6, 4, 3, 4, 3, 5, '650.00', NULL),
(17, 6, 4, 3, 5, 5, 6, '650.00', NULL),
(18, 6, 4, 4, 1, 4, 1, '650.00', NULL),
(19, 6, 4, 4, 2, 1, 2, '650.00', NULL),
(20, 6, 4, 4, 3, 2, 4, '650.00', NULL),
(21, 6, 4, 4, 4, 3, 5, '650.00', NULL),
(22, 6, 4, 4, 5, 5, 6, '650.00', NULL),
(23, 6, 4, 5, 1, 4, 1, '650.00', NULL),
(24, 6, 4, 5, 2, 1, 2, '650.00', NULL),
(25, 6, 4, 5, 3, 2, 4, '650.00', NULL),
(26, 6, 4, 5, 4, 3, 5, '650.00', NULL),
(27, 6, 4, 5, 5, 5, 6, '650.00', NULL),
(28, 6, 4, 6, 1, 4, 1, '650.00', NULL),
(29, 6, 4, 6, 2, 1, 2, '650.00', NULL),
(30, 6, 4, 6, 3, 2, 4, '650.00', NULL),
(31, 6, 4, 6, 4, 3, 5, '650.00', NULL),
(32, 6, 4, 6, 5, 5, 6, '650.00', NULL),
(33, 6, 4, 7, 1, 4, 1, '650.00', NULL),
(34, 6, 4, 7, 2, 1, 2, '650.00', NULL),
(35, 6, 4, 7, 3, 2, 4, '650.00', NULL),
(36, 6, 4, 7, 4, 3, 5, '650.00', NULL),
(37, 6, 4, 7, 5, 5, 6, '650.00', NULL),
(38, 4, 6, 1, 6, 6, 7, '650.00', NULL),
(39, 4, 6, 1, 1, 4, 8, '650.00', NULL),
(40, 4, 6, 1, 2, 1, 9, '650.00', NULL),
(41, 4, 6, 1, 7, 7, 10, '650.00', NULL),
(42, 4, 6, 1, 8, 8, 11, '650.00', NULL),
(43, 4, 6, 2, 6, 6, 7, '650.00', NULL),
(44, 4, 6, 2, 1, 4, 8, '650.00', NULL),
(45, 4, 6, 2, 2, 1, 9, '650.00', NULL),
(46, 4, 6, 2, 7, 7, 10, '650.00', NULL),
(47, 4, 6, 2, 8, 8, 11, '650.00', NULL),
(48, 4, 6, 3, 6, 6, 7, '650.00', NULL),
(49, 4, 6, 3, 1, 4, 8, '650.00', NULL),
(50, 4, 6, 3, 2, 1, 9, '650.00', NULL),
(51, 4, 6, 3, 7, 7, 10, '650.00', NULL),
(52, 4, 6, 3, 8, 8, 11, '650.00', NULL),
(53, 4, 6, 4, 6, 6, 7, '650.00', NULL),
(54, 4, 6, 4, 1, 4, 8, '650.00', NULL),
(55, 4, 6, 4, 2, 1, 9, '650.00', NULL),
(56, 4, 6, 4, 7, 7, 10, '650.00', NULL),
(57, 4, 6, 4, 8, 8, 11, '650.00', NULL),
(58, 4, 6, 5, 6, 6, 7, '650.00', NULL),
(59, 4, 6, 5, 1, 4, 8, '650.00', NULL),
(60, 4, 6, 5, 2, 1, 9, '650.00', NULL),
(61, 4, 6, 5, 7, 7, 10, '650.00', NULL),
(62, 4, 6, 5, 8, 8, 11, '650.00', NULL),
(63, 4, 6, 6, 6, 6, 7, '650.00', NULL),
(64, 4, 6, 6, 1, 4, 8, '650.00', NULL),
(65, 4, 6, 6, 2, 1, 9, '650.00', NULL),
(66, 4, 6, 6, 7, 7, 10, '650.00', NULL),
(67, 4, 6, 6, 8, 8, 11, '650.00', NULL),
(68, 4, 6, 7, 6, 6, 7, '650.00', NULL),
(69, 4, 6, 7, 1, 4, 8, '650.00', NULL),
(70, 4, 6, 7, 2, 1, 9, '650.00', NULL),
(71, 4, 6, 7, 7, 7, 10, '650.00', NULL),
(72, 4, 6, 7, 8, 8, 11, '650.00', NULL),
(101, 6, 4, 1, 1, 4, 1, '650.00', NULL),
(102, 6, 4, 1, 2, 1, 2, '650.00', NULL),
(103, 6, 4, 1, 3, 2, 4, '650.00', NULL),
(104, 6, 4, 1, 4, 3, 5, '650.00', NULL),
(105, 6, 4, 1, 5, 5, 6, '650.00', NULL),
(106, 6, 4, 2, 1, 4, 1, '650.00', NULL),
(107, 6, 4, 2, 2, 1, 2, '650.00', NULL),
(108, 6, 4, 2, 3, 2, 4, '650.00', NULL),
(109, 6, 4, 2, 4, 3, 5, '650.00', NULL),
(110, 6, 4, 2, 5, 5, 6, '650.00', NULL),
(111, 6, 4, 3, 1, 4, 1, '650.00', NULL),
(112, 6, 4, 3, 2, 1, 2, '650.00', NULL),
(113, 6, 4, 3, 3, 2, 4, '650.00', NULL),
(114, 6, 4, 3, 4, 3, 5, '650.00', NULL),
(115, 6, 4, 3, 5, 5, 6, '650.00', NULL),
(116, 6, 4, 4, 1, 4, 1, '650.00', NULL),
(117, 6, 4, 4, 2, 1, 2, '650.00', NULL),
(118, 6, 4, 4, 3, 2, 4, '650.00', NULL),
(119, 6, 4, 4, 4, 3, 5, '650.00', NULL),
(120, 6, 4, 4, 5, 5, 6, '650.00', NULL),
(121, 6, 4, 5, 1, 4, 1, '650.00', NULL),
(122, 6, 4, 5, 2, 1, 2, '650.00', NULL),
(123, 6, 4, 5, 3, 2, 4, '650.00', NULL),
(124, 6, 4, 5, 4, 3, 5, '650.00', NULL),
(125, 6, 4, 5, 5, 5, 6, '650.00', NULL),
(126, 6, 4, 6, 1, 4, 1, '650.00', NULL),
(127, 6, 4, 6, 2, 1, 2, '650.00', NULL),
(128, 6, 4, 6, 3, 2, 4, '650.00', NULL),
(129, 6, 4, 6, 4, 3, 5, '650.00', NULL),
(130, 6, 4, 6, 5, 5, 6, '650.00', NULL),
(131, 6, 4, 7, 1, 4, 1, '650.00', NULL),
(132, 6, 4, 7, 2, 1, 2, '650.00', NULL),
(133, 6, 4, 7, 3, 2, 4, '650.00', NULL),
(134, 6, 4, 7, 4, 3, 5, '650.00', NULL),
(135, 6, 4, 7, 5, 5, 6, '650.00', NULL),
(136, 4, 6, 1, 6, 6, 7, '650.00', NULL),
(137, 4, 6, 1, 1, 4, 8, '650.00', NULL),
(138, 4, 6, 1, 2, 1, 9, '650.00', NULL),
(139, 4, 6, 1, 7, 7, 10, '650.00', NULL),
(140, 4, 6, 1, 8, 8, 11, '650.00', NULL),
(141, 4, 6, 2, 6, 6, 7, '650.00', NULL),
(142, 4, 6, 2, 1, 4, 8, '650.00', NULL),
(143, 4, 6, 2, 2, 1, 9, '650.00', NULL),
(144, 4, 6, 2, 7, 7, 10, '650.00', NULL),
(145, 4, 6, 2, 8, 8, 11, '650.00', NULL),
(146, 4, 6, 3, 6, 6, 7, '650.00', NULL),
(147, 4, 6, 3, 1, 4, 8, '650.00', NULL),
(148, 4, 6, 3, 2, 1, 9, '650.00', NULL),
(149, 4, 6, 3, 7, 7, 10, '650.00', NULL),
(150, 4, 6, 3, 8, 8, 11, '650.00', NULL),
(151, 4, 6, 4, 6, 6, 7, '650.00', NULL),
(152, 4, 6, 4, 1, 4, 8, '650.00', NULL),
(153, 4, 6, 4, 2, 1, 9, '650.00', NULL),
(154, 4, 6, 4, 7, 7, 10, '650.00', NULL),
(155, 4, 6, 4, 8, 8, 11, '650.00', NULL),
(156, 4, 6, 5, 6, 6, 7, '650.00', NULL),
(157, 4, 6, 5, 1, 4, 8, '650.00', NULL),
(158, 4, 6, 5, 2, 1, 9, '650.00', NULL),
(159, 4, 6, 5, 7, 7, 10, '650.00', NULL),
(160, 4, 6, 5, 8, 8, 11, '650.00', NULL),
(161, 4, 6, 6, 6, 6, 7, '650.00', NULL),
(162, 4, 6, 6, 1, 4, 8, '650.00', NULL),
(163, 4, 6, 6, 2, 1, 9, '650.00', NULL),
(164, 4, 6, 6, 7, 7, 10, '650.00', NULL),
(165, 4, 6, 6, 8, 8, 11, '650.00', NULL),
(166, 4, 6, 7, 6, 6, 7, '650.00', NULL),
(167, 4, 6, 7, 1, 4, 8, '650.00', NULL),
(168, 4, 6, 7, 2, 1, 9, '650.00', NULL),
(169, 4, 6, 7, 7, 7, 10, '650.00', NULL),
(170, 4, 6, 7, 8, 8, 11, '650.00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `to_bus_stand`
--

CREATE TABLE `to_bus_stand` (
  `id` int(11) NOT NULL,
  `stand_name` varchar(255) DEFAULT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `to_bus_stand`
--

INSERT INTO `to_bus_stand` (`id`, `stand_name`, `city_id`) VALUES
(4, 'Colombo', 19),
(5, 'Delkanda', 13),
(6, 'Galle', 3),
(7, 'Gammana Rd', 9),
(8, 'Guru Vidyalaya', 10),
(9, 'Kirulapona', 15),
(10, 'Kottawa', 2),
(11, 'Mahalwaraya', 4),
(12, 'Maharagama', 11),
(13, 'Makumbura', 1),
(14, 'Morakatiya', 5),
(15, 'Nugegoda', 14),
(16, 'Pannipitiya', 8),
(17, 'Pettah', 18),
(18, 'Thimbirigasyaya', 16),
(19, 'Town Hall', 17),
(20, 'Wijayarama', 12);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_bin NOT NULL,
  `email` varchar(255) COLLATE utf8_bin NOT NULL,
  `password` varchar(255) COLLATE utf8_bin NOT NULL,
  `status` tinyint(1) DEFAULT '1',
  `verifyToken` varchar(255) COLLATE utf8_bin DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `avatar` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT 'defaultuser.png',
  `admin` tinyint(1) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `status`, `verifyToken`, `remember_token`, `created_at`, `updated_at`, `avatar`, `admin`) VALUES
(2, 'Dishani Nishadari', 'dilshaninishadari@gmail.com', '$2y$10$fSyknPlx8YexrWCLWQ3y9OLjNOXCI1fapW.kSKiIti5ilijGr.1E.', 1, NULL, '0ojAhFbrSBE8Zm0V4xbzjnXDp82EiFjQBv4CDU9UkDgFraCtkM6gcGsOXS2q', '2017-12-17 10:47:05', '2017-12-17 10:47:05', 'defaultuser.png', 1),
(3, 'User', 'user@gmail.com', '$2y$10$fSyknPlx8YexrWCLWQ3y9OLjNOXCI1fapW.kSKiIti5ilijGr.1E.', 1, NULL, 'CwvrnMcM9Zl4vugB6kh817CStqqwI5qSWNX96vvKlttpg6SRuuFnHjwfkdGy', '2017-12-17 10:47:05', '2017-12-17 10:47:05', 'defaultuser.png', 0),
(4, 'Chamuth Chamandana', 'c.chamandana@gmail.com', '$2y$10$.5aF2NhcFy3wouBAjiymEemAf8EHf3QXErDc800U2Z1YlBIHOqvfy', 1, NULL, 'puSIosq8QIBfnWS3Vy9xyoIVH7IQDj2pUDM7HPo9PYNcO9iDVoIxlfcFYycc', '2018-02-12 13:03:09', '2018-02-12 13:03:09', 'defaultuser.png', 0),
(5, 'Thushara Nishanthi', 'mmtnwijeratne@gmail.com', '$2y$10$ZYb.pChxcyBa8HEfWJe9n.IKeo5Y4uk7ap3x1AVyK7/naMiXGgpN2', 1, NULL, 'SRq7RpMxu8ZjQKao5tnsIpnUrN5Jo33zqstjFy4Vrnpf2SkvwJ6MpMuqLRmQ', '2018-02-15 10:52:21', '2018-02-15 10:52:21', 'defaultuser.png', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ref_UNIQUE` (`ref`),
  ADD KEY `fk_booking_time_table1_idx` (`time_table_id`),
  ADD KEY `fk_booking_customer1_idx` (`customer_id`),
  ADD KEY `fk_booking_booking_status1_idx` (`booking_status_id`),
  ADD KEY `fk_booking_payment_status1_idx` (`payment_status_id`);

--
-- Indexes for table `booking_has_seats`
--
ALTER TABLE `booking_has_seats`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_booking_has_seats_seats1_idx` (`seats_id`),
  ADD KEY `fk_booking_has_seats_booking1_idx` (`booking_id`);

--
-- Indexes for table `booking_status`
--
ALTER TABLE `booking_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_UNIQUE` (`status_name`);

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `bus_number_UNIQUE` (`bus_number`),
  ADD KEY `fk_bus_seating_layout1_idx` (`seating_layout_id`);

--
-- Indexes for table `bus_has_feature`
--
ALTER TABLE `bus_has_feature`
  ADD PRIMARY KEY (`bus_id`,`feature_id`),
  ADD KEY `fk_bus_has_feature_feature1_idx` (`feature_id`),
  ADD KEY `fk_bus_has_feature_bus1_idx` (`bus_id`);

--
-- Indexes for table `bus_info`
--
ALTER TABLE `bus_info`
  ADD PRIMARY KEY (`bus_id`);

--
-- Indexes for table `cancelation`
--
ALTER TABLE `cancelation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_cancelation_booking1_idx` (`booking_id`),
  ADD KEY `fk_cancelation_cancelation_fees1_idx` (`cancelation_fees_id`),
  ADD KEY `fk_cancelation_customer1_idx` (`customer_id`);

--
-- Indexes for table `cancelation_fees`
--
ALTER TABLE `cancelation_fees`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `description_UNIQUE` (`description`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_UNIQUE` (`city_name`);

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD KEY `fk_customer_city1_idx` (`city_id`),
  ADD KEY `fk_customer_users1_idx` (`users_id`);

--
-- Indexes for table `day_of_week`
--
ALTER TABLE `day_of_week`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `day_name_UNIQUE` (`day_name`);

--
-- Indexes for table `end_time`
--
ALTER TABLE `end_time`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `time_name_UNIQUE` (`time_name`),
  ADD UNIQUE KEY `time_value_UNIQUE` (`time_value`);

--
-- Indexes for table `extra_services`
--
ALTER TABLE `extra_services`
  ADD PRIMARY KEY (`id`),
  ADD KEY `User Constraint` (`user_id`);

--
-- Indexes for table `feature`
--
ALTER TABLE `feature`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `feature_name_UNIQUE` (`feature_name`);

--
-- Indexes for table `from_bus_stand`
--
ALTER TABLE `from_bus_stand`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `start_name_UNIQUE` (`stand_name`),
  ADD KEY `fk_from_bus_stand_city1_idx` (`city_id`);

--
-- Indexes for table `hire`
--
ALTER TABLE `hire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_hire_bus1_idx` (`bus_id`),
  ADD KEY `fk_hire_customer1_idx` (`customer_id`),
  ADD KEY `fk_hire_hireRate1_idx` (`hireRate_id`),
  ADD KEY `fk_hire_booking_status1_idx` (`booking_status_id`);

--
-- Indexes for table `hirerate`
--
ALTER TABLE `hirerate`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menuview`
--
ALTER TABLE `menuview`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_status`
--
ALTER TABLE `payment_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_UNIQUE` (`status_name`);

--
-- Indexes for table `reports`
--
ALTER TABLE `reports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `seating_layout`
--
ALTER TABLE `seating_layout`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `layout_name_UNIQUE` (`layout_name`);

--
-- Indexes for table `seats`
--
ALTER TABLE `seats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `seat_number_UNIQUE` (`seat_number`),
  ADD KEY `fk_seats_bus1_idx` (`bus_id`);

--
-- Indexes for table `sitecontent`
--
ALTER TABLE `sitecontent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `start_time`
--
ALTER TABLE `start_time`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `time_name_UNIQUE` (`time_name`),
  ADD UNIQUE KEY `time_value_UNIQUE` (`time_value`);

--
-- Indexes for table `time_table`
--
ALTER TABLE `time_table`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_time_table_from_bus_stand_idx` (`from_bus_stand_id`),
  ADD KEY `fk_time_table_to_bus_stand1_idx` (`to_bus_stand_id`),
  ADD KEY `fk_time_table_day_of_week1_idx` (`day_of_week_id`),
  ADD KEY `fk_time_table_start_time1_idx` (`start_time_id`),
  ADD KEY `fk_time_table_end_time1_idx` (`end_time_id`),
  ADD KEY `fk_time_table_bus1_idx` (`bus_id`);

--
-- Indexes for table `to_bus_stand`
--
ALTER TABLE `to_bus_stand`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `start_name_UNIQUE` (`stand_name`),
  ADD KEY `fk_to_bus_stand_city1_idx` (`city_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `booking_has_seats`
--
ALTER TABLE `booking_has_seats`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `booking_status`
--
ALTER TABLE `booking_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `bus`
--
ALTER TABLE `bus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `cancelation`
--
ALTER TABLE `cancelation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cancelation_fees`
--
ALTER TABLE `cancelation_fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `day_of_week`
--
ALTER TABLE `day_of_week`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `end_time`
--
ALTER TABLE `end_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `extra_services`
--
ALTER TABLE `extra_services`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `feature`
--
ALTER TABLE `feature`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `from_bus_stand`
--
ALTER TABLE `from_bus_stand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `hire`
--
ALTER TABLE `hire`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `hirerate`
--
ALTER TABLE `hirerate`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `menuview`
--
ALTER TABLE `menuview`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payment_status`
--
ALTER TABLE `payment_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `reports`
--
ALTER TABLE `reports`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seating_layout`
--
ALTER TABLE `seating_layout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `seats`
--
ALTER TABLE `seats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `sitecontent`
--
ALTER TABLE `sitecontent`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=142;

--
-- AUTO_INCREMENT for table `start_time`
--
ALTER TABLE `start_time`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `time_table`
--
ALTER TABLE `time_table`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `to_bus_stand`
--
ALTER TABLE `to_bus_stand`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `booking`
--
ALTER TABLE `booking`
  ADD CONSTRAINT `fk_booking_booking_status1` FOREIGN KEY (`booking_status_id`) REFERENCES `booking_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_payment_status1` FOREIGN KEY (`payment_status_id`) REFERENCES `payment_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_time_table1` FOREIGN KEY (`time_table_id`) REFERENCES `time_table` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `booking_has_seats`
--
ALTER TABLE `booking_has_seats`
  ADD CONSTRAINT `fk_booking_has_seats_booking1` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_booking_has_seats_seats1` FOREIGN KEY (`seats_id`) REFERENCES `seats` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bus`
--
ALTER TABLE `bus`
  ADD CONSTRAINT `fk_bus_seating_layout1` FOREIGN KEY (`seating_layout_id`) REFERENCES `seating_layout` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bus_has_feature`
--
ALTER TABLE `bus_has_feature`
  ADD CONSTRAINT `fk_bus_has_feature_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_bus_has_feature_feature1` FOREIGN KEY (`feature_id`) REFERENCES `feature` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `bus_info`
--
ALTER TABLE `bus_info`
  ADD CONSTRAINT `Bus Constraint` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`);

--
-- Constraints for table `cancelation`
--
ALTER TABLE `cancelation`
  ADD CONSTRAINT `fk_cancelation_booking1` FOREIGN KEY (`booking_id`) REFERENCES `booking` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cancelation_cancelation_fees1` FOREIGN KEY (`cancelation_fees_id`) REFERENCES `cancelation_fees` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_cancelation_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `customer`
--
ALTER TABLE `customer`
  ADD CONSTRAINT `fk_customer_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_customer_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `extra_services`
--
ALTER TABLE `extra_services`
  ADD CONSTRAINT `User Constraint` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `from_bus_stand`
--
ALTER TABLE `from_bus_stand`
  ADD CONSTRAINT `fk_from_bus_stand_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `hire`
--
ALTER TABLE `hire`
  ADD CONSTRAINT `fk_hire_booking_status1` FOREIGN KEY (`booking_status_id`) REFERENCES `booking_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hire_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hire_customer1` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_hire_hireRate1` FOREIGN KEY (`hireRate_id`) REFERENCES `hirerate` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `seats`
--
ALTER TABLE `seats`
  ADD CONSTRAINT `fk_seats_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `time_table`
--
ALTER TABLE `time_table`
  ADD CONSTRAINT `fk_time_table_bus1` FOREIGN KEY (`bus_id`) REFERENCES `bus` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_time_table_day_of_week1` FOREIGN KEY (`day_of_week_id`) REFERENCES `day_of_week` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_time_table_end_time1` FOREIGN KEY (`end_time_id`) REFERENCES `end_time` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_time_table_from_bus_stand` FOREIGN KEY (`from_bus_stand_id`) REFERENCES `from_bus_stand` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_time_table_start_time1` FOREIGN KEY (`start_time_id`) REFERENCES `start_time` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_time_table_to_bus_stand1` FOREIGN KEY (`to_bus_stand_id`) REFERENCES `to_bus_stand` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `to_bus_stand`
--
ALTER TABLE `to_bus_stand`
  ADD CONSTRAINT `fk_to_bus_stand_city1` FOREIGN KEY (`city_id`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
