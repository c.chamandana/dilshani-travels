<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteContent extends Model
{
    protected $table = 'sitecontent';
    protected $fillable = ['contentname','content','pagename','isimage'];
    public $timestamps = false;
}
