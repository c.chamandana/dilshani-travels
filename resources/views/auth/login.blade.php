<html>
    <head>
        <meta charset="utf-8">
        {{-- Tell IE to support the latest version --}}
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Log in | Dilshani Travels</title>

        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('/bower_components/font-awesome/css/font-awesome.min.css') }}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{ asset('/bower_components/Ionicons/css/ionicons.min.css') }}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/dist/css/AdminLTE.min.css') }}">
        <!-- iCheck -->
        <link rel="stylesheet" href="{{ asset('/bower_components/admin-lte/plugins/iCheck/square/blue.css') }}">

        @include("Front.css.main")

        {{-- Google Fonts link for Quicksand font --}}
        <link href="https://fonts.googleapis.com/css?family=Quicksand" rel="stylesheet">

        {{-- CSS for the Login Form --}}
        <link href="{{ asset('/css/login.css')}}" rel="stylesheet" type="text/css" />

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    {{-- Login Page Container --}}
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="/"><b>Dilshani</b> Travels</a>
            </div>
            <!-- /.login-logo -->
            <div class="login-box-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="{{ route('login') }}" method="post">
                    {{-- CSRF validation --}}
                    {{ csrf_field() }}
                    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" class="form-control" placeholder="Email" id="email" name="email" value="{{ old('email') }}" required autofocus>
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                        <input type="password" class="form-control" placeholder="Password" id="password" name="password" required="">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> <span class="checkbox-label">Remember Me</span>
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-4">
                            <button type="submit" class="sign-in-btn btn btn-primary btn-block btn-flat">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <!-- /.social-auth-links -->

                <a class="left link" style="float:left" href="{{ route('password.request') }}">Forgotten password</a>
                <a class="right link" style="float:right" href="{{ route('register') }}" class="text-center">Register new user</a>

            </div>
            <!-- /.login-box-body -->
        </div>
        <!-- /.login-box -->

        <!-- jQuery 3 -->
        <script src="{{ asset('/bower_components/jquery/dist/jquery.min.js') }}"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="{{ asset('/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
        <!-- iCheck -->
        <script src="{{ asset('/bower_components/admin-lte/plugins/iCheck/icheck.min.js') }}"></script>
        <!-- Notify JS -->
        <script type="text/javascript" src="{{ asset("/bower_components/remarkable-bootstrap-notify/dist/bootstrap-notify.min.js") }}"></script>

        <script>
            $(function () {
                $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });

                AOS.init(); // Initialize the AOS system
            });
        </script>

        @if (Session::has("message") && Session::has("message-type"))
            <script type="text/javascript">
                $.notify({
                    // options
                    message: "{{ Session::get("message") }}"
                },{
                    // settings
                    type: '{{ Session::get("message-type") }}',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });   
            </script>
        @endif
    </body>
</html>