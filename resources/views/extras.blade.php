@extends('layouts.admin')
<?php 
    session()->forget('selectedDate');
    $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();

    // Get selected bus
    $type = app('request')->input('type');
    $busNo = "";

    if (isset($type))
    {
        if ($type === "bushire")
        {
            $busid = app('request')->input('bus');

            if (isset($busid))
            {
                // Get the selected bus info
                $bus = \App\Bus::where("id", $busid)->first();
                $busNo = $bus->bus_number;
            }
        }
    }
?>
@section('mainContent')

    <div class="container extras-container">

        <div class="row">
            <div class="col col-lg-7 col-md-8 col-sm-12 col-xs-12">
                <div class="box">
                    <div class="box-body no-bottom-padding">
                        <h4 class="sector-title">EXTRA SERVICES APPLICATION</h4>

                        <form action="/extra/submit" enctype="multipart/form-data" method="POST">
                            {{ csrf_field() }}

                            <label for="name">Your Name</label>
                            <input value="{{ $user->name }}" placeholder="eg: John Doe" type="text" name="name" id="name" class="form-control rounded margin-bottom validation">
                            
                            <label for="email">Your Phone Number</label>
                            <input value="{{ $user->phone }}" placeholder="eg: 07X XXX XXXX" type="tel" name="phone" id="phone" class="form-control rounded margin-bottom validation">

                            <label for="email">Your Email</label>
                            <input value="{{ $user->email }}" placeholder="eg: johndoe@sample.com" type="email" name="email" id="email" class="form-control rounded margin-bottom validation">

                            <label for="service">Select an extra service from us</label>
                            <select name="service" id="service" class="need-select margin-bottom">
                                <option @if (app('request')->input('type') == "tourism") selected @endif value="Tourism">Tourism</option>
                                <option @if (app('request')->input('type') == "bushire") selected @endif value="Special Journey">Special Journey</option>
                                <option @if (app('request')->input('type') == "company") selected @endif value="Company Hires">Company Hires</option>
                            </select>

                            <div id="special-journey-fields" style="display:none">
                                <label id="bus_no">Preferred Bus</label>
                                <input value="{{ $busNo }}" placeholder="Bus Number of the selected bus" type="text" name="bus_no" id="bus_no" class="form-control rounded margin-bottom validation">

                                <label id="journey_start">Journey Start Location</label>
                                <input placeholder="Specify the location where your journey should start" type="text" name="journey_start" id="journey_start" class="form-control rounded margin-bottom validation">

                                <label id="journey_destination">Journey Destination</label>
                                <input placeholder="Specify the destination of your journey" type="text" name="journey_destination" id="journey_destination" class="form-control rounded margin-bottom validation">
                            </div>

                            <label for="description">Explain your need</label>
                            <textarea name="description" placeholder="Please describe your need for our service in detail" id="description" class="validation form-control rounded textarea-height"></textarea>

                            <button id="submit-button" type="submit" class="btn btn-primary submit-button">SUBMIT APPLICATION</button>
                        </form>
                    </div>
                </div>
            </div>

            <div class="col col-lg-5 col-md-4 col-sm-12 col-xs-12">
                <div class="box">
                    <div class="box-body no-bottom-padding">
                        <h4 class="sector-title">NEED HELP?</h4>
                        <p>Contact our Customer Support at <a href="tel:0912233272">091 2233272</a> or <a href="tel:076729399">076 729399</a> or Email us at <a href="mailto:dilshanitravels@gmail.com">dilshanitravels@gmail.com</a> </p>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
@stop

@section('scripts')

@if (Session::has("message") && Session::has("message-type"))
    <script type="text/javascript">
        $.notify({
            // options
            message: "{{ Session::get("message") }}"
        },{
            // settings
            type: '{{ Session::get("message-type") }}',
            placement:
            {
                from: "bottom",
                align: "right"
            }
        });   
    </script>
@endif

<script type="text/javascript">

    var special_journey = $("#special-journey-fields");

    $("#service").change(function()
    {
        updateForm();
    });

    function updateForm()
    {
        var selected = ($("#service").val());
        special_journey.slideUp();

        if (selected == "Special Journey")
        {
            special_journey.slideDown();
        }
    }

    updateForm();

    $("#submit-button").click(function(e)
    {
        var $inputs = $(".validation:visible");

        var invalid = false;
        for (var i = 0; i < $inputs.length; i++)
        {
            var current = $inputs[i];
            if (!$.validate(current))
            {
                invalid = true;

                $(current).addClass("invalid");
            }
        }

        if (invalid)
        {
            $.notify({
                // options
                message: "Please correct the fields in red"
            },{
                // settings
                type: 'danger',
                placement:
                {
                    from: "bottom",
                    align: "right"
                }
            }); 
            e.preventDefault();
        } 
    });
</script>

@stop
