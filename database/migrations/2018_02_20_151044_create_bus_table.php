<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bus', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('bus_name')->nullable();
			$table->string('bus_number', 15)->nullable()->unique('bus_number_UNIQUE');
			$table->integer('seat_count')->nullable();
			$table->integer('seating_layout_id')->index('fk_bus_seating_layout1_idx');
			$table->boolean('is_for_hire')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bus');
	}

}
