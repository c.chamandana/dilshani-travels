<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bus_info', function(Blueprint $table)
		{
			$table->integer('bus_id')->primary();
			$table->string('driver_name');
			$table->string('conductor_name');
			$table->string('driver_phone');
			$table->string('conductor_phone');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bus_info');
	}

}
