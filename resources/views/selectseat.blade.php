@extends('layouts.admin')
@section('mainContent')

<?php 
    
    $timetable_entry = \App\TimeTable::where('id', app('request')->input('ttID'))->first();

    $origin = \App\FromBusStand::where("id", $timetable_entry->from_bus_stand_id)->first()->stand_name;
    $destination = \App\ToBusStand::where("id", $timetable_entry->to_bus_stand_id)->first()->stand_name;

?>

<div class="modal fade" id="payment-process-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="row bs-wizard" style="border-bottom:0;">
                    <div id="step1_dot" class="col-xs-6 bs-wizard-step active">
                      <div class="text-center bs-wizard-stepnum">Select Payment Method</div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                    </div>
                    
                    <div id="step2_dot" class="col-xs-6 bs-wizard-step disabled">
                      <div class="text-center bs-wizard-stepnum">Confirm Purchase</div>
                      <div class="progress"><div class="progress-bar"></div></div>
                      <a href="#" class="bs-wizard-dot"></a>
                    </div>
                </div>

                <div class="step-content" id="step1">
                    <div class="row">
                        <h4>Payment Platforms</h4>
                        <div class="payment-method col col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>
                                <img height="100px" src="{{ asset ('/assets/images/sponsors/paypal.png') }}" for="paypal"><br>
                                <input type="radio" id="paypal" name="payment-method" value="paypal">
                            </label>
                        </div>
                        <div class="payment-method col col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>
                                <img height="100px" src="{{ asset ('/assets/images/sponsors/ezcash.png') }}" for="paypal"><br>
                                <input type="radio" id="ezcash" name="payment-method" value="ezcash">
                            </label>
                        </div>
                        <div class="payment-method col col-lg-4 col-md-4 col-sm-4 col-xs-12">
                            <label>
                                <img height="100px" src="{{ asset ('/assets/images/sponsors/mcash.png') }}" for="paypal"><br>
                                <input type="radio" id="mcash" name="payment-method" value="mcash">
                            </label>
                        </div>
                    </div>

                    <div class="row top-padding">
                        <h4>Credit / Debit Cards</h4>
                        <div class="payment-method col col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>
                                <img style="padding-bottom: 25px;padding-top: 25px;" height="100px" src="{{ asset ('/assets/images/sponsors/visa.png') }}" for="paypal"><br>
                                <input type="radio" id="visa" name="payment-method" value="visa">
                            </label>
                        </div>
                        <div class="payment-method col col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>
                                <img height="100px" src="{{ asset ('/assets/images/sponsors/amex.png') }}" for="paypal"><br>
                                <input type="radio" id="amex" name="payment-method" value="amex">
                            </label>
                        </div>
                        <div class="payment-method col col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>
                                <img style="padding-top: 10px;padding-bottom: 10px;" height="100px" src="{{ asset ('/assets/images/sponsors/mastercard.png') }}" for="paypal"><br>
                                <input type="radio" id="mastercard" name="payment-method" value="mastercard">
                            </label>
                        </div>
                        <div class="payment-method col col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <label>
                                <img style="padding-top: 10px;padding-bottom: 10px;" height="100px" src="{{ asset ('/assets/images/sponsors/discover.jpeg') }}" for="paypal"><br>
                                <input type="radio" id="mastercard" name="payment-method" value="mastercard">
                            </label>
                        </div>
                    </div>
                </div>

                <div class="step-content" id="step2" style="display:none">
                    <table class="table table-bordered">
                        <p style="text-align:left;font-weight:bold;">ORDER SUMMARY</p>
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">Item</th>
                                <th scope="col">Unit Price</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Line Price</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th scope="row">{{ $origin }} - {{ $destination }} Adult Bus Ticket</th>
                                <td>LKR. 650.00</td>
                                <td id="tickets_count">2</td>
                                <td id="tickets_price">LKR. 1300.00</td>
                            </tr>
                            <tr>
                                <th scope="row">Tax</th>
                                <td>LKR. 75.00</td>
                                <td></td>
                                <td>LKR. 75.00</td>
                            </tr>
                        </tbody>
                    </table>

                    Are you sure you want to confirm the purchase and switch to the Payment Platform?
                </div>            

            </div>
            <div class="modal-footer">
                <a id="next-confirm-button" type="button" class="btn btn-primary">Next</a>
            </div>
        </div>
    </div>
</div>

<div class="container">
   
<div class="box">
    <div class="box-body">
        <h4 class="sector-title">PURCHASE TICKETS</h4>
        <form id="seatsForm" method="post" action="saveSeats">
            <div class="col-md-3">
                <label>Select Date</label><br>
                <input class="date-select" type="date" name="bookingDate" required="" min="<?php 
                if(session()->has("selectedDate")){
                    echo session('selectedDate');
                }else{
                    echo date('Y-m-d');
                }
                ?>" <?php 
                if (session()->has("selectedDate")) {
                    echo 'readonly=""';
                    echo 'value="'. session('selectedDate').'"';
                }
                ?>/>
            </div>
            <div class="col-md-6">
                <label>Select Seats</label>
                <div class="divTable" style="width: 100%;" >
                    <div class="divTableBody">
                        <div class="divTableRow">
                            <div class="divTableCell btn" title="1">1</div>
                            <div class="divTableCell btn" title="2">2</div>
                            <div class="divTableCell btn" title="3">3</div>
                            <div class="divTableCell btn" title="4">4</div>
                            <div class="divTableCell btn" title="5">5</div>
                            <div class="divTableCell btn" title="6">6</div>
                            <div class="divTableCell btn" title="7">7</div>
                            <div class="divTableCell btn" title="8">8</div>
                            <div class="divTableCell btn" title="9">9</div>
                            <div class="divTableCell btn" title="10">10</div>
                            <div class="divTableCell btn" title="11">11</div>
                            <div class="divTableCell btn" title="12">12</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell btn" title="13">13</div>
                            <div class="divTableCell btn" title="14">14</div>
                            <div class="divTableCell btn" title="15">15</div>
                            <div class="divTableCell btn" title="16">16</div>
                            <div class="divTableCell btn" title="17">17</div>
                            <div class="divTableCell btn" title="18">18</div>
                            <div class="divTableCell btn" title="19">19</div>
                            <div class="divTableCell btn" title="20">20</div>
                            <div class="divTableCell btn" title="21">21</div>
                            <div class="divTableCell btn" title="22">22</div>
                            <div class="divTableCell btn" title="23">23</div>
                            <div class="divTableCell btn" title="24">24</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCel   novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell btn" title="25">25</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell  novisible"></div>
                            <div class="divTableCell btn" title="26">26</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell btn" title="27">27</div>
                            <div class="divTableCell btn" title="28">28</div>
                            <div class="divTableCell btn" title="29">29</div>
                            <div class="divTableCell btn" title="30">30</div>
                            <div class="divTableCell btn" title="31">31</div>
                            <div class="divTableCell btn" title="32">32</div>
                            <div class="divTableCell btn" title="33">33</div>
                            <div class="divTableCell btn" title="34">34</div>
                            <div class="divTableCell btn" title="35">35</div>
                            <div class="divTableCell btn" title="36">36</div>
                            <div class="divTableCell btn" title="37">37</div>
                            <div class="divTableCell btn" title="38">38</div>
                        </div>
                        <div class="divTableRow">
                            <div class="divTableCell btn" title="39">39</div>
                            <div class="divTableCell btn" title="40">40</div>
                            <div class="divTableCell btn" title="41">41</div>
                            <div class="divTableCell btn" title="42">42</div>
                            <div class="divTableCell btn" title="43">43</div>
                            <div class="divTableCell btn" title="44">44</div>
                            <div class="divTableCell btn" title="45">45</div>
                            <div class="divTableCell btn" title="46">46</div>
                            <div class="divTableCell btn" title="47">47</div>
                            <div class="divTableCell btn" title="48">48</div>
                            <div class="divTableCell btn" title="49">49</div>
                            <div class="divTableCell btn" title="50">50</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <label>Proceed to Payment</label><br>

                <div class="purchase-price">
                    <div class="row">
                        <div class="col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span class="count" id="seat-count">0 x </span> <span class="item">Adult Seat(s)</span>
                        </div>
                        <div class="align-right col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span id="seat-price">LKR. 0</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span class="item">Tax</span>
                        </div>
                        <div class="align-right col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span id="seat-price">LKR. 75.00</span>
                        </div>
                    </div>

                    <div class="row subtotal">
                        <div class="col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span class="item">Sub-total</span>
                        </div>
                        <div class="align-right col col-lg-6 col-md-6 col-sm-6 col-xs-6 align-left">
                            <span id="subtotal">LKR. 75.00</span>
                        </div>
                    </div>
                </div>

                {{ csrf_field() }}
                <input type="hidden" id="bookingID" name="bookingID"/>
                <input type="hidden" id="seats" name="seats"/>
                <input type="submit" value="Book Now" class="book-btn"/>
            </div>
        </form>
    </div>
</div>

 <div class="row">
    <?php $busid = \App\TimeTable::where('id', app('request')->input('ttID'))->first()->bus_id;
        $busno = \App\Bus::where("id", $busid)->first()->bus_number;
            $businfo = \App\BusInfo::where("id", $busid)->first(); ?>
        <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-body" style="padding-bottom:20px;">
                    <h4 class="sector-title">BUS INFORMATION</h4>

                    <label for="bus-no">Bus No.</label>
                    <input value="{{ $busno }}" readonly id="bus-no" type="text" class="form-control">
                    <br>
                    <label for="bus-no">Driver Name</label>
                    <input value="{{ $businfo->driver_name }}" readonly id="bus-no" type="text" class="form-control">
                    <br>
                    <label for="bus-no">Driver Phone</label>
                    <input value="{{ $businfo->driver_phone }}" readonly id="bus-no" type="text" class="form-control">
                    <br>
                    <label for="bus-no">Conductor Name</label>
                    <input value="{{ $businfo->conductor_name }}" readonly id="bus-no" type="text" class="form-control">
                    <br>
                    <label for="bus-no">Conductor Phone</label>
                    <input value="{{ $businfo->conductor_phone }}" readonly id="bus-no" type="text" class="form-control">
                </div>
            </div>
        </div>
        <div class="col col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="box">
                <div class="box-body">
                    <h4 class="sector-title">ROUTE MAP</h4>

                    <iframe
                      width="100%"
                      height="450"
                      frameborder="0" style="border:0"
                      src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyCmJ-GViWmgK9BgHcWdvCsMNT6XQzSybhc&origin={{ $origin }}&destination={{ $destination }}&avoid=tolls|highways" allowfullscreen>
                    </iframe>
                </div>
            </div>
        </div>
    </div>

</div>
@stop
@section('scripts')

<script type="text/javascript">

    $("#next-confirm-button").click(function() 
    {
        var $step1 = $("#step1");
        var $step2 = $("#step2");
        var $step1_dot = $("#step1_dot");
        var $step2_dot = $("#step2_dot");

        var _visible1 = $step1.is(':visible');
        var _visible2 = $step2.is(':visible');     

        if (_visible1)
        {
            $radios = $("input[type='radio']");
            var checked = false;
            for(var i = 0; i < $radios.length; i++)
            {
                if ($($radios[i]).prop("checked") == true)
                {
                    checked = true;
                }
            }

            if (checked)
            {
                 $step1_dot.addClass("complete").removeClass("active");
                $step2_dot.addClass("active").removeClass("disabled");
                $(this).html("Confirm");

                $step1.fadeOut(250, () => 
                {
                    $step2.fadeIn();       
                });
            } else {
                $.notify({
                    // options
                    message: "Please select a Payment Method to continue"
                },{
                    // settings
                    type: 'danger',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });
            }
        }
        else if (_visible2)
        {
            // Ending the shit out of it
            $('#seatsForm').submit();
        }
    });
</script>

<script>
    $(document).ready(function () {
        var reservations = [];
        $.urlParam = function (name) {
            var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
            if (results == null) {
                return null;
            } else {
                return decodeURI(results[1]) || 0;
            }
        }
        $("#bookingID").val($.urlParam("ttID"));

        $.ajax({
            url: "getSeats/" + $.urlParam("ttID"),
            dataType: 'json',
            type: 'get',
            success: function (data, textStatus, jQxhr) {
                console.log(data);
                var json = data;
                var all = $(".divTableCell").map(function () {
                    return this.title;
                }).get();
                for (var i = 0; i < json.length; i++) {
                    if ($.inArray(json[i].title, all)) {
                        if (json[i].status !== 2 && json[i].status !== 5) {
                            $("div[title|='" + json[i].title + "']").addClass('Booked');
                            $("div[title|='" + json[i].title + "']").removeClass("btn");
                        }
                    }
                }
            }
        });
        $('#seatsForm').submit(function (e) {
            if ($(".BookedNow").length != 0)
            {
                var seats = $(this).find("input[id=seats]");
                var all = $(".BookedNow").map(function () {
                    return this.title;
                }).get();
                //reservations = [];
                //for (var i = 0; i < all.length; i++) {
                //  reservations.push({seat: all[i]});
                //}

                // Set the seats count
                seats.val(all);


                // Set the values in the modal
                var $ticket_count = $("#tickets_count");
                var $ticket_price = $("#tickets_price");

                $ticket_count.html($(".BookedNow").length);
                $ticket_price.html("LKR. " + ($(".BookedNow").length * 650).toString());

                // Show the confirm purchase modal
                if (($("#payment-process-modal").data('bs.modal') || {}).isShown)
                {
                    // Continue the execution
                }else{
                    
                    $("#payment-process-modal").modal();
                    e.preventDefault();
                
                }

            } else {
            
                $.notify({
                    // options
                    message: "No seats were selected"
                },{
                    // settings
                    type: 'danger',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });

                e.preventDefault();
            }
        });

        $('.divTableCell.btn').click(function () {
            if ($(this).hasClass('BookedNow')) {
                $(this).removeClass('BookedNow');
            } else if(!($(this).hasClass('Booked'))){
                $(this).addClass('BookedNow');
            }

            // Count number of seats reserved by the user
            var seats = $(".BookedNow").length;
            $("#seat-count").html(seats + " x ");
            $("#seat-price").html("LKR. " + (650 * seats).toString() + ".00");
            $("#subtotal").html("LKR. " + (650 * seats + 75).toString() + ".00");
        });
    });
</script>
@stop