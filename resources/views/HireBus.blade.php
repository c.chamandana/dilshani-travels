@extends('layouts.admin')

@section('mainContent')
    <div class="container">

    	<h4>SELECT A BUS TO CONTINUE</h4>
        
        <div class="search-container">
            <input type="text" class="form-control" name="search-text" placeholder="Search a bus">
            <button class="btn btn-primary"><i class="fa fa-search"></i></button>
        </div>

        <div class="bus-container">
        	<div class="row">
        		@foreach (\App\Bus::all() as $bus)
        		<?php $businfo = \App\BusInfo::where("id", $bus->id)->first();  ?>
	        		<div class="col col-lg-3 col-md-4 col-sm-6 col-xs-12">
	        			@if ($bus->is_for_hire == 1)
	        			<a href="#">
	        			@else
	        			<a href="/extras?type=bushire&bus={{ $bus->id }}">
	        			@endif
		        			<div class="card bus">
		        				@if ($bus->is_for_hire == 1)
		        					<div class="status hired">ON HIRE</div>
		        				@endif
		        				<span class="bus-no">{{ $bus->bus_number }}</span>
		        				<span class="bus-driver">{{ $businfo["driver_name"] }}</span>
		        				<span class="driver-number">{{ $businfo["driver_phone"] }}</span>
		        			</div>
		        		</a>
	        		</div>
        		@endforeach
        	</div>
        </div>
    </div>
@stop
@section('scripts')
<script>
//     $(document).ready(function (){
        
//         <?php
//         if (Illuminate\Support\Facades\Session::has("hired")) {
//             Illuminate\Support\Facades\Session::forget("hired");
//             ?>
//                 swal({
//                     title:"Good job!", 
//                     text:"You Booked a Bus!\nWe Will Contact You Shortly...!!!",
//                     timer: 4000,
//                     icon:"success"});
//                 <?php
//         }
//         ?>
        
//         var table = $("#hiretbl").DataTable({
//             "searching": true,
//             paging: true,
//             "ajax": {
//                 "url": "/LoadHireTable"
//             },
//             "columns": [
//                 {"data": "id", 'title': ''},
//                 {"data": "bus_number", 'title': 'Bus No.'},
//                 {"data": "feature_name", 'title': 'Features'},
//                 {"data": "seat_count", 'title': 'Seats'},
//                 {"data": "action", "title": "ACTIONS"}
//             ],
//             "columnDefs":[
//                 {
//                     "targets": -1,
//                     "data": null,
//                     "defaultContent": '<form action="/hireDetails" method="get">\n\
// <input type="hidden" name="busID"/>\n\
// <input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
// <div class="btn-group">\n\
// <button type="submit" class="btn btn-primary" value="Book">\n\
// Book\n\
// </button>\n\
// </div>\n\
// <form>',
//                     "className": "text-center"
//                 }
//             ],
//             'rowCallback': function (row, data, index) {
//                 $(row).find('input[name=busID]').val(data['id']);
//             }
//         });
//     });
</script>
@stop