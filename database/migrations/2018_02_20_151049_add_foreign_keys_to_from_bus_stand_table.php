<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToFromBusStandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('from_bus_stand', function(Blueprint $table)
		{
			$table->foreign('city_id', 'fk_from_bus_stand_city1')->references('id')->on('city')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('from_bus_stand', function(Blueprint $table)
		{
			$table->dropForeign('fk_from_bus_stand_city1');
		});
	}

}
