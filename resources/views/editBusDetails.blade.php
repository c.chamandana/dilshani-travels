@extends('layouts.admin')

@section('mainContent')

<div class="modal fade" id="edit-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="display:inline" id="edit-modal-title">Edit Review Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="save-bus-form" validate action="/save-bus-details" method="POST" enctype="multipart/form-data">
					
				<div class="modal-body">
					{{ csrf_field() }}
						
					<label for="number">Bus Number</label>
					<input id="number" type="text" class="form-control rounded margin-bottom" name="number" required>

					<label for="driver_name">Driver Name</label>
					<input id="driver_name" type="text" class="form-control rounded margin-bottom" name="driver_name" required>

					<label for="driver_phone">Driver Phone Number</label>
					<input id="driver_phone" type="tel" class="form-control rounded margin-bottom" validate name="driver_phone" required>

					<label for="conductor_name">Conductor Name</label>
					<input id="conductor_name" type="text" class="form-control rounded margin-bottom" name="conductor_name" required>

					<label for="conductor_phone">Conductor Phone</label>
					<input id="conductor_phone" type="tel" class="form-control rounded margin-bottom" validate name="conductor_phone" required>

					<input type="checkbox" name="bus_hire" id="bus_hire" class="margin-bottom">
					<label for="bus_hire">Is bus on hire?</label>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>

			</form>
		</div>
	</div>
</div>

<div class="container messages-container">
	<div class="row">
		<h4 class="sector-title" style="margin-bottom:25px">Select Bus to Edit Details</h4>

		<div class="row">

			@foreach (\App\Bus::all() as $bus)
			<?php $businfo = \App\BusInfo::where("id", $bus->id)->first();  ?>
				<div class="col col-lg-3 col-md-4 col-sm-6 col-xs-12">
					<div  data-task="edit" data-id="{{ $bus->id }}" class="card bus modal-trigger">
						@if ($bus->is_for_hire == 1)
							<div class="status hired">ON HIRE</div>
						@endif

						<span class="bus-no">{{ $bus->bus_number }}</span>
						<span class="bus-driver">{{ $businfo["driver_name"] }}</span>
						<span class="driver-number">{{ $businfo["driver_phone"] }}</span>
					</div>
				</div>
			@endforeach

			<div class="col col-lg-3 col-md-4 col-sm-6 col-xs-12">
		    	<div class="add-book-btn modal-trigger" data-toggle="modal" data-target="#edit-review-modal" data-task="new">
		    		<div class="centered">
		    			<i class="fa fa-plus"></i>Add a new Bus
		    		</div>
		    	</div>
		    </div>

		</div>
	</div>

</div>
@stop

@section('nonShown')

@stop
@section('scripts')
<script type="text/javascript">
	$('.collapse').collapse('hide')
</script>

<script type="text/javascript">
	function fillDetails(f, id)
	{
		$.ajax(
		{
			url : "/bus/details/" + id,
			type : "GET",
			success : function(data) 
			{
				console.log(data);
				// Call the function
				f(JSON.parse(data));
			},
			error : function (err)
			{
				console.log(err);
			}
		});
	}

	$(".modal-trigger").click(function(e)
	{
		var $number = $("#number");
		var $driver_name = $("#driver_name");
		var $driver_phone = $("#driver_phone");
		var $conductor_name = $("#conductor_name");
		var $conductor_phone = $("#conductor_phone");
		var $bus_hire = $("#bus_hire");

		// Empty the results
		$number.val("");
		$driver_name.val("");
		$driver_phone.val("");
		$conductor_name.val("");
		$conductor_phone.val("");
		$bus_hire.prop('checked', false);

		var task = $(this).data("task");

		if (task == "edit")
		{
			var busID = $(this).data("id"); // Load the busID
			$("#save-bus-form").attr("action", "/bus/save/" + busID); // Set the URL to save the bus details

			fillDetails(function(data)
			{
				// Set the values
				$number.val(data.number);
				$driver_name.val(data.driver_name);
				$driver_phone.val(data.driver_phone);
				$conductor_name.val(data.conductor_name);
				$conductor_phone.val(data.conductor_phone);

				if (data.hire == 1)
					$bus_hire.prop("checked", true);
				else
					$bus_hire.prop("checked", false);

				$('.modal').modal('show'); // Show the modal

			}, busID);
		}
		else if (task == "new")
		{
			$("#save-bus-form").attr("action", "/bus/new"); // Set the URL to create a new bus
		}

	})
</script>
@stop
@section('styles')

@stop