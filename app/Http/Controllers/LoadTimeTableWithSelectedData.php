<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of LoadTimeTableWithSelectedData
 *
 * @author anura
 */
class LoadTimeTableWithSelectedData {
    public function index($start,$end,$from,$to,$date) {
        
        $whereArray = [["id","<>",0]];
        if ($start != 0) {
            array_push($whereArray,["from_bus_stand_id","=",$start]);
        }
        if ($end != 0) {
            array_push($whereArray,["to_bus_stand_id","=",$end]);
        }
        if ($from != 0) {
            array_push($whereArray,["start_time_id","=",$from]);
        }
        if ($to != 0) {
            array_push($whereArray,["end_time_id","=",$to]);
        }
        if($date != ''){
            array_push($whereArray,["day_of_week_id","=", date('w', strtotime($date))]);
        }
//        echo $start;
//        echo $end;
//        echo date('w', strtotime($date));
        $timeTables = \App\TimeTable::with('Bus','DayOfWeek','EndTime','FromBusStand','StartTime','ToBusStand')->where($whereArray)->get();//->toSql();
        
//        dd($timeTables);
        $arrTimeTable = array();
        foreach ($timeTables as $timeTable) {
            array_push($arrTimeTable, array('ID'=>$timeTable->id,'FromBusStand'=>$timeTable->FromBusStand->stand_name,'ToBusStand'=>$timeTable->ToBusStand->stand_name,'BusNo'=>$timeTable->Bus->bus_number,'Day'=>$timeTable->DayOfWeek->day_name,
                'StartTime'=>$timeTable->StartTime->time_name,'EndTime'=>$timeTable->EndTime->time_name));
        }
        $dataArray = array('data'=>$arrTimeTable);
        return json_encode($dataArray);
//        return $timeTables;
    }
}
