<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $bus_id
 * @property string $seat_number
 * @property Bus $bus
 * @property BookingHasSeat[] $bookingHasSeats
 */
class Seats extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['bus_id', 'seat_number',"title"];
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookingHasSeats()
    {
        return $this->hasMany('App\BookingHasSeat', 'seats_id');
    }
}
