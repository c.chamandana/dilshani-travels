<style>
/*=================================================
||				1. Imports 
==================================================*/
@import url(http://fonts.googleapis.com/css?family=Roboto:100,400,700);
@import url(http://fonts.googleapis.com/css?family=Roboto+Slab);

body
{
    /*font-family: 'Roboto', 'Open-Sans', sans-serif;*/
    font-family: 'Quicksand', sans-serif;
    overflow-x: hidden;
    color: {{ \App\SiteContent::find(93)->content }};
}
th,td{
    color: #000;
    text-align: center;
}

/* Bootstrap Material DateTime Picker */

.typeahead { z-index: 10000; }

.dtp-btn-cancel, .dtp-btn-ok
{
    background-color:#2565BA;
    color:white;
    border-radius:50px;
    transition:all 500ms;
    margin-left:10px;
}

.dtp-btn-cancel:hover, .dtp-btn-ok:hover
{
    color:white;
    opacity:0.8;
}

.dtp .dtp-actual-meridien a.selected { background: #2565BA; color: #fff; }
/*Owl Carousel*/
.owl-prev, .owl-next
{
    position: absolute;
    top: 43%;
    font-size: 22px;
    padding: 5px;
}
.owl-next
{
    right: -40px;
}
.owl-prev
{
    left: -40px;
}
.owl-prev:before
{
    content: "\f3d2";
    font-family: 'ionicons';
    /*color: #fff;*/
}
.owl-next:before
{
    content: "\f3d3";
    font-family: "ionicons";
    /*color: #fff;*/
}


.section-title{
    color: {{ \App\SiteContent::find(94)->content }};
    text-align: center;
}
.section-subtitle{
    color: {{ \App\SiteContent::find(95)->content }};
    text-align: center;
    margin-bottom: 50px;
}
.section-wrapper{
    padding-top: 70px;
    padding-bottom: 70px;
}
p{
    color: {{ \App\SiteContent::find(96)->content }};
}
.border-radius{
    border-color: {{ \App\SiteContent::find(97)->content }};
    height: 45px;
}
p:hover{
    cursor: default;
}
.navbar-toggle {
    background-color: #2565BA;
    border: 1px solid #2565BA;
    border-radius: 0;
    transition: all .4s ease-in-out;
}
.navbar-default .navbar-toggle {
    border-color: #2565BA;
}
.navbar-default .navbar-toggle .icon-bar{
    background: #fff;
}
.navbar-default .navbar-toggle:focus, .navbar-default .navbar-toggle:hover{
    background: #fff;
}
.navbar-toggle:hover .icon-bar, .navbar-toggle:focus .icon-bar {
    background-color: #2565BA;
}
.form-control:focus {
    border-color: {{ \App\SiteContent::find(98)->content }};
    outline: 0;
    -webkit-box-shadow: none;
    box-shadow: none;
}
/*=================================================
||				3. Header
==================================================*/
/*navbar*/

.navbar
{
    -webkit-box-shadow: 0px 6px 28px 1px rgba(0,0,0,0.2);
    -moz-box-shadow: 0px 6px 28px 1px rgba(0,0,0,0.2);
    box-shadow: 0px 6px 28px 1px rgba(0,0,0,0.2);
}
.navbar-default .navbar-brand{
    border: 1px solid transparent;
    color: #2565BA;
    font-size: 33px;
    padding: 10px 0px;
    height: auto;
    font-weight: 400;
    letter-spacing: -1px;
    text-transform: capitalize;
}
ul.nav.navbar-nav.navbar-right{
    margin-top: 13px;
}
.navbar-default .navbar-brand span{
    color: #583b54;
}
.navbar-default .navbar-brand:focus, .navbar-default .navbar-brand:hover{
    color: #2565BA;
}
.navbar-default {
    background-color: #fff;
    border-color: transparent;
    padding: 5px 0px;
    border-radius: 0;
    text-transform: uppercase;
}
.navbar-default .navbar-nav>li>a{
    color: #2565BA;
    padding-left: 20px;
    border: 1px solid transparent;
    font-size: 12px;
    font-weight: 600;
    letter-spacing: 1px;
    transition: all .4s ease-in-out;
    font-family: 'Rubik', sans-serif;
}
.navbar-default .navbar-nav>li>a:hover{
    color: #2565BA;
}
.navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover{
    color: #2565BA;
    background: #e8f9ff;
}
.navbar-nav>li>a {
    padding-top: 8px;
    margin-left:15px;
    padding-bottom: 8px;
    background-color:transparent !important;
} 
.navbar-nav>li>a:hover {
    border-radius:50px;
    color:white !important; 
    background-color:#2565BA !important;
} 

.log-btn
{
    border:none !important;
    border-radius:50px !important;
    background-color:#2565BA !important;
    color:white !important;
    padding:10px 15px !important;
    font-weight:bold;
    margin-left:25px !important;
}

.log-btn:hover
{
    text-decoration:none;
}

.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, .navbar-default .navbar-nav>.active>a:hover{
    background-color: transparent;
    color: #2565BA;
    background: #e8f9ff;
}
/*Home*/
.flexslider{
    text-align: center;
    text-transform: uppercase;
    color: #fff;
    margin: 0;
    padding: 0;
    border: 0;
    overflow-x: hidden;
    background: transparent;
    /*background-image: url('../images/b2-min.jpg');*/
    background-repeat: no-repeat;
    background-size: cover;

}

.flexslider li {
    position: relative;
}

.flexslider li .meta {
    position: absolute;
    bottom: 200px;
    left: 20px;
    color: white;
    display: flex;
    flex-direction: column;
    justify-content: flex-start;
    align-items: flex-start;
    font-family: 'Roboto', sans-serif;
}

.searcher-information
{
    position:absolute;
    top:50%;
    left:50%;
    z-index:1000;
    transform:translateX(-50%) translateY(-50%);
    text-align:center;
}

.searcher-information h1
{
    text-transform:uppercase;
    text-shadow: 2px 2px rgba(0,0,0,0.5);
    color:white;
    font-weight:bold;
    font-size:45px;
    display:inline-block;
    padding:10px 15px;
    letter-spacing:1px;
}

.searcher-information h2
{
    text-shadow: 2px 2px rgba(0,0,0,0.5);
    text-transform:uppercase;
    color:white;
    font-weight:bold;
    font-size:18px;
    display:inline-block;
    margin-top:5px !important;
    margin-bottom:10px !important;
    padding:10px 15px;
}

.searcher
{
    background-color:white;
    overflow:hidden;
    border-radius:10px;
    margin-top:50px;
    -webkit-box-shadow: 0px 8px 36px 0px rgba(0,0,0,0.61);
    -moz-box-shadow: 0px 8px 36px 0px rgba(0,0,0,0.61);
    box-shadow: 0px 8px 36px 0px rgba(0,0,0,0.61);
}

.searcher .input-group
{
    border:none !important;
    border-radius:0 !important;
}

.searcher td
{
    padding:0;
}
.searcher .search-btn
{
    padding:10px 15px;
    font-size:18px;
    background-color:#2565BA;
    color:white;
    transform:scale(1.1);
}
.searcher .search-btn:hover
{
    background-color:#13335D;
}
.searcher input
{
    border:none !important;
    height:50px;
    font-size:16px;
}

.searcher .input-group-addon
{
    border: none;
    border-left:1px solid rgba(0,0,0,0.2);
    padding-left:20px;
    font-size:16px;
    background-color: white;
}

.flexslider li h1 {
    background: rgba(51, 51, 51, 0.5);
    padding: 12px 18px 12px;
    margin-bottom: 0;
    font-size: 1.8em;
    font-weight: 300;
}

.flexslider li h2 {
    background: rgba(51, 51, 51, 0.5);
    padding: 13px 18px 11px;
    font-size: 1.1em;
    margin-bottom: 0;
    font-weight: 300;
}

.flexslider li .category {
    display: flex;
    flex-direction: row;
}

.flexslider li .category p {
    background: #e43837;
    margin-right: 7px;
    font-size: 1.1em;
    padding: 12px 18px 10px;
    font-weight: 300;
}

.flexslider li .category span {
    background: #e43837;
    margin-top: 17px;
    padding: 8px 12px 0;
    font-size: 0.9em;
    font-weight: 300;
    height: 26px;
}

.flexslider li h1,
.flexslider li h2,
.flexslider li .category p,
.flexslider li .category span {
    -webkit-animation-duration: .6s;
    animation-duration: .6s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation-name: fadeOutLeft;
    animation-name: fadeOutLeft;
}

.flexslider li.flex-active-slide .meta h1,
.flexslider li.flex-active-slide .meta h2,
.flexslider li.flex-active-slide .meta .category p,
.flexslider li.flex-active-slide .meta .category span {
    -webkit-animation-delay: .4s;
    animation-delay: .4s;
    -webkit-animation-duration: .6s;
    animation-duration: .6s;
    -webkit-animation-fill-mode: both;
    animation-fill-mode: both;
    -webkit-animation-name: fadeInLeft;
    animation-name: fadeInLeft;
}

.flexslider li.flex-active-slide .meta h2 {
    -webkit-animation-delay: .5s;
    animation-delay: .5s;
}

.flexslider li.flex-active-slide .meta .category p {
    -webkit-animation-delay: .6s;
    animation-delay: .6s;
}

.flexslider li.flex-active-slide .meta .category span {
    -webkit-animation-delay: .8s;
    animation-delay: .8s;
}

.flex-direction-nav {
    position: absolute;
    top: 0;
    left: 30px;
}

.flex-direction-nav a {
    text-decoration: none;
    display: block;
    width: 30px;
    height: 30px;
    background-color: #333;
    margin: -20px 0 0;
    position: absolute;
    top: 50px;
    z-index: 10;
    overflow: hidden;
    opacity: .5;
    cursor: pointer;
    color: #fff;
    -webkit-transition: all .3s ease;
    -moz-transition: all .3s ease;
    transition: all .3s ease;
}

.flex-direction-nav .flex-prev {
    text-align: left;
    left: -10px;
}

.flex-direction-nav .flex-next {
    text-align: right;
    left: 30px;
}

.flexslider:hover .flex-prev {
    left: -10px;
}

.flexslider:hover .flex-next {
    left: 30px;
}

.flexslider:hover .flex-next:hover,
.flexslider:hover .flex-prev:hover {
    opacity: 1;
}

.flex-direction-nav a:before {
    font-family: FontAwesome;
    content: '\f104';
    font-size: 25px;
    display: flex;
    justify-content: center;
    align-items: center;
    position: relative;
    top: 0;
    padding: 2px;
    font-size: 19px;
    line-height: 27px;
}

.flex-direction-nav a.flex-next:before {
    content: '\f105';
}

@-webkit-keyframes fadeInLeft {
    0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
    }
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
    }
}

@keyframes fadeInLeft {
    0% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
    }
    100% {
        opacity: 1;
        -webkit-transform: none;
        transform: none;
    }
}

@-webkit-keyframes fadeOutLeft {
    0% {
        opacity: 1;
    }
    100% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
    }
}

@keyframes fadeOutLeft {
    0% {
        opacity: 1;
    }
    100% {
        opacity: 0;
        -webkit-transform: translate3d(-100%, 0, 0);
        transform: translate3d(-100%, 0, 0);
    }
}

.slider-item{
    background-size: inherit;
    background-position: left bottom;
    background-repeat: no-repeat;
}
.inner-intro{
    display: table-cell;
    vertical-align: middle;
    position: relative;
}
.intro{
    display: table;
    text-align: center;
    width: 100%;
    height: 100vh;
}
.header-title{
    font-size: 34px;
    font-weight: 400;
}
.header-title span{
    font-weight: 700;
}
.header-sub-title{
    font-size: 14px;
    color: #fff;
    margin: 20px 40px 25px;
}
.custom-btn{
    background: #fff;
    color: {{ \App\SiteContent::find(99)->content }};
    border-color: #FFF;
    border-radius: 0px;
    text-transform: uppercase;
    font-size: 12px;
    letter-spacing: 3px;
    font-weight: 1200;
    transition: all .4s ease-in-out;
    width: 200px;
    height: 35px;
}
.custom-btn:hover{
    background: #fff;
    color: {{ \App\SiteContent::find(100)->content }};
}
/*=================================================
||				4. Find a Tour
==================================================*/
.form-dropdown{
    margin-top: -25px;
}
.custom-addon{
    background: #fff;
}
.border-right{
    border-right: 0px;
}
.custom-button{
    background: {{ \App\SiteContent::find(101)->content }};
    color: #fff;
    width: 100%;
    text-align: center;
    padding: 10px;
    font-size: 16px;
    transition: all .4s ease-in-out;
}
.custom-button:hover{
    background: {{ \App\SiteContent::find(102)->content }};
    border-color: {{ \App\SiteContent::find(102)->content }};
    color: #fff;
}
.tour .input-group{
    margin-bottom: 22px;
}
/*=================================================
||				5. Our Blazzing offers
==================================================*/
.offer{
    color: {{ \App\SiteContent::find(103)->content }};
    text-align: center;
    background: {{ \App\SiteContent::find(104)->content }};
}
.offer-item{
    background: #fff;
    letter-spacing: 1px;
    padding: 30px;
    transition: all .7s ease-in;
    margin: 10px 0px;
    height: 340px; box-shadow: 0 0 30px rgba(125, 125, 125, 0.1);
    box-shadow: 0 0 2px rgba(125, 125, 125, 0.1);
    transition:all 500ms;
}
.icon{
    padding: 20px 0;
    font-size: 35px;
    color: {{ \App\SiteContent::find(105)->content }};
}
.offer-item h3{
    padding-bottom: 40px;
    margin: 0px;
    font-size: 15px;
}
.offer-item p{
    padding-bottom: 20px;
    font-size: 13px;
    line-height: 170%;
}
.offer-item:hover{
    box-shadow: 0 0 30px rgba(125, 125, 125, 0.2);
}


/*=================================================
||				6. Top place to visit
==================================================*/
.visit-item{
    padding: 0px 15px;
    transition: all .4s ease-in-out;
    opacity: .6;
    height:200px;
    width:500px;
}
.visit-item:hover{
    opacity: 1;
}
.visit .owl-theme .owl-controls .owl-nav [class*=owl-]{
    background: #fff;
    color: {{ \App\SiteContent::find(106)->content }};
    font-size: 25px;
}

/*=================================================
||				7. offer-cta
==================================================*/
.offer-cta{
    background: url("../images/cta-1.png") right center;
    background-size: cover;
}
.offering{
    float: right;
    color: {{ \App\SiteContent::find(106)->content }};
    text-align: center;
    padding: 32px 0;
}
.percent{
    font-style: italic;
    font-size: 68px;
}
.percent span{
    font-size: 117px;
    font-weight: 700;
    font-style: normal;
}
.FTour{
    font-size: 30px;
    font-weight: 200;
    margin-top: -20px;
}
.price-btn{
    background: {{ \App\SiteContent::find(106)->content }};
    color: #fff;
    border-color: {{ \App\SiteContent::find(106)->content }};
    text-transform: capitalize;
    margin: 20px;
}
.price-btn:hover{
    color: {{ \App\SiteContent::find(106)->content }};
    background-color: #fff;
    border-color: {{ \App\SiteContent::find(106)->content }};
}
/*=================================================
||				8. Additional Services
==================================================*/
.additional-services .col-md-4{
    margin-bottom: 30px;
}
.custom-table{
    display: table;
}
/*.additional-services .custom-table:hover{
        box-shadow: 0 0 30px rgba(125, 125, 125, 0.1);
}*/
.add-srvc-img{
    left: 0;
    top: 0;
    bottom: 0;
}
.add-srvc-heading{
    color: #313131;
    font-size: 16px;
}
.add-srvc-detail{
    display: table-cell;
    vertical-align: middle;
    padding-left: 10px;
    background-color: {{ \App\SiteContent::find(108)->content }};
    color: {{ \App\SiteContent::find(108)->content }};
    font-size: 13px;
    padding-right: 30px;
}
/*=================================================
||				9. Sponsor
==================================================*/
.sponsor{
    text-align: center;
    background: {{ \App\SiteContent::find(107)->content }};
}
.sponsor-item{
    /*width: 100px !important; */
    padding: 30px;
    opacity: .3;
    transition: all .4s ease-in-out;
    margin-bottom: 30px;
}
.sponsor-item:hover{
    opacity: 1;
}
.owl-theme .owl-dots .owl-dot.active span, .owl-theme .owl-dots .owl-dot:hover span{
    background: {{ \App\SiteContent::find(106)->content }};
}
.owl-theme .owl-dots .owl-dot span{
    background: #f1f1f1;
}
/*=================================================
||				10. Subscribe
==================================================*/
.subscribe{
    text-align: center !important;
}
.brand-logo{
    font-size: 33px;
    color: {{ \App\SiteContent::find(106)->content }};
}

a.brand-logo i
{
    margin-right:15px
}

a.brand-logo span{
    color: {{ \App\SiteContent::find(99)->content }};
}
a.brand-logo:focus, a.brand-logo:hover{
    color: {{ \App\SiteContent::find(106)->content }};
    text-decoration: none;
}

.subscribe-now{
    color: {{ \App\SiteContent::find(109)->content }};
    font-size: 13px;
    margin: 40px 0 20px;
}
.custom-sub-btn{
    background-color: {{ \App\SiteContent::find(106)->content }};
    color: #fff;
    border-color: {{ \App\SiteContent::find(106)->content }};
    transition: all .4s ease-in-out;
}
.custom-sub-btn:hover{
    background: {{ \App\SiteContent::find(102)->content }};
    border-color: {{ \App\SiteContent::find(102)->content }};
    color: #fff;
}
/*.input-group .custom-sub{
        width: 300px;
}*/
.social-icon{
    text-align: center;
    list-style-type: none;
    margin: 30px 0 0;
    padding: 0;
    font-size: 20px;
}
.social-icon li{
    display: inline;
    padding: 5px;
    margin-left:6px;
    margin-right:6px;
}
.social-icon a{
    font-size:25px !important;
    color: {{ \App\SiteContent::find(106)->content }};
}
.social-icon a:hover{
    color: {{ \App\SiteContent::find(102)->content }};
}

.top {
    display: inline-block;
    float: right;
    background-color: {{ \App\SiteContent::find(110)->content }};
    border-radius: 4px;
}
/*=================================================
||				13. Media Query
==================================================*/

@media (max-width: 991px){
    .offer-cta{
        background: url("../images/cta-1.png") bottom left no-repeat;
    }
    .offering{
        padding-bottom: 300px;
    }
    .navbar-nav li
    {
        margin-bottom:5px;
    }
}
@media (max-width: 480px){
    .slider-item{
        background-image: none !important;
    }
    .navbar-nav li
    {
        margin-bottom:5px;
    }
    .slide{
        height: auto;
    }
    .intro{
        max-height: 627px;
    }
    .navbar-default .navbar-nav>li>a{
        font-weight: 400;
    }
    .navbar-default .navbar-brand{
        padding-left: 15px;
    }
    .slides img
    {
        height:100vh !important;
        width:calc(100vh / 9 * 16) !important;
    }
}


/*=================================================
||              14. locations
==================================================*/

.location-container
{
    margin-bottom:25px;
}

.location-card
{
    position:relative;
    background-color:white;
    border-radius:5px;
    overflow:hidden;
    height:250px;
}

.location-info
{
    position:absolute;
    left:0;
    bottom:0;
    width:100%;
    padding:20px 25px;
    background-color:rgba(0,0,0,0.6);
    color:white;
    max-height:68px;
    transition:all 500ms;
}

.location-info:hover
{
    max-height:500px;
}

.location-info .location-title
{
    background-color:white;
    color:black;
    display:inline-block;
    padding:5px 10px;
    margin-bottom:15px;
    font-weight:bold;
    text-transform:uppercase;
}

.location-info .location-description
{
    color:white;
    font-weight:normal;
}

/*=================================================
||              15. sponsors
==================================================*/
.upper-padded
{
    margin-top:30px
}

/*=================================================
||              16. footer
==================================================*/
.footer .upperFooter {
  background-color: #2565BA;
  color: white !important;
  padding-top: 40px;
  padding-bottom: 25px; }
  .footer .upperFooter .brand-playstore-link {
    height: 75px;
    transform: translateY(-15px);
    float: right; }
  .footer .upperFooter .brand-logo {
    height: 30px;
    margin-bottom: 25px; }
  .footer .upperFooter .center {
    text-align: center; }
  .footer .upperFooter .non-center {
    display: inline-block;
    text-align: left; }
  .footer .upperFooter .right {
    text-align: right; }
  .footer .upperFooter a {
    display:block;
    margin-bottom:15px;
    color: rgba(255, 255, 255, 0.6) !important; }
  .footer .upperFooter a:hover {
    color: white; }
  .footer .upperFooter a:visited {
    color: white; }
  .footer .upperFooter a:focus {
    color: white; }
.footer .copyrights {
  background-color: #003270;
  color: white !important;
  padding-top: 15px;
  padding-bottom: 15px; }
  .footer .copyrights .copyrights-left {
    display: inline; 
float:left;}
    .footer .copyrights .copyrights-left span {
      font-size: 12px;
      font-weight: bold;
      opacity: 0.8; }
    .footer .copyrights .copyrights-left span:last-child {
      font-weight: normal;
      font-size: 11px;
      opacity: 0.6; }
  .footer .copyrights .copyrights-right {
    float: right;
    font-size: 11px; }
    .footer .copyrights .copyrights-right a {
      color: white !important;
      padding-right: 15px;
      border-right: solid 2px white;
      margin-right: 15px; }
    .footer .copyrights .copyrights-right a:last-child {
      margin-right: 0 !important;
      padding-right: 0 !important;
      border-right: none !important; }
    .footer .copyrights .copyrights-right a:hover {
      color: white; }
    .footer .copyrights .copyrights-right a:visited {
      color: white; }
    .footer .copyrights .copyrights-right a:focus {
      color: white; }

@media only screen and (max-width: 600px) {
  .footer .brand-playstore-link {
    display: none; }
  .footer .upperFooter .center {
    text-align: center; }
  .footer .upperFooter .right {
    text-align: center; }
  .footer .upperFooter .non-center {
    text-align: center; }
  .footer .container {
    text-align: center; }
  .footer .copyrights-right {
    margin-top: 15px;
    display: block !important;
    float: none !important;
    text-align: center; } }

.newsletter-container
{
    padding-left:100px;
    padding-right:100px;
}

/*# sourceMappingURL=footer.css.map */

/* modal */
.modal-title
{
    color:black;
    font-weight:bold;
}

.results-waiter
{
    text-align:center;
}
.modal-body
{
    color:black;
    padding:10px 15px;
}

.result-card
{
    width:100%;
    display:block;
    padding:10px 15px;
    border-radius:5px;
    color:black !important;
    border:2px solid #ddd;
    position:relative;
    margin-bottom:10px;
}


.result-card .bus-from-to
{
    font-weight:bold;
}

.result-card .bus-start-end
{
}

.book-btn
{
    background-color:#2565BA;
    color:white;
    font-weight:bold;
    padding:10px 15px;
    border-radius:50px;
    transition: all 500ms;
    border:none;
}

.book-btn:hover
{
    opacity:0.9;
    color:white;
    background-color:#2565BA;
}

.book-btn:active
{
    opacity:1;
    color:white;
    background-color:#2565BA;
}

.result-card table
{
    width:100%;
    text-align:left;
}

.result-card table tr .left
{
    text-align:left;
}

.result-card table tr .right
{
    text-align:right;
}

/* ALERTS */
.alert-danger
{
    background-color:rgba(255,50,50,1);
    color:white;
    border:none;
}

.alert-info
{
    background-color:#2565BA;
    color:white;
    border:none;
}

button.close
{
    color:white !important;
    opacity:1 !important;
}

.report-container
{
    padding-top:150px;
    padding-bottom:100px;
    color:black;
}

.report-container h4
{
    font-size:25px;
    font-weight:bold;

}

.report-form
{
    margin-top:40px
}

.report-container .bottom-margin
{
    margin-bottom:15px;
}

.report-container .report-btn
{
    background-color:#2565BA;
    color:white;
    border-radius:15px;
    padding:5px 10px;
    font-size:16px;
    font-weight:bold;
    margin-top:5px;
}

.invalid
{
    border-color:rgb(255,50,50);
}


.tnc-container
{
    margin-top:150px;
    margin-bottom:100px;
    color:black;
    padding:0 150px;
}

.tnc-container p
{
    text-align:justify;
}

.tnc-container h4
{
    font-weight:bold;
}

.tnc-container p
{
    margin-top:20px;
    margin-bottom:20px;
}
.tnc-container h5
{
    font-weight:bold;
}

.dilshani-travels-text
{
    font-weight:bold;
}

.tnc-container p .number
{
    margin-right:10px;
}

.lobster-two
{
    font-family:"Lobster Two", cursive;
}

/* TESTIMONIALS */
.testimonial-carousel
{
    overflow:hidden;
    text-align:center;
    padding-top:50px;
    padding-bottom:50px;
    text-decoration:none !important;
}

.testimonial-carousel:hover
{
    text-decoration:none !important;
    text-decoration-line:0 !important;
}

.testimonial
{
    pointer-events:none;
}

.user-name
{
    font-weight:bold;
    margin-bottom:5px;
}

.user-position
{
    font-size:12px;
}

.testimonial .user-image
{
    border-radius:50%;
}

.testimonial .sentence
{
    margin-top:10px;
    margin-bottom:10px;
    font-weight:lighter;
    font-size:18px;
    text-decoration:none !important;
}

.owl-next,.owl-prev
{
    display:none;
}

.testimonial .inline-stars
{
    margin-top:25px;
    margin-bottom:10px;
}
.testimonial .inline-stars i
{
    display:inline;
}

.glyphicon-star
{
    font-size:18px;
}

.glyphicon.gold
{
    color:gold;
}

.glyphicon.gray
{
    color:gray;
}

.gallery{
    max-width: 1000px;
    width: 100%;
    overflow-x: hidden;
    
}


.gallery .owl-stage{
    display: flex;
    text-decoration:none !important;
}

.owl-stage:hover
{
    text-decoration:none !important;
}
</style>