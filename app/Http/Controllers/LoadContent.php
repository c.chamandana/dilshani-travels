<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of LoadContent
 *
 * @author anura
 */
class LoadContent {
    public function index() {
        $all = \App\SiteContent::all();
        
        $data = array("data"=>$all);
        
        return json_encode($data);
    }
}
