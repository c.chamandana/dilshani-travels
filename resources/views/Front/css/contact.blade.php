<style>
.contact-and-map .section-title{
	font-weight: 400;
	text-align: left;
	margin-bottom: 40px;
}
#googleMap{
	width: 100%;
	height: 414px;
}
.message{
	height: 180px;
}
.form .input-group{
	margin-bottom: 15px;
}
.contacts{
	background: {{ \App\SiteContent::find(89)->content }};
	text-align: center;
}
.contact{
	height:250px;
	padding: 35px 0;
	background: white;
	transition: all .4s ease-in-out;
	box-shadow:0 5px 10px rgba(0,0,0,0.2);
}
.contact:hover{
	background: #2565BA;
}
.contact:hover .contact-icon, .contact:hover .contact-name, .contact:hover .contact-detail{
	color: {{ \App\SiteContent::find(87)->content }};
}
.contact-icon{
	color: #2565BA;
	font-size: 40px;
	transition:all 500ms;
}
.contact-name{
	color: #2565BA;
	font-weight:bold;
	margin-top:10px;
	margin-bottom:25px;
	font-size: 15px;
	transition:all 500ms;
}
.contact-detail{
	color: {{ \App\SiteContent::find(92)->content }};
	line-height: 180%;
	font-size: 13px;
	transition:all 500ms;
}
</style>