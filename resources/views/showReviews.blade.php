@extends('layouts.admin')

@section('mainContent')

<div class="modal fade" id="edit-review-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" style="display:inline" id="edit-modal-title">Edit Review Details</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form id="review-save-form" action="/" method="POST" enctype="multipart/form-data">
					
				<div class="modal-body">
					{{ csrf_field() }}
					<label for="name">Customer Name</label>
					<input placeholder="eg: John Doe" type="text" id="name" name="name" class="form-control margin-bottom rounded">

					<label for="image">Customer Profile Image URL</label>
					<input placeholder="eg: /assets/img/125.jpg" type="text" id="image" name="image" class="form-control margin-bottom rounded">

					<label for="position">Customer's Job / Position</label>
					<input placeholder="eg: CEO at ACME Corporation" type="text" id="position" name="position" class="form-control margin-bottom rounded">

					<label for="sentence">Customer Testimonial</label>
					<input placeholder="eg: This is the best website I've ever used" type="text" id="sentence" name="sentence" class="form-control margin-bottom rounded">

					<label for="rating">Customer Rating</label>
					<input type="hidden" name="rating" id="rating_control">
					<div id="rating" class="inline-stars big-inliner">
                        <i id="star-1" data-index="1" class="glyphicon glyphicon-star gray rate-btn"></i>
						<i id="star-2" data-index="2" class="glyphicon glyphicon-star gray rate-btn"></i>
						<i id="star-3" data-index="3" class="glyphicon glyphicon-star gray rate-btn"></i>
						<i id="star-4" data-index="4" class="glyphicon glyphicon-star gray rate-btn"></i>
						<i id="star-5" data-index="5" class="glyphicon glyphicon-star gray rate-btn"></i>
                    </div>
				</div>

				<div class="modal-footer">
					<button id="delete-button" class="btn btn-primary bg-danger">Delete Review</button>
					<button type="submit" class="btn btn-primary">Save Changes</button>
				</div>

			</form>
		</div>
	</div>
</div>

    <div class="container messages-container">
		<h4 class="sector-title">Manage Reviews</h4>

    	<div class="row review-container">
    		@foreach (App\Review::all() as $review)
	    		<div data-modal-task="edit" data-edit="{{ $review->id }}" class="modal-trigger col col-lg-4 col-md-4 col-md-6 col-sm-12 col-xs-12" data-toggle="modal" data-target="#edit-review-modal">
		    		<div class="card review">
		    			<div class="edit-icon">
		    				<i class="fa fa-edit"></i> EDIT
		    			</div>

		    			<img src="{{ $review->image }}" height="100px" height="100px" class="user-image">

		    			<div class="inline-stars">
	                        @for ($i = 0; $i < $review->stars; $i++)
	                            <i class="glyphicon glyphicon-star gold"></i>
	                        @endfor

	                        @for ($i = 0; $i < 5 - $review->stars; $i ++)
	                            <i class="glyphicon glyphicon-star gray"></i>
	                        @endfor
	                    </div>

		    			<p class="sentence">&quot;{{ $review->sentence }}&quot;</p>
		    			<p class="name">{{ $review->name }}</p>
		    			<p class="position">{{ $review->position }}</p>
		    		</div>
		    	</div>
	    	@endforeach

	    	<div data-modal-task="new" class="modal-trigger col col-lg-4 col-md-4 col-md-6 col-sm-12 col-xs-12" data-toggle="modal" data-target="#edit-review-modal">
	    		<div class="add-review-card">
		    		<span class="add-plus"><i class="fa fa-plus"></i><br>Add a new User Testimonial</span>
		    	</div>
	    	</div>
    	</div>

    </div>
@stop

@section('nonShown')

@stop
@section('scripts')
	<script type="text/javascript">
		$('.collapse').collapse('hide')
	</script>

	<script type="text/javascript">
		function setStars(index)
		{
			$("#rating_control").val(index);

			// Add gray to all
			for (var i = 1; i <= 5; i ++)
			{
				$("#star-" + i).removeClass("gold").removeClass("gray").addClass("gray");
			}

			// Add gold to the selected
			for (var i = index; i > 0; i --)
			{
				$("#star-" + i).removeClass("gray").addClass("gold");
			}
		}

		$(".rate-btn").click(function()
		{
			var index = $(this).data("index");

			setStars(index); // Stargaze			
		});

		$("#delete-button").click(function(e)
		{
			e.preventDefault();
			window.location = "/delete/review/" + $(this).data("delete");
		});

		$(".modal-trigger").click(function(e) 
		{
			var task = $(this).data("modal-task");
			var $title = $("#edit-modal-title");
			var $form = $("#review-save-form");

			var $name = $("#name");
			var $position = $("#position");
			var $sentence = $("#sentence");
			var $rating = $("#rating_control");
			var $image = $("#image");
			var $delete_button = $("#delete-button");

			if (task == "edit")
			{
				$delete_button.show();

				$title.html("Edit a Review");
				var edit = $(this).data("edit");
				$form.attr("action", "/edit/review/" + edit);

				$delete_button.data("delete", edit);

				// Get the review data from the database
				$.ajax({
					url : "/get/review/" + edit,
					success: function(data)
					{
						// Parse the JSON data out
						data = JSON.parse(data);

						// Set data
						$name.val(data.name);
						$position.val(data.position);
						$sentence.val(data.sentence);
						$image.val(data.image);
						setStars(data.rating);

						$("#edit-review-modal").modal();
					},
					error : function(er)
					{
						$.notify({
		                    // options
		                    message: "Could not load data from the server"
		                },{
		                    // settings
		                    type: 'danger',
		                    placement:
		                    {
		                        from: "bottom",
		                        align: "right"
		                    }
		                }); 
					}
				})

				e.preventDefault();
				
			}
			else if (task == "new")
			{
				$delete_button.hide();
				$title.html("Create new Review");
				$form.attr("action", "/create/review");

				// Empty out the text
				$name.val("");
				$position.val("");
				$sentence.val("");
				$image.val("");
				setStars(0);
			}
		});

	</script>
@stop
@section('styles')
    
@stop