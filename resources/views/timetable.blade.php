@extends('layouts.admin')
<?php 
    session()->forget('selectedDate');
?>
@section('mainContent')

{{-- Modal to show the application has been successfully submitted --}}
<div class="modal fade" id="extra-service-thanks" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body padded-container">
        <div class="text-center">
            <h4>APPLICATION SUCCESSFULLY SUBMITTED</h4>
        </div>

        <div class="left-right-padding text-center">
            <p>Please wait, one of our Support Agents will contact you within the next few minutes</p>
        </div>
      </div>
      <div class="modal-footer">
        <a type="button" class="btn btn-primary" data-dismiss="modal">OK</a>
      </div>
    </div>
  </div>
</div>

{{-- Contains the table itself --}}
<div class="container">
<table id="timetbl" class="table table-striped table-bordered" width='100%'>
    <thead>
        <tr>
            <th></th>
            <th>From</th>
            <th>To</th>
            <th>Bus No.</th>
            <th>Day</th>
            <th>Start Time</th>
            <th>End Time</th>
            <th>Book</th>
        </tr>
    </thead>
</table>
</div>
@stop

@section('scripts')

<script>
    $(document).ready(function () {
        var table = $("#timetbl").DataTable({
            "searching": true,
            paging: true,
            "ajax": {
                "url": "/LoadTimeTable"
            },
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<div class="btn-group"><form action="/seating" method="get">\n\
<input type="hidden" name="ttID"/>\n\
<input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
\n\
<button type="submit" class="btn btn-primary" value="Book">\n\
Book\n\
</button>\n\
\n\
</form></div>\n\
<div class="btn-group">\n\
<form action="/dailySeating" method="get">\n\
<input type="hidden" name="ttID"/>\n\
<input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
\n\
<button type="submit" class="btn btn-primary" value="Book">\n\
Daily\n\
</bu1tton>\n\
</form>\n\
</div>\n\
',
                    "className": "text-center"            
                },
                {
                    "targets": 0,
                    "visible": false
                }],
            "columns": [
                {"data": "ID", 'title': ''},
                {"data": "FromBusStand", 'title': 'From'},
                {"data": "ToBusStand", 'title': 'To'},
                {"data": "BusNo", 'title': 'Bus No.'},
                {"data": "Day", 'title': 'Day'},
                {"data": "StartTime", 'title': 'Start Time'},
                {"data": "EndTime", 'title': 'End Time'},
                {"data": "action", "title": "ACTIONS"}
            ],
            'rowCallback': function (row, data, index) {
                $(row).find('input[name=ttID]').val(data['ID']);
            }
//            "order": [[2, "desc"]]
        });


        var q = getParameterByName("q");

        if (q)
        {
            var search = decodeURI(q); // Decode the search query
            var $input = $("input[name=q]");

            $input.val(search); // Set the input values
            table.search(search).draw(); // Update the table
        }
        
    });


</script>

<script type="text/javascript">
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;

        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
</script>

@if (Session::has("modal"))
    @if (Session::get("modal") == "extra-service-modal")
        <script type="text/javascript">
            $("#extra-service-thanks").modal();
            $("#extra-service-thanks").modal("open");
        </script>  
    @endif
@endif

@stop
