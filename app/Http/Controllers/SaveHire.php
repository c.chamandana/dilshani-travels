<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/**
 * Description of SaveHire
 *
 * @author anura
 */
class SaveHire {
    public function index(Request $request) {
        \App\hire::create([
            'bus_id'=>$request->input("busID"),
            'customer_id'=>$request->input("cusID"),
            'booking_status_id'=>9, 
            'fromdate'=>$request->input("FromDate"),
            'todate'=>$request->input("ToDate"), 
            'fromplace'=>$request->input("start"), 
            'toplace'=>$request->input("destination"), 
            'km'=>$request->input("km"),
            'hireRate_id'=>1, 
            'actual_end_date'=>$request->input("ToDate"),
            'actual_milage'=>$request->input("km"),
            'total'=>0]);
        \Illuminate\Support\Facades\Session::put("hired",true);
        return redirect()->route("hire");
    }
}
