<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $seating_layout_id
 * @property string $bus_name
 * @property string $bus_number
 * @property int $seat_count
 * @property SeatingLayout $seatingLayout
 * @property Feature[] $features
 * @property Seat[] $seats
 * @property TimeTable[] $timeTables
 */
class Bus extends Model {

    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'bus';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['seating_layout_id', 'bus_name', 'bus_number', 'seat_count'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seatingLayout() {
        return $this->belongsTo('App\SeatingLayout');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function features() {
        return $this->belongsToMany('App\Feature', 'bus_has_feature');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function seats() {
        return $this->hasMany('App\Seat');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeTables() {
        return $this->hasMany('App\TimeTable');
    }
    
    public function hires() {
        return $this->hasMany("App\hire");
    }

}
