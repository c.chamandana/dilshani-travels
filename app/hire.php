<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $bus_id
 * @property int $customer_id
 * @property int $booking_status_id
 * @property string $fromdate
 * @property string $todate
 * @property string $fromplace
 * @property string $toplace
 * @property float $km
 * @property int $hireRate_id
 * @property string $actual_end_date
 * @property float $actual_milage
 * @property float $total
 * @property BookingStatus $bookingStatus
 * @property Bus $bus
 * @property Customer $customer
 * @property Hirerate $hirerate
 */
class hire extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'hire';
    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = ['bus_id', 'customer_id', 'booking_status_id', 'fromdate', 'todate', 'fromplace', 'toplace', 'km', 'hireRate_id', 'actual_end_date', 'actual_milage', 'total'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bookingStatus()
    {
        return $this->belongsTo('App\BookingStatus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function hirerate()
    {
        return $this->belongsTo('App\Hirerate', 'hireRate_id');
    }
}
