<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $description
 * @property float $fee
 * @property Cancelation[] $cancelations
 */
class CancelationFees extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['description', 'fee'];
    public $timestamps = false;
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cancelations()
    {
        return $this->hasMany('App\Cancelation', 'cancelation_fees_id');
    }
}
