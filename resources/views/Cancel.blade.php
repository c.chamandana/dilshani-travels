@extends('layouts.admin')
@section('mainContent')
<div class="container" style="margin-top:50px">
    <table id="tblCancel" class="table table-striped table-bordered" width='100%'>
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
</div>
@stop
@section('scripts')
<script>
    $(document).ready(function () {
        var table = $("#tblCancel").DataTable({
            "searching": false,
            paging: true,
            "ajax": {
                "url": "/getUserPendingSummary/" + <?php echo $userID; ?>},
            "columns": [
                {"data": "id", 'title': 'ID'},
                {"data": "ref", 'title': 'Ref. No.'},
                {"data": "type", "title": "Booking Type"},
                {"data": "booked_date", "title": "Booked Date"},
                {"data": "booking_date", 'title': 'Booking Date'},
                {"data": "from", 'title': 'From'},
                {"data": "to", 'title': 'To'},
                {"data": "bus_name", 'title': 'Bus Name'},
                {"data": "bus_number", 'title': 'Bus No.'},
                {"data": "action", 'title': 'Action'}
            ],
            "columnDefs": [
                {
                    "targets": -1,
                    "data": null,
                    "defaultContent": '<form action="/cancelBooking" method="post">\n\
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
                        <input type="hidden" name="bookingID" id="bookingID/>\n\
            <div class="btn-group">\n\
<button type="submit" class="btn btn-danger" value="Cancel">\n\
Cancel\n\
</button>\n\
</div>\n\
</form>',
                    "className": "text-center"
                },
                {
                    "targets": 0,
                    "visible": false
                }
            ],
            'rowCallback': function (row, data, index) {
                $(row).find('input[name=bookingID]').val(data['id']);
            }
        });
    });
</script>
@stop