<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Auth;
use Hash;
use App\Http\Controllers\Controller;

class SettingsController extends Controller
{
    public function profileview(Request $request)
    {
    	return view("settings.profile");
    }

    public function saveProfile(Request $request)
    {
    	// Get the request input and user
    	$name = $request->name;
    	$user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();

    	if (isset($name) && isset($user))
    	{
    		$user->name = $name; // Set the name
    		$user->save();

    		// Show a success message to the user
    		Session::flash("message", "Your profile settings was saved succesfully");
    		Session::flash("message-type", "info");

    		// Return the user to the settings page
    		return redirect("/settings/profile");
    	} else {
    		// Show a danger message to the user
    		Session::flash("message", "Could not save your profile settings");
    		Session::flash("message-type", "danger");

    		// Return the user to the settings page
    		return redirect("/settings/profile");
    	}
    }

    public function changePassword(Request $request)
    {
        // Get the input from the request
        $current = $request->current_password;
        $new = $request->new_password;
        $confirm = $request->confirm_password;

        // Get the logged in user
        $user = Auth::user();

        // Make sure the current password entered by the user is correct
        if (Hash::check($current, $user->password))
        {
            $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();
            $user->password = Hash::make($new);
            $user->save();

            // Show a success message to the user
            Session::flash("message", "Your password has been saved");
            Session::flash("message-type", "info");

            // Return the user to the settings page
            return redirect("/settings/profile");
        } else {
            var_dump (Hash::check($current, $user->password));
            
            // Show a success message to the user
            Session::flash("message", "The password you entered was wrong");
            Session::flash("message-type", "danger");

            // Return the user to the settings page
            return redirect("/settings/profile");
        }
    }

    public function deleteAccount(Request $request)
    {
        // Get the user account
        $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first(); 

        if ($user)
        {
            // Delete the object from the database
            $customer = \App\Customer::where("users_id", $user->id)->first()->delete();
            $user->delete(); 

            // Show a success message to the user
            Session::flash("message", "Your account has been deleted");
            Session::flash("message-type", "info");

            // Return the user to the settings page
            return redirect("/");
        }
    }

}
