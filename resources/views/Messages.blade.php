@extends('layouts.admin')

@section('mainContent')
    <div class="container messages-container">
    	<div class="row">
    		<div class="col col-lg-8 col-md-6 col-sm-12 col-xs-12">
    			<h4 class="sector-title" style="margin-bottom:25px">Customer Messages</h4>

    			@if (count(\App\Message::all()) > 0)
	    			<div id="accordion">

						@foreach (\App\Message::all() as $message)
							<div class="card">
								<div class="card-header" id="headingOne">
									<h5 class="mb-0">
										<button class="btn-link" data-toggle="collapse" data-target="#collapse{{$message->id}}" aria-expanded="true" aria-controls="collapseOne">
											{{ substr($message->message, 0, 25) . "..."}}
										</button>

										<span class="time pull-right">{{ Carbon\Carbon::parse($message->created_at)->format("d-m-Y H:i") }}</span>
									</h5>

								</div>

								<div id="collapse{{$message->id}}" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
									<div class="card-body">
										<span class="message-meta">From : {{ $message->name }} </span>
										<span class="message-meta">Email : {{ $message->email }} </span>
										<span class="message-meta">Phone : {{ $message->phone }} </span>

										<span class="message">{{ $message->message }}</span>
									</div>
								</div>
							</div>
						@endforeach

					</div>
				@else

					<div class="no-messages-container">
						No Messages Available 						
					</div>

				@endif
    		</div>

    		<div class="col col-lg-4 col-md-6 col-sm-12 col-xs-12">
    			<div class="box">
				    <div class="box-body" style="padding-bottom:20px;">
				        <h4 class="sector-title">Customer Messages</h4>

				        <p>Messages sent by customers or potential customers will be shown here</p>
				    </div>
				</div>

				<div class="box red">
				    <div class="box-body red" style="padding-bottom:20px;">
				        <h4 class="sector-title">HELP</h4>

				       	<p>Click on a Message to expand it's content and view Message sender, sender's email, and his/her phone number</p>
				       	
				    </div>
				</div>
    		</div>
    	</div>
		

    </div>
@stop

@section('nonShown')

@stop
@section('scripts')
	<script type="text/javascript">
		$('.collapse').collapse('hide')
	</script>
@stop
@section('styles')
    
@stop