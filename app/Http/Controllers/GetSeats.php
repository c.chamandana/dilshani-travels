<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\TimeTable;
/**
 * Description of GetSeats
 *
 * @author anura
 */
class GetSeats {

    public function index($ttID) {
    	// Get the timetable entry for the bus
    	$busID = TimeTable::where("id", $ttID)->first()->bus_id;

        $command = "SELECT `seats`.`id`, `seats`.`seat_number`, IFNULL(`booking`.`booking_status_id`,2) as status,seats.title FROM `booking_has_seats`RIGHT JOIN  `seats` ON (`booking_has_seats`.`seats_id` = `seats`.`id`)LEFT JOIN  `booking` ON (`booking_has_seats`.`booking_id` = `booking`.`id`) WHERE seats.`bus_id`=".$busID;
        $input_array = array();
        $executePDOQuery = executePDOQuery($command, $input_array, false);
        echo $executePDOQuery;
    }

}
