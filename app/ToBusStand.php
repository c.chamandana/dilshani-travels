<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $city_id
 * @property string $stand_name
 * @property City $city
 * @property TimeTable[] $timeTables
 */
class ToBusStand extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'to_bus_stand';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['city_id', 'stand_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeTables()
    {
        return $this->hasMany('App\TimeTable');
    }
}
