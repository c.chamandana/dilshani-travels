<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $feature_name
 * @property Bus[] $buses
 */
class Feature extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'feature';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['feature_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function buses()
    {
        return $this->belongsToMany('App\Bus', 'bus_has_feature');
    }
}
