<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of BookingSummary
 *
 * @author anura
 */
class BookingSummary {
    public function index($bookingID) {
         $command = "SELECT `booking`.`ref` , date_format(`booking`.`booking_date`,'%Y-%m-%d') as booking_date, `customer`.`customer_name` , `seats`.`seat_number` FROM `booking` INNER JOIN `customer`  ON (`booking`.`customer_id` = `customer`.`id`) INNER JOIN `users`  ON (`customer`.`users_id` = `users`.`id`) INNER JOIN `booking_has_seats` ON (`booking_has_seats`.`booking_id` = `booking`.`id`)INNER JOIN `seats`  ON (`booking_has_seats`.`seats_id` = `seats`.`id`) where booking_has_seats.booking_id=".$bookingID."";
        $input_array = array();
        $executePDOQuery = executePDOQuery($command, $input_array, false);
        $dataArray = array("data"=> json_decode($executePDOQuery, true));
        return json_encode($dataArray,JSON_UNESCAPED_UNICODE);
    }
}
