<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $city_id
 * @property string $customer_name
 * @property string $email
 * @property string $address_line1
 * @property string $address_line2
 * @property string $mobile_no
 * @property string $password
 * @property string $recovery_code
 * @property City $city
 * @property Booking[] $bookings
 * @property Cancelation[] $cancelations
 */
class Customer extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'customer';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['city_id', 'customer_name', 'email', 'address_line1', 'address_line2', 'mobile_no', 'recovery_code','users_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo('App\City');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cancelations()
    {
        return $this->hasMany('App\Cancelation');
    }
    
    public function user() {
        return $this->belongsTo('App\User');
    }
    
    public function hires() {
        return $this->hasMany("App\hire");
    }
}
