<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ asset("/bower_components/admin-lte/dist/img/user.png") }}" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->name }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <?php
            if (Illuminate\Support\Facades\Auth::user()->admin == 1) {
                ?>
                    <li><a href = "{{ route('editContent') }}"><i class = "fa fa-edit"></i> <span>Edit Content</span></a></li>
                    <li><a href = "{{ route('user-messages') }}"><i class = "fa fa-envelope"></i> <span>Messages</span></a></li>
                    <li><a href = "{{ route('showReports') }}"><i class = "fa fa-file"></i> <span>Reports</span></a></li>
                    <li><a href = "{{ route('showReviews') }}"><i class = "fa fa-star"></i> <span>Reviews</span></a></li>
                    <li><a href = "{{ route('editBusDetails') }}"><i class = "fa fa-bus"></i> <span>Bus Details</span></a></li>
                <?php
                } else {
                  ?><li><a href = "{{ route('timetable') }}"><i class = "fa fa-calendar"></i> <span>Timetable</span></a></li>
                    <li><a href = "{{ route('UserPending') }}"><i class = "fa fa-book"></i> <span>My Bookings</span></a></li>
                    <li><a href = "{{ route('hire') }}"><i class = "fa fa-bus"></i> <span>Hire a Bus</span></a></li>
                    <li><a href = "{{ route('extras') }}"><i class = "fa fa-star"></i> <span>Extra Services</span></a></li>
                <?php
                }
                ?>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>