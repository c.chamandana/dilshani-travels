<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
    <!--<![endif]-->
    <head>
        {{-- Insert the tags for the head --}}
        @include('Front.head')

        {{-- Include CSS for all 3 of About, Contact and Services --}}
        @include('Front.css.about')
        @include('Front.css.contact')
        @include('Front.css.services')

        <!-- Attach AOS Scrolling Animation kit -->
        <link rel="stylesheet" href="{{ asset('/bower_components/aos/dist/aos.css') }}">
    </head>
    <body>

        {{-- This is used to print the results from the inline search menu of the carousel  --}}
        @include("partials.results_card")

        {{-- Include the Facebook JavaScript API --}}
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.12&appId=1553318174738433&autoLogAppEvents=1';
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        {{-- The Navigation Bar for the front-end --}}
        @include('Front.navbar')

        {{-- Marketting carousel with pictures of Buses and fixed inline search bar --}}
        <div id="header">
            <div class="flexslider">
                <ul class="slides">
                    <li>
                        <img src="{{ asset ('/assets/images/carousel/1.jpg') }}">
                    </li>
                    <li>
                        <img src="{{ asset ('/assets/images/carousel/2.jpg') }}">
                    </li>
                    <li>
                        <img src="{{ asset ('/assets/images/carousel/3.jpg') }}">
                    </li>
                </ul>
            </div> <!-- /.flexslider -->
        </div> <!-- /#header -->

        <div class="modal fade" id="searchModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <span class="modal-title" id="exampleModalLabel">Book a Bus</span>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div class="modal-body" id="results-container">
                        
                        <div class="results-waiter">
                            Please wait...
                        </div>
                       
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                </div> <!-- /.modal-content -->
            </div>
        </div>

        {{-- Inline Search bar --}}
        <!-- 

            Here users can search for available buses using several variables, such as Bus Start Time, Bus Arrival Time, Starting Point, and Destination.

        -->
        <div class="searcher-container">
            <div class="searcher-information">
                <h1 class="lobster-two" data-aos="fade-up">{{ \App\SiteContent::find(1)->content }}</h1>
                <h2 data-aos="fade-up" data-aos-delay="500">{{ \App\SiteContent::find(2)->content }}</h2>

                <div data-aos="fade-up" data-aos-delay="1500" class="searcher">
                    <table>
                        <tr>
                            <td>
                                <table style="table-layout:fixed">
                                    <tr>
                                        <td width="25%">
                                            <label class="sr-only" for="fromInput">FROM</label>
                                            <div data-toggle="popover" data-placement="right" class="input-group">
                                                <div class="input-group-addon"><i class="ion-location"></i></div>
                                                <input data-provide="typeahead" autocomplete="off" type="text" class="form-control" id="fromInput" placeholder="FROM">
                                            </div>
                                        </td>
                                        <td width="25%">
                                            <label class="sr-only" for="toInput">TO</label>
                                            <div data-toggle="popover" data-placement="right" class="input-group">
                                                <div class="input-group-addon"><i class="ion-location"></i></div>
                                                <input data-provide="typeahead" autocomplete="off" type="text" class="form-control typeahead" id="toInput" placeholder="TO">
                                            </div>
                                        </td>
                                        <td width="25%">
                                            <label class="sr-only" for="starttimeInput">START TIME</label>
                                            <div data-toggle="popover" data-placement="right" class="input-group">
                                                <div class="input-group-addon"><i class="ion-clock"></i></div>
                                                <input type="text" class="form-control" id="starttimeInput" placeholder="START TIME">
                                            </div>
                                        </td>
                                        <td width="25%">
                                            <label class="sr-only" for="endtimeInput">END TIME</label>
                                            <div data-toggle="popover" data-placement="right" class="input-group">
                                                <div class="input-group-addon"><i class="ion-clock"></i></div>
                                                <input type="text" class="form-control" id="endtimeInput" placeholder="END TIME">
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>

                            <td>
                                <div class="search-btn">
                                    <i class="ion-search"></i>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div> <!-- /.searcher-container -->

        <!-- Our Blazzing offers -->
        <section class="offer section-wrapper">
            <div class="container">
                <h2 data-aos="fade-up" class="section-title">
                    {{ \App\SiteContent::find(8)->content }}
                </h2>
                <p data-aos="fade-up" class="section-subtitle">
                    {{ \App\SiteContent::find(9)->content }}
                </p>
                <div class="row">
                    <div class="col-sm-3 col-xs-12">
                        <div class="offer-item" data-aos="flip-left" data-aos-delay="0">
                            <div class="icon">
                                <i class="ion-social-euro"></i>
                            </div>
                            <h3>
                                {{ \App\SiteContent::find(10)->content }}
                            </h3>
                            <p>
                                {{ \App\SiteContent::find(11)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-md-3 -->

                    <div class="col-sm-3 col-xs-12">
                        <div class="offer-item" data-aos="flip-right" data-aos-delay="100">
                            <div class="icon">
                                <i class="ion-ios-home"></i>
                            </div>
                            <h3>
                                {{ \App\SiteContent::find(12)->content }}
                            </h3>
                            <p>
                                {{ \App\SiteContent::find(13)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-md-3 -->

                    <div class="col-sm-3 col-xs-12">
                        <div class="offer-item" data-aos="flip-left" data-aos-delay="200">
                            <div class="icon">
                                <i class="ion-android-bus"></i>
                            </div>
                            <h3>
                                {{ \App\SiteContent::find(14)->content }}
                            </h3>
                            <p>
                                {{ \App\SiteContent::find(15)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-md-3 -->

                    <div class="col-sm-3 col-xs-12">
                        <div class="offer-item" data-aos="flip-right" data-aos-delay="300">
                            <div class="icon">
                                <i class="ion-ios-locked"></i>
                            </div>
                            <h3>
                                {{ \App\SiteContent::find(16)->content }}
                            </h3>
                            <p>
                                {{ \App\SiteContent::find(17)->content }}
                            </p>
                        </div>
                    </div> <!-- /.col-md-3 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.offer -->



        <!-- Top place to visit -->
        <section class="visit section-wrapper">
            <div class="container">
                <h2 data-aos="fade-up" class="section-title">
                    {{ \App\SiteContent::find(18)->content }}
                </h2>
                <p data-aos="fade-up" class="section-subtitle">
                    {{ \App\SiteContent::find(19)->content }}
                </p>

                <div data-aos="fade-up" class="owl-carousel visit-carousel" id="">
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(20)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(21)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(22)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(23)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(24)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                    <div class="item">
                        <img src="{{ asset (\App\SiteContent::find(25)->content) }}" alt="visit-image" class="img-responsive visit-item">
                    </div>
                </div>
            </div> <!-- /.container -->
        </section> <!-- /.visit -->

         <!-- About Section of Dilshani Travels -->
        <a id="about"></a>
        <section class="story-and-client section-wrapper">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6" data-aos="fade-up">
                        {{-- Contains the Story of Dilshani Travels --}}
                        <div class="story">
                            <h2 class="section-title">
                                {{ \App\SiteContent::find(61)->content }}
                            </h2>
                            <img src="{{ asset(\App\SiteContent::find(62)->content) }}" alt="story" class="story-img">
                            <p class="story-detail">
                                {{ \App\SiteContent::find(63)->content }}
                            </p>
                            <p class="story-detail">
                                {{ \App\SiteContent::find(64)->content }}
                            </p>
                        </div> <!-- /.story -->
                    </div> <!-- /.col-sm-6 -->

                    <div class="col-sm-6" data-aos="fade-up" data-aos-delay="200">
                        {{-- Contains the ratings of Dilshani Travels --}}
                        <div class="story">
                            <h2 class="section-title">
                                {{ \App\SiteContent::find(65)->content }}
                            </h2>
                            <table class="table _table-bordered">
                                <tbody>
                                    <tr>
                                        <td>
                                            5 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(66)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(66)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(66)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(67)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            4 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(68)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(68)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(68)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(69)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            3 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(70)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(70)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(70)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(71)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            2 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(72)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(72)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(72)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(73)->content }}
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            1 Stars
                                        </td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{ \App\SiteContent::find(74)->content }}"
                                                     aria-valuemin="0" aria-valuemax="100" style="width:{{ \App\SiteContent::find(74)->content }}%">
                                                    <span class="sr-only">{{ \App\SiteContent::find(74)->content }}% Complete</span>
                                                </div> <!-- /.progress-bar -->
                                            </div> <!-- /.progress -->
                                        </td>
                                        <td>
                                            {{ \App\SiteContent::find(75)->content }}
                                        </td>
                                    </tr>

                                </tbody>
                            </table> <!-- /.table -->
                        </div> <!-- /.story -->

                        <a id="testimonials">
                        <div class="testimonials gallery">
                            <div class="testimonial-carousel">
                                @foreach (\App\Review::all() as $review)
                                    <div class="testimonial">
                                        <img width="100px"  src="{{ $review->image }}" class="user-image">
                                        <div class="inline-stars">
                                            @for ($i = 0; $i < $review->stars; $i++)
                                                <i class="glyphicon glyphicon-star gold"></i>
                                            @endfor

                                            @for ($i = 0; $i < 5 - $review->stars; $i ++)
                                                <i class="glyphicon glyphicon-star gray"></i>
                                            @endfor
                                        </div>

                                        <p class="sentence">&quot;{{ $review->sentence }}&quot;</p>
                                        <p class="user-name">{{ $review->name }}</p>
                                        <p class="user-position">{{ $review->position }}</p>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                    </div> <!-- /.col-sm-6 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.story-and-client -->


        <!--        <div class="offer-cta">
                    <div class="container">
                        <div class="offering">
                            <div class="percent">
                                <span>15%</span> off
                            </div>
                            <div class="FTour">
                                for <strong>Family Tour</strong>
                            </div>
                            <a class="btn btn-default price-btn" href="#">
                                see our price
                            </a>
                        </div>  /.offering 
                    </div>  /.container 
                </div>  /.offer-cta -->

        <a id="destinations"></a>
        {{-- Destinations that Dilshani Travels is targeting --}}
        <section class="additional-services section-wrapper">
            <div class="container">
                <h2 data-aos="fade-up" class="section-title">
                    {{ \App\SiteContent::find(26)->content }}
                </h2>
                <p data-aos="fade-up" class="section-subtitle" style="margin-bottom:75px;">
                    {{ \App\SiteContent::find(27)->content }}
                </p>
                <div class="row location-container">
                   {{-- List of destinations / destination cards using CSS3 --}}
                    <div data-aos="flip-left" data-aos-delay="0" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/1.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">Hikkaduwa Beach</p>
                                <p class="location-description">The beach of Hikkaduwa is situated 98 km from Colombo towards the south of Sri Lanka. The coral sanctuary found along the coast of Hikkaduwa is a large shallow body of water enclosed by a reef, decorated with layers of multi coloured corals, and is home to countless numbers of vibrantly coloured fish.</p>
                            </div>
                        </div>
                    </div>

                    <div data-aos="flip-right" data-aos-delay="100" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/2.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">Galle Fort</p>
                                <p class="location-description">Galle Fort, in the Bay of Galle on the southwest coast of Sri Lanka, was built first in 1588 by the Portuguese, then extensively fortified by the Dutch during the 17th century from 1649 onwards.</p>
                            </div>
                        </div>
                    </div>

                    <div data-aos="flip-left" data-aos-delay="200" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/4.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">Galle Light House</p>
                                <p class="location-description">The Galle Lighthouse, or Pointe de Galle light as it was also known, was the very first light station to be built in Sri Lanka. Dating back to 1848; the lighthouse was built by the British to ensure the safe arrival of their ships at the Galle harbour..</p>
                            </div>
                        </div>
                    </div>
                </div> <!-- /.row -->

                <div class="row location-container">
                    
                    <div data-aos="flip-left" data-aos-delay="200" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/3.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">National Museum of Galle</p>
                                <p class="location-description">The National Museum of Galle (Galle National Museum) is one of the national museums of Sri Lanka. It is located in the oldest remaining Dutch building in the Galle fort, Galle, a single storey colonnaded Dutch building built in 1656 as the commissariat store for the Dutch garrison at the fort.</p>
                            </div>
                        </div>
                    </div>

                    <div data-aos="flip-right" data-aos-delay="300" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/5.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">Unawatuna Beach</p>
                                <p class="location-description">Unawatuna is one of the biggest tourist destinations in Sri Lanka and is the most “famous” beach in the country.</p>
                            </div>
                        </div>
                    </div>

                    <div data-aos="flip-left" data-aos-delay="400" class="col col-lg-4 md-6">
                        <div class="location-card">
                            <img src="{{ asset("/assets/images/places/6.jpg") }}" width="100%" height="100%">

                            <div class="location-info">
                                <p class="location-title">Yatagala Raja  Maha Viharaya</p>
                                <p class="location-description">2300 year old Yatagala Rajamaha Viharaya is a Buddhist place of worship that has been recipient of
Royal Patronage from Three Kings of Sri Lanka..</p>
                            </div>
                        </div>
                    </div>

                </div> <!-- /.row -->

            </div> <!-- /.container -->
        </section> <!-- /.Additional-services -->

        <a id="services"></a>
        {{-- Additional / Extra Services rendered by Dilshani Travels --}}
        <section class="additional-services section-wrapper features">
            <div class="container">
                <h2 data-aos="fade-up" class="section-title">
                    {{ \App\SiteContent::find(122)->content }}
                </h2>
                <p data-aos="fade-up" class="section-subtitle">
                    {{ \App\SiteContent::find(123)->content }}
                </p>
                <div class="row">

                    <div data-aos="flip-left" data-aos-delay="0" class="col col-lg-4 md-6">
                        {{-- TOURISM SERVICE --}}
                        <a href="/extras?type=tourism">
                            <div class="location-card">
                                <img src="{{ asset("/assets/images/services/tourism.jpg") }}" width="100%" height="100%">

                                <div class="location-info">
                                    <p class="location-title">Tourism</p>
                                    <p class="location-description">We offer transport services for tourists and visitors to suit their requirements and their budget. Also, safe with driver/guide from us and we would guarantee 100% satisfaction and comfort throughout your journey.</p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div data-aos="flip-right" data-aos-delay="100" class="col col-lg-4 md-6">
                        {{-- HIRING FOR SPECIAL JOURNEY --}}
                        <a href="/extras?type=bushire">
                            <div class="location-card">
                                <img src="{{ asset("/assets/images/services/journey.jpg") }}" width="100%" height="100%">

                                <div class="location-info">
                                    <p class="location-title">Special Journey</p>
                                    <p class="location-description">We provide our vehicle for special journey in Sri Lanka with your satisfaction.Day trips / Day Excursions from any hotel, Colombo City tour, Shopping tour, Hotel Transfers, weddings and special events etc..,</p>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div data-aos="flip-left" data-aos-delay="200" class="col col-lg-4 md-6">
                        {{-- HIRING FOR COMPANIES AND THEIR TASKS --}}
                        <a href="/extras?type=company">
                            <div class="location-card">
                                <img src="{{ asset("/assets/images/services/hire.jpg") }}" width="100%" height="100%">

                                <div class="location-info">
                                    <p class="location-title">Company Hires</p>
                                    <p class="location-description">Any company can book their willing for any time and can get the service 24 hours with excellent service.</p>
                                </div>
                            </div>
                        </a>
                    </div>

                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.Additional-services -->


        {{-- CONTACT SECTION FOR DILSHANI TRAVELS --}}
        {{-- Contact section contains information on how to connect with Dilshani Travels --}}
        <a id="contact"></a>
        <section class="section-wrapper contact-and-map">
            <div class="container">
                <div class="row">
                    <div data-aos="fade-up" class="col-sm-6">
                        <h2 class="section-title">
                            Send Message
                        </h2>
                        {{-- FORM FOR SENDING MESSAGES DIRECTLY TO DILSHANI TRAVELS --}}
                        <form  method="POST" action="/SendMessage" enctype="multipart/form-data">
                            {{ csrf_token() }}
                            <div class="form">
                                <div class="input-group">       
                                    <input name="name" class="form-control border-radius border-right" type="text" placeholder="Name" required validate>
                                    <span class="input-group-addon border-radius custom-addon">
                                        <i class="ion-person"></i>
                                    </span>
                                </div>
                                <div class="input-group">       
                                    <input name="email" class="form-control border-radius border-right" name="email" type="email" placeholder="Email address" required>
                                    <span class="input-group-addon  border-radius custom-addon">
                                        <i class="ion-email"></i>
                                    </span>
                                </div>
                                <div class="input-group">       
                                    <input name="phone" class="form-control border-radius border-right" type="tel" placeholder="Phone number">
                                    <span class="input-group-addon  border-radius custom-addon">
                                        <i class="ion-ios-telephone"></i>
                                    </span>
                                </div>
                                <div class="input-group">
                                    <textarea name="message" class="form-control border-radius border-right" rows="8" placeholder="Write Message"></textarea>  
                                            <!-- <input type="text" name="text" rows="8" class="form-control border-radius border-right message" placeholder="Write Message"> -->
                                    <span class="input-group-addon border-radius custom-addon">
                                        <i class="ion-chatbubbles"></i>
                                    </span>
                                </div>
                                <button type="submit" class="btn btn-default border-radius custom-button">SEND MESSAGE </button>
                            </div>
                        </form>
                    </div> <!-- /.col-sm-6 -->
                    {{-- A GOOGLE MAP CONTAINING THE DIRECTIONS / LOCATION OF DILSHANI TRAVELS, GALLE --}}
                    <div data-aos="fade-up" data-aos-delay="200" class="col-sm-6">
                        <h2 class="section-title">
                            Find Us Via Google Map
                        </h2>
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5944.688362667108!2d80.21386392152459!3d6.049004550142081!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ae173c40000001b%3A0x50eb682c8d711497!2sDilshani+Travels!5e0!3m2!1sen!2slk!4v1518708248695" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div> <!-- /.col-sm-6 -->
                </div>
            </div>
        </section>

        <section class="contacts section-wrapper">
            <div class="container">
                <div class="row">
                    <div data-aos="fade-up" data-aos-delay="0" class="col-sm-4">
                        <div class="contact">
                            <div class="contact-icon">
                                <i class="ion-android-map"></i>
                            </div>
                            <div class="contact-name">
                                Address
                            </div>
                            <div class="contact-detail">
                                {{ \App\SiteContent::find(78)->content }} <br>
                                {{ \App\SiteContent::find(79)->content }}
                            </div>
                        </div> <!-- /.contact -->
                    </div> <!-- /.col-sm-4 -->
                    <div data-aos="fade-up" data-aos-delay="100" class="col-sm-4">
                        <div class="contact">
                            <div class="contact-icon">
                                <i class="ion-ios-telephone"></i>
                            </div>
                            <div class="contact-name">
                                Phone
                            </div>
                            <div class="contact-detail">
                                Local: {{ \App\SiteContent::find(80)->content }} <br>
                                Mobile: {{ \App\SiteContent::find(81)->content }}
                            </div>
                        </div> <!-- /.contact -->
                    </div> <!-- /.col-sm-4 -->
                    <div data-aos="fade-up" data-aos-delay="200" class="col-sm-4">
                        <div class="contact">
                            <div class="contact-icon">
                                <i class="ion-email"></i>
                            </div>
                            <div class="contact-name">
                                Email Address
                            </div>
                            <div class="contact-detail">
                                {{ \App\SiteContent::find(82)->content }} <br>
                            </div>
                        </div> <!-- /.contact -->
                    </div> <!-- /.col-sm-4 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.contacts -->


        <div class="section-wrapper sponsor">
            <div class="container">
                <div class="owl-carousel sponsor-carousel">
                    <div data-aos="fade-up" data-aos-delay="0" class="item upper-padded">
                        <a href="https://www.visa.com.lk">
                            <img src="{{ asset ('/assets/images/sponsors/visa.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="100" class="item">
                        <a href="www.americanexpress.lk">
                            <img src="{{ asset ('/assets/images/sponsors/amex.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="200" class="item upper-padded">
                        <a href="https://www.mastercard.us/">
                            <img src="{{ asset ('/assets/images/sponsors/mastercard.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="300" class="item upper-padded">
                        <a href="http://www.boc.lk">
                            <img src="{{ asset ('/assets/images/sponsors/boc.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="400" class="item">
                        <a href="http://www.ezcash.lk/">
                            <img src="{{ asset ('/assets/images/sponsors/ezcash.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="500" class="item upper-padded">
                        <a href="http://www.ezcash.lk/">
                            <img src="{{ asset ('/assets/images/sponsors/mcash.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="600" class="item">
                        <a href="http://www.ezcash.lk/">
                            <img width="200px" src="{{ asset ('/assets/images/sponsors/sampath.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div data-aos="fade-up" data-aos-delay="700" class="item">
                        <a href="http://www.paypal.com/">
                            <img width="200px" src="{{ asset ('/assets/images/sponsors/paypal.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                   
                </div> <!-- /.owl-carousel -->
            </div> <!-- /.container -->
        </div> <!-- /.sponsor -->

        @include('Front.subscribe')
        @include('Front.footer')

        <script src="{{ asset ('/assets/js/script.js') }}"></script>
        <script src="{{ asset ('/assets/js/contact.js') }}"></script>
        <!-- AOS js -->
        <script src="{{ asset('/bower_components/aos/dist/aos.js') }}"></script>
        <script type="text/javascript" src="{{ asset("/bower_components/handlebars/handlebars.min.js") }}"></script>

        @if (Session::has("message") && Session::has("message-type"))
            <script type="text/javascript">
                $.notify({
                    // options
                    message: "{{ Session::get("message") }}"
                },{
                    // settings
                    type: '{{ Session::get("message-type") }}',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });   
            </script>
        @endif

        <script>
            $(".testimonial-carousel").owlCarousel({
                dots: false, items:1, autoplay:true, navigation: false, loop:true
            });

            function submitSearchForm(e)
            {
                $(".book-btn").click(function(e) 
                {
                    var id = this.data("id");
                    $("#ttID").val(id); // Set the ttID Value
                    alert(id);
                });
            }

            function searchTimeTable(template, container, data, query)
            {
                var start = moment(query.start, "HH:mm");
                var end = moment(query.end, "HH:mm");

                var matched = false;

                for (var i = 0; i < data.length; i ++)
                {
                    var current = data[i];
                    var busStart = moment(current.StartTime, "HH:mm");

                    var busFrom = current.FromBusStand;
                    var busTo = current.ToBusStand;

                    var difference = Math.abs(moment.duration(start.diff(busStart)).asMinutes());

                    if ( ( query.from.toLowerCase() === current.FromBusStand.toLowerCase() || 
                        query.to.toLowerCase() === current.ToBusStand.toLowerCase() ) 
                        && (difference <= 60))
                    {
                        // If the From bus stand is equal or the to bus stand is equal or the time is only deviated by one hour
                        var html = template({
                            ID : current.ID,
                            csrf_token : "{{ csrf_token() }}",
                            busFrom : busFrom,
                            busTo : busTo,
                            busStart : current.StartTime,
                            busEnd : current.EndTime,
                            busDay : current.Day
                        });

                        container.append(html);

                        matched = true;
                    } 
                }

                if (!matched)
                {
                    var $waiter = $(".results-waiter");
                    $waiter.show();
                    $waiter.html("Sorry, no results found");
                }
            }

            $(document).ready(function () {
                
                $("#startInput").popover();
                $("#toInput").popover();

                var data = JSON.parse("{{ json_encode(\App\FromBusStand::all()) }}".replace(/&quot;/g, "\"")); 
                var cities = [];

                for (var i = 0; i < data.length; i++) {
                    cities.push(data[i].stand_name);
                }

                var $fromInput = $("#fromInput");
                $fromInput.typeahead({ source : cities });
                var $toInput = $("#toInput");
                $toInput.typeahead({ source : cities });

                $('.typeahead').typeahead();

                var $startTime = $("#starttimeInput");
                var $endTime = $("#endtimeInput");
                var $resultsRemover = $(".results-waiter");

                var $searchModal = $("#searchModal");
                var $resultsContainer = $("#results-container");

                setTimeout(() => {
                    AOS.init(); // Initialize the AOS system  
                }, 2000);

                var source   = document.getElementById("result-template").innerHTML;
                var template = Handlebars.compile(source);

                $(".searcher .search-btn").click(function(event)
                {
                    // Change the text to please wait...
                    $resultsRemover.html("Please wait...");

                    var from = $fromInput.val();
                    var to = $toInput.val();
                    var start = $startTime.val();
                    var end = $endTime.val();

                    // Search for a bus
                    if (/\S/.test(from)
                        && /\S/.test(to)
                        && /\S/.test(start)
                        && /\S/.test(end))
                    {
                        $(".result-card").hide();
                        $searchModal.modal("show");
                        $resultsRemover.show();

                        $.ajax({
                            type: "GET",
                            url : "/LoadTimeTable",

                            success : function(data)
                            {   
                                $resultsRemover.hide();

                                // Search Time Table
                                searchTimeTable(template, $resultsContainer, (JSON.parse(data)).data, {
                                    from : from, to : to, start : start, end : end
                                })

                            },
                            error : function(er)
                            {

                            }
                        });
                    } else {
                        $.notify({
                            // options
                            message: 'Please fill all the fields to continue the search' 
                        },{
                            // settings
                            type: 'danger',
                            placement:
                            {
                                from: "bottom",
                                align: "right"
                            }
                        });                
                    }

                });

                $('#timetbl tfoot th').each(function () {
                    var title = $(this).text();
                    $(this).html('<input type="text" placeholder="Search ' + title + '" />');
                });
                var table = $("#timetbl").DataTable({
                    "searching": true,
                    paging: true,
                    pageLength:7,
                    "ajax": {
                        "url": "/LoadTimeTable"
                    },
                    "columnDefs": [
                        {
                            "targets": -1,
                            "data": null,
                            "defaultContent": '<form action="/seating" method="get">\n\
            <input type="hidden" name="ttID"/>\n\
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>\n\
            <div class="btn-group">\n\
            <button type="submit" class="btn btn-primary" value="Book">\n\
            Book\n\
            </button>\n\
            </div>\n\
            <form>',
                            "className": "text-center"
                        },
                        {
                            "targets": 0,
                            "visible": false
                        }],
                    "columns": [
                        {"data": "ID", 'title': ''},
                        {"data": "FromBusStand", 'title': 'From'},
                        {"data": "ToBusStand", 'title': 'To'},
                        {"data": "BusNo", 'title': 'Bus No.'},
                        {"data": "Day", 'title': 'Day'},
                        {"data": "StartTime", 'title': 'Start Time'},
                        {"data": "EndTime", 'title': 'End Time'},
                        {"data": "action", "title": "ACTIONS"}
                    ],
                    'rowCallback': function (row, data, index) {
                        $(row).find('input[name=ttID]').val(data['ID']);
                    }
            //                    ,
            //                    "order": [[2, "desc"]]
                });
                // Apply the search
                table.columns().every(function () {
                    var that = this;

                    $('input', this.footer()).on('keyup change', function () {
                        if (that.search() !== this.value) {
                            that
                                    .search(this.value)
                                    .draw();
                        }
                    });
                });

            });
        </script>
    </body>
</html>