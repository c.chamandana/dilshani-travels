<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateHireTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('hire', function(Blueprint $table)
		{
			$table->increments('id');
			$table->date('fromdate')->nullable();
			$table->date('todate')->nullable();
			$table->string('fromplace')->nullable();
			$table->string('toplace')->nullable();
			$table->decimal('km', 7)->nullable();
			$table->integer('bus_id')->index('fk_hire_bus1_idx');
			$table->integer('customer_id')->index('fk_hire_customer1_idx');
			$table->integer('hireRate_id')->unsigned()->index('fk_hire_hireRate1_idx');
			$table->integer('booking_status_id')->index('fk_hire_booking_status1_idx');
			$table->date('actual_end_date')->nullable();
			$table->decimal('actual_milage', 10)->nullable();
			$table->decimal('total', 12)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('hire');
	}

}
