<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMenuviewTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('menuview', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('menuname', 100);
			$table->string('url');
			$table->integer('level');
			$table->integer('parentmenu');
			$table->string('icon')->nullable();
			$table->boolean('admin')->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('menuview');
	}

}
