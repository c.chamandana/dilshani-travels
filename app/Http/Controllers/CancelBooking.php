<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;

/**
 * Description of CancelBooking
 *
 * @author User
 */
class CancelBooking {
    public function cancel(Request $request) {
        $booking = \App\Booking::find($request->input("bookingID"));
        $booking->booking_status_id = 2;
        $booking->save();
        return redirect()->route("UserPending");
    }
}
