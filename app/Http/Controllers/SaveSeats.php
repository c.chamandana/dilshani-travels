<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of SaveSeats
 *
 * @author anura
 */
use Illuminate\Http\Request;

class SaveSeats {

    public function index(Request $request) {
        $user = \App\User::where("id",\Illuminate\Support\Facades\Auth::user()->id)->first();

        $customer = null;

        if (\App\Customer::where("users_id", $user->id)->exists() == false)
        {
            // Make the user a new customer
            $customer = new \App\Customer;
            $customer->email = $user->email;
            $customer->customer_name = $user->name;
            $customer->address_line1 = "";
            $customer->address_line2 = "";
            $customer->city_id = "1";
            $customer->mobile_no = "";
            $customer->recovery_code = null;
            $customer->users_id = $user->id;

            $customer->save();
        } 
        else 
        {
            // Get the customer
            $customer = \App\Customer::where("users_id", $user->id)->first();
        }

        $adults = $request->input("seats");

        $booking = \App\Booking::create(
        [
            "time_table_id" => $request->input("bookingID"),
            "customer_id" => $customer->id,
            "booking_status_id" => 4,
            "payment_status_id" => 2,
            "booked_date" => date("Y-m-d"),
            "booking_date" => $request->input("bookingDate"),
            "end_date" => $request->input("bookingDate")
        ]);

        $booking->ref = "Ref".str_pad($booking->id, 8, "0", STR_PAD_LEFT);
        $booking->save();
        $seats = $request->input("seats");
        $seatsArray = explode(",", $seats);

        // Reserve the seats
        foreach ($seatsArray as $seat) {
            \App\BookingHasSeats::create([
                "booking_id" => $booking->id,
                "seats_id" => $seat,
                "ticket_printed" => false
            ]);
        }
        
        return redirect()->route("viewSummary",['bookingID'=>$booking->id]);
    }

}
