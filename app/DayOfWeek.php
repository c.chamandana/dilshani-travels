<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $day_name
 * @property TimeTable[] $timeTables
 */
class DayOfWeek extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'day_of_week';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['day_name'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeTables()
    {
        return $this->hasMany('App\TimeTable');
    }
}
