<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
    <!--<![endif]-->
    <head>
        @include('Front.head')
        @include('Front.css.services')
    </head>
    <body>

        <!-- Home -->
        <section class="header">

            @include('Front.navbar')
        </section> <!-- /#header -->

        <!-- Section Background -->
        <section class="section-background">
            <div class="container">
                <h2 class="page-header">
                    {{ \App\SiteContent::find(113)->content }}
                </h2>
                <ol class="breadcrumb">
                    <li><a href="/">&nbsp;</a></li>
                    <li class="active">&nbsp;</li>
                </ol>
            </div> <!-- /.container -->
        </section> <!-- /.section-background -->


        <section class="features section-wrapper">
            <div class="container">
                <h2 class="section-title">
                    {{ \App\SiteContent::find(114)->content }}
                </h2>
                <p class="section-subtitle">
                    {{ \App\SiteContent::find(115)->content }}
                </p>
                <div class="row custom-table">
                    <div class="grid-50 table-cell">
                        <p class="features-details">
                            {{ \App\SiteContent::find(116)->content }}
                        </p>
                        <ul class="features-list">
                            <li>{{ \App\SiteContent::find(117)->content }}</li>
                            <li>{{ \App\SiteContent::find(118)->content }}</li>
                            <li>{{ \App\SiteContent::find(119)->content }}</li>
                            <li>{{ \App\SiteContent::find(120)->content }}</li>
                        </ul>
<!--                        <a href="#" class="btn btn-default custom-button border-radius">
                            PURCHASE
                        </a>-->
                    </div>

                    <div class="grid-50 table-cell">
                        <img src="{{ asset (\App\SiteContent::find(121)->content) }}" alt="" class="features-img img-responsive _pos-abs">
                    </div>
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.features -->



        <section class="additional-services section-wrapper">
            <div class="container">
                <h2 class="section-title">
                    {{ \App\SiteContent::find(122)->content }}
                </h2>
                <p class="section-subtitle">
                    {{ \App\SiteContent::find(123)->content }}
                </p>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(124)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(125)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(126)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->

                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(127)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(128)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(129)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->

                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(130)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(131)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(132)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->

                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(133)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(134)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(135)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->

                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(136)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(137)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(138)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->

                    <div class="col-md-4 col-sm-6">
                        <div class="custom-table">
                            <img src="{{ asset (\App\SiteContent::find(139)->content) }}" alt="" class="add-srvc-img">
                            <div class="add-srvc-detail">
                                <h4 class="add-srvc-heading">
                                    {{ \App\SiteContent::find(140)->content }}
                                </h4>
                                <p class="add-srvc">
                                    {{ \App\SiteContent::find(141)->content }}
                                </p>
                            </div> <!-- /.add-srvc-detail -->
                        </div> <!-- /.custom-table -->
                    </div> <!-- /.col-md-4 col-sm-6 -->
                </div> <!-- /.row -->
            </div> <!-- /.container -->
        </section> <!-- /.Additional-services -->


<!--        <section class="section-wrapper services-owl">
            <div class="container">
                <div class="owl-carousel services-owl-carousel row">
                    <div class="item col-sm-8 col-sm-offset-2">
                        <div class="item-name">
                            David Martin
                        </div>
                        <p class="item-detail">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis, architecto ipsam laudantium corporis eveniet blanditiis eaque ab ex eum, provident culpa tenetur adipisci libero aliquid quia dolores deleniti illo.
                        </p>
                    </div>  /.item 

                    <div class="item col-sm-8 col-sm-offset-2">
                        <div class="item-name">
                            David Martin
                        </div>
                        <p class="item-detail">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis, architecto ipsam laudantium corporis eveniet blanditiis eaque ab ex eum, provident culpa tenetur adipisci libero aliquid quia dolores deleniti illo.
                        </p>
                    </div>  /.item 

                    <div class="item col-sm-8 col-sm-offset-2">
                        <div class="item-name">
                            David Martin
                        </div>
                        <p class="item-detail">
                            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eos omnis, architecto ipsam laudantium corporis eveniet blanditiis eaque ab ex eum, provident culpa tenetur adipisci libero aliquid quia dolores deleniti illo.
                        </p>
                    </div>  /.item 
                </div>  /.services-owl-carousel 
            </div>  /.container 
        </section>  /.services-owl -->


<!--        <div class="section-wrapper sponsor">
            <div class="container">
                <div class="owl-carousel sponsor-carousel">
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-1.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-2.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-3.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-4.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-5.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-6.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-1.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-2.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-3.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-4.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-5.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-6.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-1.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-2.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-3.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-4.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-5.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                    <div class="item">
                        <a href="#">
                            <img src="{{ asset ('/assets/images/sp-6.png') }}" alt="sponsor-brand" class="img-responsive sponsor-item">
                        </a>
                    </div>
                </div>  /.owl-carousel 
            </div>  /.container 
        </div>  /.sponsor -->

        @include('Front.subscribe')


        <script src="{{ asset ('/assets/js/jquery-1.11.2.min.js') }}"></script>
        <script src="{{ asset ('/assets/js/bootstrap.min.js') }}"></script>
        <script src="{{ asset ('/assets/js/owl.carousel.min.js') }}"></script>
        <script src="{{ asset ('/assets/js/contact.js') }}"></script>
        <script src="{{ asset ('/assets/js/script.js') }}"></script>






    </body>
</html>