<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
/**
 * Description of ContentEdit
 *
 * @author User
 */
class ContentEdit {
    public function index(Request $request) {
        $siteContent = \App\SiteContent::find($request->id);
        $siteContent->content = $request->content;
        $siteContent->save();
    }
    public function image(Request $request) {
        $siteContent = \App\SiteContent::find($request->input("imgid"));
        $avatar = $request->file('imgimage');
        $filename = $avatar->getClientOriginalName();
        Image::make($avatar)->resize(370, 430)->save( public_path('/assets/images/' . $filename ) );
        $siteContent->content = '/assets/images/' . $filename;
        $siteContent->save();
        return redirect()->route('editContent');
    }
}
