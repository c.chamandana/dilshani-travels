<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use App\TimeTable;
/**
 * Description of LoadTimeTable
 *
 * @author User
 */
class LoadTimeTable {
    public function index() {
        $timeTables = TimeTable::with('Bus','DayOfWeek','EndTime','FromBusStand','StartTime','ToBusStand')->get();
        $arrTimeTable = array();
        foreach ($timeTables as $timeTable) {
            array_push($arrTimeTable, array('ID'=>$timeTable->id,'FromBusStand'=>$timeTable->FromBusStand->stand_name,'ToBusStand'=>$timeTable->ToBusStand->stand_name,'BusNo'=>$timeTable->Bus->bus_number,'Day'=>$timeTable->DayOfWeek->day_name,
                'StartTime'=>$timeTable->StartTime->time_name,'EndTime'=>$timeTable->EndTime->time_name));
        }
        $dataArray = array('data'=>$arrTimeTable);
        return json_encode($dataArray);
    }
}
