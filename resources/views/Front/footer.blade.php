<footer class="footer">
    <div class="upperFooter">
        <div class="container">
            <div class="row">
                <div class="col col-lg-4 col-md-4 col-sm-12 left">
                    <a href="/#about">About</a>
                    <a href="/#destinations">Destinations</a>
                    <a href="/#testimonials">Reviews</a>
                    <a href="/#contact">Contact</a>
                </div>
                <div class="col col-lg-4 col-md-4 col-sm-12 center">
                    <div class="non-center">
                        <a href="/login">Login</a>
                        <a href="/register">Sign up</a>
                        <a href="/extras">Extra Services</a>
                        <a href="/report">Report a Problem</a>
                    </div>  
                </div>
                <div class="col col-lg-4 col-md-4 col-sm-12 right">
                    
                </div>
            </div>
        </div>
    </div>
    <div class="copyrights">
        <div class="container">
            <div class="copyrights-left">
                <span>Copyrights &copy; {{ date('Y') }} Dilshani Travels</span><br>
            </div>
        </div>
    </div>
</footer>