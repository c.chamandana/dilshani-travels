<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToHireTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('hire', function(Blueprint $table)
		{
			$table->foreign('booking_status_id', 'fk_hire_booking_status1')->references('id')->on('booking_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('bus_id', 'fk_hire_bus1')->references('id')->on('bus')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('customer_id', 'fk_hire_customer1')->references('id')->on('customer')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('hireRate_id', 'fk_hire_hireRate1')->references('id')->on('hirerate')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('hire', function(Blueprint $table)
		{
			$table->dropForeign('fk_hire_booking_status1');
			$table->dropForeign('fk_hire_bus1');
			$table->dropForeign('fk_hire_customer1');
			$table->dropForeign('fk_hire_hireRate1');
		});
	}

}
