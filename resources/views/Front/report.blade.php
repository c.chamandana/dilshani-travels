<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
    <!--<![endif]-->
    <head>
        @include('Front.head')
        @include('Front.css.about')
        @include('Front.css.contact')
        @include('Front.css.services')

        {{-- Google Maps API --}}
        <script
            src="http://maps.googleapis.com/maps/api/js">
        </script>
        <style>
            tfoot input {
                width: 100%;
                padding: 3px;
                box-sizing: border-box;
            }
        </style>

        <!-- AOS Scrolling animations -->
        <link rel="stylesheet" href="{{ asset('/bower_components/aos/dist/aos.css') }}">
    </head>
    <body>

         @include('Front.navbar')

         {{-- CONTAINS REPORTING FORM --}}
            <div class="report-container">
                <div class="container">
                    <div class="row">
                        <div class="col col-lg-push-2 col-lg-8 col-md-push-3 col-md-6 col-sm-12 col-xs-12">
                            <h4>Report a problem</h4>

                            <form class="report-form" action="/report/submit" enctype="multipart/form-data" method="POST">
                                {{ csrf_field() }}

                                <label for="name">What is your name?</label>
                                <input placeholder="John Doe" id="name" type="text" class="form-control rounded bottom-margin validate" name="name">
                                
                                <label for="phone">What is your phone number?</label>
                                <input placeholder="+94XXXXXXXXX" id="phone" type="tel" class="form-control rounded bottom-margin validate" name="phone">
                                
                                <label for="problem_type">What kind of a problem do you like to report?</label>
                                <select id="problem_type" class="form-control bottom-margin validate" name="problem_type">
                                    <option selected disabled>Please select an answer</option>
                                    <option value="bus_problem">A problem with one of our buses, their drivers or conductors</option>
                                    <option value="website_problem">A problem with our website </option>
                                    <option value="customer_service_problem">A problem with our customer service</option>
                                </select>

                                <label for="description">Please explain the problem in detail</label>
                                <textarea name="description" placeholder="" id="description" style="height:250px" class="bottom-margin validate form-control"></textarea>

                                <button type="submit" class="report-btn btn btn-primary">Report Problem</button>
                            </form>
                        </div>
                    </div>
                    
                </div>
            </div>

         @include('Front.footer')
         <script src="{{ asset ('/assets/js/script.js') }}"></script>
        <script src="{{ asset ('/assets/js/contact.js') }}"></script>
        <!-- AOS js -->
        <script src="{{ asset('/bower_components/aos/dist/aos.js') }}"></script>
        <script type="text/javascript" src="{{ asset("/bower_components/handlebars/handlebars.min.js") }}"></script>
        <script type="text/javascript" src="{{ asset("/js/validator.js") }}"></script>

        @if (Session::has("message") && Session::has("message-type"))
            <script type="text/javascript">
                $.notify({
                    // options
                    message: "{{ Session::get("message") }}"
                },{
                    // settings
                    type: '{{ Session::get("message-type") }}',
                    placement:
                    {
                        from: "bottom",
                        align: "right"
                    }
                });   
            </script>
        @endif

        <script type="text/javascript">
            function validate()
            {
                $inputs = $(".report-form .validate");

                var valid = true;

                for (var i = 0; i < $inputs.length; i ++)
                {
                    $($inputs[i]).removeClass("invalid");

                    var valid = $.validate($inputs[i]);
                    
                    if (!valid)
                    {
                        $($inputs[i]).addClass("invalid");
                        valid = false;
                    }
                }

                return valid;
            }

            $(".report-form").submit(function(e)
            {
                if (!validate())
                {
                    $.notify({
                        // options
                        message: "Please fill all the fields in red"
                    },{
                        // settings
                        type: 'danger',
                        placement:
                        {
                            from: "bottom",
                            align: "right"
                        }
                    });  

                    e.preventDefault();
                }
            });
        </script>
     </body>

</html>