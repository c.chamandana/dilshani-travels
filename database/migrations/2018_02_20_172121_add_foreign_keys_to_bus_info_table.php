<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBusInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('bus_info', function(Blueprint $table)
		{
			$table->foreign('bus_id', 'Bus Constraint')->references('id')->on('bus')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('bus_info', function(Blueprint $table)
		{
			$table->dropForeign('Bus Constraint');
		});
	}

}
