<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToBookingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('booking', function(Blueprint $table)
		{
			$table->foreign('booking_status_id', 'fk_booking_booking_status1')->references('id')->on('booking_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('customer_id', 'fk_booking_customer1')->references('id')->on('customer')->onUpdate('NO ACTION')->onDelete('CASCADE');
			$table->foreign('payment_status_id', 'fk_booking_payment_status1')->references('id')->on('payment_status')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('time_table_id', 'fk_booking_time_table1')->references('id')->on('time_table')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('booking', function(Blueprint $table)
		{
			$table->dropForeign('fk_booking_booking_status1');
			$table->dropForeign('fk_booking_customer1');
			$table->dropForeign('fk_booking_payment_status1');
			$table->dropForeign('fk_booking_time_table1');
		});
	}

}
