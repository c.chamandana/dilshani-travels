<footer class="main-footer">
	<div class="pull-right hidden-xs">
		
	</div>
	<strong>Copyright &copy; {{ date("Y") }} <a href="/">Dilshani Travels</a>.</strong> All rights
	reserved.
</footer>