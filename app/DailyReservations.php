<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DailyReservations extends Model
{
    protected $table = "daily_reservations";
}
