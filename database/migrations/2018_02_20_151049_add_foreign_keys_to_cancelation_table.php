<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToCancelationTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('cancelation', function(Blueprint $table)
		{
			$table->foreign('booking_id', 'fk_cancelation_booking1')->references('id')->on('booking')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('cancelation_fees_id', 'fk_cancelation_cancelation_fees1')->references('id')->on('cancelation_fees')->onUpdate('NO ACTION')->onDelete('NO ACTION');
			$table->foreign('customer_id', 'fk_cancelation_customer1')->references('id')->on('customer')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('cancelation', function(Blueprint $table)
		{
			$table->dropForeign('fk_cancelation_booking1');
			$table->dropForeign('fk_cancelation_cancelation_fees1');
			$table->dropForeign('fk_cancelation_customer1');
		});
	}

}
