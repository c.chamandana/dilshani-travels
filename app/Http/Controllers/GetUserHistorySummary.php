<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

/**
 * Description of GetUserHistorySummary
 *
 * @author User
 */
class GetUserHistorySummary {
    public function index($userID) {
        //$sql = "call sp_get_user_history_summary(".$userID.")";
		$sql = 'SELECT`booking`.`id`, `booking`.`ref`, DATE_FORMAT(`booking`.`booked_date`,"%Y-%m-%d") AS booked_date, DATE_FORMAT(`booking`.`booking_date`,"%Y-%m-%d") AS booking_date, `from_bus_stand`.`stand_name` AS `from`, `to_bus_stand`.`stand_name` AS `to`, `booking_status`.`status_name` AS `bkstat`, `payment_status`.`status_name` AS `paystat`, `bus`.`bus_name`, `bus`.`bus_number`FROM`customer`INNER JOIN `users` ON (`customer`.`users_id` = `users`.`id`)INNER JOIN `booking` ON (`booking`.`customer_id` = `customer`.`id`)INNER JOIN `time_table` ON (`booking`.`time_table_id` = `time_table`.`id`)INNER JOIN `from_bus_stand` ON (`time_table`.`from_bus_stand_id` = `from_bus_stand`.`id`)INNER JOIN `to_bus_stand` ON (`time_table`.`to_bus_stand_id` = `to_bus_stand`.`id`)INNER JOIN `booking_status` ON (`booking`.`booking_status_id` = `booking_status`.`id`)INNER JOIN `payment_status` ON (`booking`.`payment_status_id` = `payment_status`.`id`)INNER JOIN `bus` ON (`time_table`.`bus_id` = `bus`.`id`)WHERE (`users`.`id` ='.$userID.')';
        $input_array = array();
        $tbl = array();
        $decoded = json_decode(executePDOQuery($sql, $input_array), true);
        foreach ($decoded as $row) {
            array_push($tbl, array("id"=>$row['id'],"ref"=>$row['ref'],"booked_date"=>$row['booked_date'],
                "booking_date"=>$row['booking_date'],"from"=>$row['from'],"to"=>$row['to'],
                "bkstat"=>$row['bkstat'],"paystat"=>$row['paystat'],"bus_name"=>$row['bus_name'],
                "bus_number"=>$row['bus_number'],"action"=>""));
        }
        $dataArray = array("data"=>$tbl);
        return json_encode($dataArray);
    }
}
