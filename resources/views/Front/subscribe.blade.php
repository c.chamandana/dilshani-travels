<div class="subscribe section-wrapper">
    <div class="container">
        <div class="row">
            <div data-aos="fade-up" class="col col-lg-6 col-md-6 col-sm-12 col-xs-12 newsletter-container">
                <a class="brand-logo" href="/" title="Dilshani Travels"><i class="ion-paper-airplane"></i> Dilshani <span>Travel</span></a>

                <p class="subscribe-now">
                    Subscribe to our Newsletter
                </p>

                <form action="/newsletter/subscribe" enctype="multipart/form-data" method="POST">
                    {{ csrf_field() }}
                    <div class="input-group">
                        <input type="email" validate required class="form-control border-radius" name="email" placeholder="Email address">
                        <span class="input-group-btn">
                            <button class="btn btn-default border-radius custom-sub-btn" type="submit">SUBSCRIBE</button>
                        </span>
                    </div><!-- /input-group -->
                </form>
                       

                <ul class="social-icon">
                    <li><a href="#"><i class="ion-social-twitter"></i></a></li>
                    <li><a href="#"><i class="ion-social-facebook"></i></a></li>
                    <li><a href="#"><i class="ion-social-linkedin-outline"></i></a></li>
                    <li><a href="#"><i class="ion-social-googleplus"></i></a></li>
                </ul>
            </div>

            <div data-aos="fade-up" data-aos-delay="200" class="col col-lg-6 col-md-6 col-sm-12 col-sx-12 facebook-page-container">
                <div class="fb-page" data-height="250px" data-href="https://www.facebook.com/dilshani.travels/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/dilshani.travels/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/dilshani.travels/">Dilshani Travels</a></blockquote></div>
            </div>
        </div>
    </div>
</div>

