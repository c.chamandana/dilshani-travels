<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bus;
use App\BusInfo;

class BusDetailsController extends Controller
{
	// Gets bus information according to the index
    public function get(Request $request, $id)
    {
    	$bus = Bus::where("id", $id)->first();
    	$businfo = BusInfo::where("id", $id)->first();

    	// Echo the JSON data
    	echo json_encode([
    		"number" => $bus->bus_number,
    		"driver_name" => $businfo->driver_name,
    		"driver_phone" => $businfo->driver_phone,
    		"conductor_name" => $businfo->conductor_name,
    		"conductor_phone" => $businfo->conductor_phone,
    		"hire" => $bus->is_for_hire
    	]);
    }

    // Save bus details to the database
    public function save(Request $request , $id)
    {

    	$bus = Bus::where("id", $id)->first();
    	$businfo = BusInfo::where("id", $id)->first();

    	if ($bus && $businfo)
    	{
    		$bus->bus_number = $request->number;
    		$businfo->driver_name = $request->driver_name;
    		$businfo->driver_phone = $request->driver_phone;
    		$businfo->conductor_name = $request->conductor_name;
    		$businfo->conductor_phone = $request->conductor_phone;

    		if ($request->bus_hire == "on")
    		{
    			$bus->is_for_hire = true;
    		} else {
    			$bus->is_for_hire = false;
    		}

    		// Save the bus details
    		$bus->save();
    		$businfo->save();
    	}

    	// return the user to the edit page
    	return redirect("/editBusDetails")->with([
    		"message" => "Bus details has been successfully saved",
    		"message-type" => "info"
    	]);
    }

    // Create new bus and bus details to the database
    public function new (Request $request)
    {
    	$busno = $request->number;
    	$driver_name = $request->driver_name;
    	$driver_phone = $request->driver_phone;
    	$conductor_name = $request->conductor_name;
    	$conductor_phone = $request->conductor_phone;
    	$is_for_hire = $request->bus_hire;

    	$bus = new Bus;
    	$businfo = new BusInfo;

    	// Set the bus information
    	$bus->bus_number = $request->number;
    	$bus->bus_name = $request->number;
		$businfo->driver_name = $request->driver_name;
		$businfo->driver_phone = $request->driver_phone;
		$businfo->conductor_name = $request->conductor_name;
		$businfo->conductor_phone = $request->conductor_phone;

		if ($request->bus_hire == "on")
		{
			$bus->is_for_hire = true;
		} else {
			$bus->is_for_hire = false;
		}

		$bus->save(); // Create a new bus instance

		$businfo->bus_id = $bus->id;
		$businfo->save();

		// return the user to the edit page
    	return redirect("/editBusDetails")->with([
    		"message" => "A new bus has been added to the collection",
    		"message-type" => "info"
    	]);
    }
}
