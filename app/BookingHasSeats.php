<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $booking_id
 * @property int $seats_id
 * @property boolean $ticket_printed
 * @property Booking $booking
 * @property Seat $seat
 */
class BookingHasSeats extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['ticket_printed','booking_id','seats_id'];
    public $timestamps = false;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function booking()
    {
        return $this->belongsTo('App\Booking');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function seat()
    {
        return $this->belongsTo('App\Seat', 'seats_id');
    }
}
