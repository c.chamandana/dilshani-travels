<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBookingHasSeatsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('booking_has_seats', function(Blueprint $table)
		{
			$table->integer('booking_id')->index('fk_booking_has_seats_booking1_idx');
			$table->integer('seats_id')->index('fk_booking_has_seats_seats1_idx');
			$table->boolean('ticket_printed')->nullable();
			$table->increments('id');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('booking_has_seats');
	}

}
