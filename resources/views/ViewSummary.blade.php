@extends('layouts.admin')
@section('mainContent')

<?php 

    $origin = "Colombo";
    $destination = "Galle";

?>

    <div class="container thankyou-container">
        <div class="thankyou-card">
            <span class="sub-title">
                SUCCESSFUL
            </span>
            <span class="main-title">
                TICKET PURCHASE
            </span>

            <p class="purchase-sentence">You have successfully booked <b>{{ sizeof(Session::get("seats")) }} Bus 
                @if(sizeof(Session::get("seats")) > 1) 
                    seats
                @else
                    seat
                @endif 
                @if (Session::get("booking")->booking_date == Session::get("booking")->end_date)
                    for {{ Carbon\Carbon::parse(Session::get("booking")->booking_date)->format('d.m.Y') }}
                @else
                    from {{ Carbon\Carbon::parse(Session::get("booking")->booking_date)->format('d.m.Y') }} to {{ Carbon\Carbon::parse(Session::get("booking")->end_date)->format('d.m.Y') }}
                @endif
                </b> here's the summary of your purchase
            </p>

            <table class="receipt">
                <tr>
                    <td class="item">
                        {{ sizeof(Session::get("seats")) }} x Adult Seats
                    </td>
                    <td class="price">
                        LKR. {{ sizeof(Session::get("seats")) * 650 }}
                    </td>
                </tr>
            </table>

            <a href="/home">
                <button class="continue-btn">
                    Continue to Dashboard
                </button>
            </a>
        </div>

        <div class="thankyou-card" style="margin-top:25px;padding:0 !important">
            <iframe
            width="100%"
            height="450"
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed/v1/directions?key=AIzaSyCmJ-GViWmgK9BgHcWdvCsMNT6XQzSybhc&origin={{ $origin }}&destination={{ $destination }}&avoid=tolls|highways" allowfullscreen>
            </iframe>
        </div>
    </div>
@stop
@section('scripts')
<script>
    // $(document).ready(function () {
    //     var table = $("#tblsummary").DataTable({
    //         "searching": false,
    //         paging: true,
    //         "ajax": {
    //             "url": "/getBookingSummary/<?php echo $bookingID;?>"
    //         },
    //         "columns": [
    //             {"data": "ref", 'title': 'Ref. No.'},
    //             {"data": "booking_date", 'title': 'Booking Date'},
    //             {"data": "seat_number", 'title': 'Seat'}
    //         ]
    //     });
    // });
</script>
@stop