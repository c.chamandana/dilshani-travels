<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property float $Rate
 * @property string $description
 * @property float $defaultmilage
 * @property float $defaultrate
 * @property Hire[] $hires
 */
class hirerate extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'hirerate';

    /**
     * @var array
     */
    protected $fillable = ['Rate', 'description', 'defaultmilage', 'defaultrate'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hires()
    {
        return $this->hasMany('App\Hire', 'hireRate_id');
    }
}
