<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property int $bus_id
 * @property int $day_of_week_id
 * @property int $end_time_id
 * @property int $from_bus_stand_id
 * @property int $start_time_id
 * @property int $to_bus_stand_id
 * @property float $ticket_price_adult
 * @property float $ticket_price_child
 * @property Bus $bus
 * @property DayOfWeek $dayOfWeek
 * @property EndTime $endTime
 * @property FromBusStand $fromBusStand
 * @property StartTime $startTime
 * @property ToBusStand $toBusStand
 * @property Booking[] $bookings
 */
class TimeTable extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'time_table';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['bus_id', 'day_of_week_id', 'end_time_id', 'from_bus_stand_id', 'start_time_id', 'to_bus_stand_id', 'ticket_price_adult', 'ticket_price_child'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function dayOfWeek()
    {
        return $this->belongsTo('App\DayOfWeek');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function endTime()
    {
        return $this->belongsTo('App\EndTime');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fromBusStand()
    {
        return $this->belongsTo('App\FromBusStand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function startTime()
    {
        return $this->belongsTo('App\StartTime');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function toBusStand()
    {
        return $this->belongsTo('App\ToBusStand');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookings()
    {
        return $this->hasMany('App\Booking');
    }
}
