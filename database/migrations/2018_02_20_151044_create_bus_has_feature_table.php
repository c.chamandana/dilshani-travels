<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBusHasFeatureTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('bus_has_feature', function(Blueprint $table)
		{
			$table->integer('bus_id')->index('fk_bus_has_feature_bus1_idx');
			$table->integer('feature_id')->index('fk_bus_has_feature_feature1_idx');
			$table->primary(['bus_id','feature_id']);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('bus_has_feature');
	}

}
