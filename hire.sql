-- MySQL Workbench Synchronization
-- Generated: 2018-01-30 07:15
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: anura

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `bus_booking`.`hire` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `fromdate` DATE NULL DEFAULT NULL,
  `todate` DATE NULL DEFAULT NULL,
  `fromplace` VARCHAR(255) NULL DEFAULT NULL,
  `toplace` VARCHAR(255) NULL DEFAULT NULL,
  `km` DECIMAL(7,2) NULL DEFAULT NULL,
  `bus_id` INT(11) NOT NULL,
  `customer_id` INT(11) NOT NULL,
  `hireRate_id` INT(10) UNSIGNED NOT NULL,
  `booking_status_id` INT(11) NOT NULL,
  `actual_end_date` DATE NULL DEFAULT NULL,
  `actual_milage` DECIMAL(10,2) NULL DEFAULT NULL,
  `total` DECIMAL(12,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_hire_bus1_idx` (`bus_id` ASC),
  INDEX `fk_hire_customer1_idx` (`customer_id` ASC),
  INDEX `fk_hire_hireRate1_idx` (`hireRate_id` ASC),
  INDEX `fk_hire_booking_status1_idx` (`booking_status_id` ASC),
  CONSTRAINT `fk_hire_bus1`
    FOREIGN KEY (`bus_id`)
    REFERENCES `bus_booking`.`bus` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hire_customer1`
    FOREIGN KEY (`customer_id`)
    REFERENCES `bus_booking`.`customer` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hire_hireRate1`
    FOREIGN KEY (`hireRate_id`)
    REFERENCES `bus_booking`.`hireRate` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_hire_booking_status1`
    FOREIGN KEY (`booking_status_id`)
    REFERENCES `bus_booking`.`booking_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;

CREATE TABLE IF NOT EXISTS `bus_booking`.`hireRate` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `Rate` DECIMAL(10,2) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `defaultmilage` DECIMAL(7,2) NULL DEFAULT NULL,
  `defaultrate` DECIMAL(10,2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
