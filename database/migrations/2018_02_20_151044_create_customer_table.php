<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCustomerTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('customer', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('customer_name')->nullable();
			$table->string('email')->nullable()->unique('email_UNIQUE');
			$table->string('address_line1')->nullable();
			$table->string('address_line2')->nullable();
			$table->integer('city_id')->index('fk_customer_city1_idx');
			$table->string('mobile_no', 15);
			$table->string('recovery_code', 15)->nullable();
			$table->integer('users_id')->unsigned()->index('fk_customer_users1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('customer');
	}

}
