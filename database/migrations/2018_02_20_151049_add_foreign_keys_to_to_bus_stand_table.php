<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToToBusStandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('to_bus_stand', function(Blueprint $table)
		{
			$table->foreign('city_id', 'fk_to_bus_stand_city1')->references('id')->on('city')->onUpdate('NO ACTION')->onDelete('NO ACTION');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('to_bus_stand', function(Blueprint $table)
		{
			$table->dropForeign('fk_to_bus_stand_city1');
		});
	}

}
