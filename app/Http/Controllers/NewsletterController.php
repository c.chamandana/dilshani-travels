<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\NewsletterSubscription;
use Session;

class NewsletterController extends Controller
{
    public function submit(Request $request)
    {
    	$subscription = new NewsletterSubscription;
    	$subscription->email = $request->email;
    	$subscription->save();

    	Session::flash("message", "You have been successfully subscribed to our newsletter");
    	Session::flash("message-type", "info");

    	return redirect("/");
    }
}
