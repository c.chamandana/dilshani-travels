<?php

use App\Http\Middleware\SummaryViewer;
use App\Http\Middleware\LoginInsist;
use App\Http\Middleware\AdminInsist;
use App\Http\Middleware\PreventAdmin;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Home route for the Front-end
Route::get('/', function () {
    return view('Front.index');
})->name("index");

// Set Daily Seating options
Route::get('dailySeating', function () {
    return view('DailyBooking');
})->middleware(LoginInsist::class)->middleware(PreventAdmin::class)->name('dailySeating');

// Hire a bus through here
Route::get("hire", function () {
        return view('HireBus');
})->middleware(LoginInsist::class)->name("hire");

// Shows hire details after the purchases
Route::get("hireDetails", function () {
    return view('HireDetails');
})->middleware(LoginInsist::class)->name("hireDetails");

// Administrators only
Route::get('admin', function () {
    return view('admin');
})->middleware(LoginInsist::class)->middleware(LoginInsist::class)->name('admin');

// Shows the timetable for the end-user
Route::get('timetable', function () {
    return view('timetable');
})->middleware(LoginInsist::class)->name('timetable');

// Shows the selectedDates of a timetable entry
Route::get('selectedDates', function () {
    return view('selectedDates');
})->middleware(LoginInsist::class)->name('selectedDates');

// only for the admin
Route::get('editContent', function () {
    return view('EditPageContent');
})->middleware(AdminInsist::class)->name('editContent');

// only for the Admin
Route::get("messages", function() 
{
    return view("Messages");
})->middleware(AdminInsist::class)->name("user-messages");

// only for the admin
Route::get("showReviews", function() 
{
    return view("showReviews");
})->middleware(AdminInsist::class)->name("showReviews");

// Only for the admin
Route::get("editBusDetails", function()
{
    return view("editBusDetails");
})->middleware(AdminInsist::class)->name("editBusDetails");

Route::get("showReports", function() 
{
    return view("showReports");
})->middleware(AdminInsist::class)->name("showReports");

// Shows seating options
Route::get('seating', function () {
    return view('selectseat');
})->middleware(LoginInsist::class)->name('seating');

// Authentication routes such as login, register and etc.
Auth::routes();

Route::get('/home', 'HomeController@index')->middleware(LoginInsist::class)->middleware(PreventAdmin::class)->name('home');

Route::post('saveHire','SaveHire@index')->middleware(LoginInsist::class)->name('saveHire');

Route::get('/content', 'LoadContent@index')->name('content');

Route::get('getSeats/{busID}', 'GetSeats@index')->middleware(LoginInsist::class)->name('getSeats');

Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

Route::get('LoadTimeTable', 'LoadTimeTable@index')->name('LoadTimeTable');

Route::get('LoadHireTable', 'LoadHireTable@index')->name('LoadHireTable');

Route::get('LoadTimeTableWithSelectedData/{start}/{end}/{from}/{to}/{date}', 'LoadTimeTableWithSelectedData@index')->name('LoadTimeTableWithSelectedData');

Auth::routes();

Route::get('/home', 'HomeController@index')->middleware(LoginInsist::class)->middleware(PreventAdmin::class)->name('home');

Route::get('UserHistory', function () {
    return view("UserHistorySummary",["userID"=> Illuminate\Support\Facades\Auth::user()->id]);
})->name('UserHistory');

Route::get("getUserHistorySummary/{userID}", "GetUserHistorySummary@index")->name("getUserHistorySummary");

Route::get('UserPending', function () {
    return view("Cancel",["userID"=> Illuminate\Support\Facades\Auth::user()->id]);
})->name('UserPending');

Route::get("getUserPendingSummary/{userID}","GetUserPendingSummary@index")->name("getUserPendingSummary");

Route::post("saveSeats", "SaveSeats@index")->middleware(LoginInsist::class)->name("saveSeats");

Route::post("cancelBooking","CancelBooking@cancel")->name("cancelBooking");

Route::get("/viewSummary/{bookingID}", function ($bookingID) {
    return view("ViewSummary",["bookingID"=>$bookingID]);
})->middleware(LoginInsist::class)->middleware(SummaryViewer::class)->name("viewSummary");

Route::get("getBookingSummary/{bookingID}", "BookingSummary@index")->name("getBookingSummary");

Route::post("contentEdit","ContentEdit@index")->name("contentEdit");

Route::post("imageEdit","ContentEdit@image")->name("imageEdit");

// Messages recieving and saving
Route::post("/SendMessage", "MessageController@save")->name("saveMessages");

Route::get("/extras", "DashboardController@extras")->middleware(LoginInsist::class)->middleware(PreventAdmin::class)->name("extras");

// Submit extra services
Route::post("/extra/submit", "ExtraServicesController@submit")->middleware(PreventAdmin::class)->middleware(LoginInsist::class)->name("submitExtraServices");

Route::get("/settings/profile", "SettingsController@profileView")->middleware(LoginInsist::class)->name("ProfileSettings");

Route::post("/settings/save/profile", "SettingsController@saveProfile")->middleware(LoginInsist::class)->name("saveProfileSettings");

Route::post("/settings/save/password", "SettingsController@changePassword")->middleware(LoginInsist::class)->name("changePassword");

Route::get("/settings/delete/account", "SettingsController@deleteAccount")->middleware(LoginInsist::class)->name("deleteAccount");


Route::get("/report", function()
{
    return view("Front.report");
})->name("report");

Route::post("/report/submit", "ReportController@submit")->name("SubmitReport");

Route::get("/tnc", function()
{
    return view("Front.tnc");
})->name("TermsAndConditions");

Route::post("/newsletter/subscribe", "NewsletterController@submit")->name("newsletterSubscribe");

Route::post("/create/review", "ReviewController@create")->middleware(AdminInsist::class)->name("createReview");
Route::post("/edit/review/{id}", "ReviewController@save")->middleware(AdminInsist::class)->name("saveReviewDetails");
Route::get("/get/review/{id}", "ReviewController@getDetails")->middleware(AdminInsist::class)->name("getReviewDetails");
Route::get("/delete/review/{id}", "ReviewController@delete")->middleware(AdminInsist::class)->name("deleteReviews");

// Get the bus details
Route::get("/bus/details/{id}", "BusDetailsController@get")->middleware(AdminInsist::class)->name("getBusDetails");
Route::post("/bus/save/{id}", "BusDetailsController@save")->middleware(AdminInsist::class)->name("saveBusDetails");
Route::post("/bus/new", "BusDetailsController@new")->middleware(AdminInsist::class)->name("createNewBus");


// Route for Saving daily passenger
Route::post("/save-daily-passenger", "DailyPassengerController@save")->middleware(PreventAdmin::class)->name("SaveDailyPassenger");