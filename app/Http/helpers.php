<?php
function executePDOQuery($sql, $input_array,$reversed=false) {
    $pdo = null;
//        if (null == $pdo) {
    $host = 'localhost';
    $db = 'busbooking';
    $user = 'root';
    $pass = '';

    $dsn = "mysql:host=$host;dbname=$db";
    $opt = [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);
//        }
    $stmt = $pdo->prepare($sql);
    if (empty($input_array)) {
        $stmt->execute();
    } else {
        $stmt->execute($input_array);
    }
    $results = $stmt->fetchAll(PDO::FETCH_ASSOC);
    $stmt = null;
    $pdo = null;
    
    if ($reversed) {
        $results = array_reverse($results);
    }
    
    return json_encode($results,false);
}
?>