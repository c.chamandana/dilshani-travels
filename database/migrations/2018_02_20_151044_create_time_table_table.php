<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTimeTableTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('time_table', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->integer('from_bus_stand_id')->index('fk_time_table_from_bus_stand_idx');
			$table->integer('to_bus_stand_id')->index('fk_time_table_to_bus_stand1_idx');
			$table->integer('day_of_week_id')->index('fk_time_table_day_of_week1_idx');
			$table->integer('start_time_id')->index('fk_time_table_start_time1_idx');
			$table->integer('end_time_id')->index('fk_time_table_end_time1_idx');
			$table->integer('bus_id')->index('fk_time_table_bus1_idx');
			$table->decimal('ticket_price_adult', 6)->nullable();
			$table->decimal('ticket_price_child', 6)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('time_table');
	}

}
