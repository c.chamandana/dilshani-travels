<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $time_name
 * @property string $time_value
 * @property TimeTable[] $timeTables
 */
class EndTime extends Model
{
    /**
     * The table associated with the model.
     * 
     * @var string
     */
    protected $table = 'end_time';
    public $timestamps = false;
    /**
     * @var array
     */
    protected $fillable = ['time_name', 'time_value'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function timeTables()
    {
        return $this->hasMany('App\TimeTable');
    }
}
