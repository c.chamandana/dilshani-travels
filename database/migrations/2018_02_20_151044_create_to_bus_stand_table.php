<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateToBusStandTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('to_bus_stand', function(Blueprint $table)
		{
			$table->integer('id', true);
			$table->string('stand_name')->nullable()->unique('start_name_UNIQUE');
			$table->integer('city_id')->index('fk_to_bus_stand_city1_idx');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('to_bus_stand');
	}

}
