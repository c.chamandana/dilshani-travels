<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BusInfo extends Model
{
    protected $table = "bus_info";
    public $timestamps = false;
}
